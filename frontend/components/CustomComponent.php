<?php
namespace frontend\components;

use Yii;
use yii\base\Component;

class CustomComponent extends Component
{
    /**
	 * Формат вывода API
     * @return json
     */
    public function returnApi($success, $data = [], $messages = []) {
	    header("Content-type: application/json; charset=utf-8");
        header('Access-Control-Allow-Origin: *');

        echo json_encode([
			'success' => ($success)?true:false,
			'messages' => $messages,
			'data' => $data,
		]);
		exit;
    }

	
}
