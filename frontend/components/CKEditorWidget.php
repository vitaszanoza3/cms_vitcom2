<?php

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Html;

class CKEditorWidget extends Widget
{
    public $model, $attribute, $view, $id;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
		$id = ($this->id)?$this->id:$this->attribute;
		//echo 'model=<pre>'.print_r($this->model->getErrors(), true).'</pre><br/>';
		//echo 'attribute=<pre>'.print_r($this->attribute, true).'</pre><br/>';
		//echo 'view=<pre>'.print_r($this->view, true).'</pre><br/>';
		$this->view->registerJsFile('/backend/web/extensions/ckeditor/ckeditor.js', ['position' => \yii\web\View::POS_END], 'ckeditor');
		$this->view->registerJs('
			var editor_'.$this->attribute.' = CKEDITOR.replace( "'.$id.'", {
			language: "ru",
			//filebrowserBrowseUrl : "/backend/web/extensions/ckfinder3_5/ckfinder.html",
			//filebrowserImageBrowseUrl : "/backend/web/extensions/ckfinder3_5/ckfinder.html?type=Images",
			//filebrowserImageUploadUrl : "/backend/web/site/ckfinder?command=QuickUpload&type=Images",
			});
		');
        return '<textarea id="'.$id.'" data-type="editor" class="form-control" name="DynamicForm['.$this->attribute.']">'.$this->model->{$this->attribute}.'</textarea>';
    }
}