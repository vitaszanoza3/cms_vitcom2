<?php
namespace frontend\components;

use Yii;
use yii\base\Component;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\helpers\Json;

use common\models\User;
use common\models\Page;
use common\models\Fields;
use frontend\models\DynamicForm;

class ActionFormComponent extends Component
{
    const FUNC_createAndValidateForm = 'createAndValidateForm';
    const FUNC_ajaxPost_SendMail = 'ajaxPost_SendMail';
    const FUNC_ajaxPost_FormAuthentication = 'ajaxPost_FormAuthentication';

    /*КЛЮЧИ reCAPTCHA*/
    const SITE_KEY = '6LcED1IaAAAAAK6qaesnNk2HkwGM4tsArK-TPcIv';
    const SECRET_KEY = '6LcED1IaAAAAAIjx3x_hECknSz3Z0zh-WB9Hdqkw';
	
    /*
	* Создание и валидация DynamicForm
	* return DynamicForm()
	*/
	private function _createAndValidateForm($formId, $page, $postData, $config, &$magicData){

		if(!isSet($postData['DynamicForm'])) throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не найдены данные DynamicForm'));
		$formModel = new DynamicForm();
		$formModel->setFields($config['fields']);
	
        if (Yii::$app->request->isPost) {
				
			$loadBool = $formModel->load($postData);
			foreach($config['fields'] as $fieldName => $data){
				if(in_array($data[1], ['TYPE_FILES', 'TYPE_IMAGES', 'TYPE_FILE']) && isSet($_FILES['DynamicForm']['name'][$fieldName])){
					$formModel->{$fieldName} = UploadedFile::getInstance($formModel, $fieldName);
				}
			}

			if(Yii::$app->controller->action->id == 'index' && Yii::$app->request->isAjax && $loadBool){
				Yii::$app->response->format = Response::FORMAT_JSON;
				return Json::encode(\yii\widgets\ActiveForm::validate($formModel));
			}  
			
			if(isSet($config['recaptcha']) && $config['recaptcha']){
				if(!isSet($postData['g-recaptcha-response'])){
					$formModel->addError('recaptcha', Yii::$app->t('error', 'Ошибка recaptcha'));
					return $formModel;
				} else {
					$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".Yii::$app->actionForm::SECRET_KEY."&response=".$postData['g-recaptcha-response']);
					$return = json_decode($response);

					if(!$return->success){
						$formModel->addError('recaptcha', Yii::$app->t('error', 'Ошибка recaptcha'));
						$loadBool = false;
					} else {
						if($return->hostname != $_SERVER['HTTP_HOST']){
							$formModel->addError('recaptcha', Yii::$app->t('error', 'Ошибка recaptcha'));
							$loadBool = false;
						}
					}
				}
			}

			if ($loadBool && $formModel->validate()) {
				foreach($config['fields'] as $fieldName => $data){
					if(in_array($data[1], ['TYPE_FILES', 'TYPE_IMAGES', 'TYPE_FILE']) && isSet($_FILES['DynamicForm']['name'][$fieldName])){
						if($_FILES['DynamicForm']['error'][$fieldName]){ 
							//$formModel->addError($fieldName, Yii::$app->t('error', 'Неизвестная ошибка!')); 
							continue;
						}
					
						$ext = explode('.', $_FILES['DynamicForm']['name'][$fieldName]);
						$ext = mb_strtolower(end($ext));
						
						//Проверка MIME TYPE
						$mimeTypes = Fields::mimeTyps($ext);
						if($mimeTypes && in_array($_FILES['DynamicForm']['type'][$fieldName], $mimeTypes)){}
						else {
							$formModel->addError($fieldName, Yii::$app->t('error', 'Не верный тип файла: {type}', ['type' => $ext]));
							continue;
						}
						
						//Проверка доступных типов
						if(isSet($data['types'])) $types = $data['types'];
						else $types = Fields::preset($data[1])['types'];
						if(!in_array($ext, $types)){
							$formModel->addError($fieldName, Yii::$app->t('error', 'Не верный тип файла: {type}', ['type' => $ext]));
							continue;
						}
							
						//Проверка разрешенного размера
						if(isSet($data['max_size'])) $max_size = $data['max_size'];
						else $max_size = Fields::preset($data[1])['max_size'];
						if($_FILES['DynamicForm']['size'][$fieldName] > $max_size * 1000000){
							$formModel->addError($fieldName, Yii::$app->t('error', 'Файл {name} превышает допустимый размер {size}', ['name' => $_FILES['DynamicForm']['name'][$fieldName], 'size' => $max_size.' Mb']));
							continue;
						}

						if(isSet($_FILES['DynamicForm']['name'][$fieldName])) $formModel->{$fieldName} = [
							'name' => $_FILES['DynamicForm']['name'][$fieldName],
							'type' => $_FILES['DynamicForm']['type'][$fieldName],
							'ext' => $ext,
							'tmp_name' => $_FILES['DynamicForm']['tmp_name'][$fieldName],
							'error' => $_FILES['DynamicForm']['error'][$fieldName],
							'size' => $_FILES['DynamicForm']['size'][$fieldName],
						];
					}
				}
				
				if(count($formModel->getErrors()) == 0) $formModel->success = true;
			}
			
			if($formModel->success){
				$magicData[Yii::$app->controller::API_DATA]->success = true;
				$options = $this->_getFormOptions($page, $formId, $config);

				$messages[] = $options->success;
				$magicData[Yii::$app->controller::API_DATA]->messages = $messages;
			}else {
				$magicData[Yii::$app->controller::API_DATA]->success = false;
				$messages = [];
				foreach($formModel->getErrors() as $field => $message){ 
					$messages[$field] = $message[0]; 
				}
				$magicData[Yii::$app->controller::API_DATA]->messages = $messages;
			}
		}
		
		//die("formModel1<pre>".print_r($formModel, true)."</pre>");
		return $formModel;
	}
	
	private function _getFormOptions($page, $fieldName, $config){
		$options = null;

		//Берем настройки с текущей страницы
		$opt = json_decode(\yii\helpers\Html::decode($page->$fieldName));
		if($opt && isSet($opt->on) && strpos($opt->on, 'on')){ $options = $opt;}
		
		//Если не нашли то с aliasOptions
		if(!$options && isSet($config['aliasOptions'])){
			$page = Page::findPage($config['aliasOptions']);
			$opt = json_decode(\yii\helpers\Html::decode($page->$fieldName));
			if($opt && isSet($opt->on) && strpos($opt->on, 'on')){ $options = $opt;}
		}
		
		//Если не нашли то с корневой страницы
		if(!$options){
			$options = json_decode(\yii\helpers\Html::decode(Yii::$app->controller->mainPage->defaultMailForm));
		}

		return $options;
	}
	
	public function getDefaultMailForm(){
		if(Yii::$app->controller->mainPage) $mainPage = Yii::$app->controller->mainPage;
		else $mainPage = Page::findPage('main');
		return json_decode(\yii\helpers\Html::decode($mainPage->defaultMailForm));
	}
	
	/*
	* Метод вызывается из BaseController->beforeAction метод api или index. 
	* Предназначена для проверки валидации
	* return вывод через magicData
	*/
	public function createAndValidateForm($fieldName, $page, $postData, $config, &$magicData){
		$formModel = $this->_createAndValidateForm($fieldName, $page, $postData, $config, $magicData);
		$magicData[$fieldName] = $formModel;
	}
	
	/*
	* Метод вызывается из BaseController->beforeAction метод api или index. 
	* Предназначена для отправки почты
	* return вывод через magicData
	*/
	public function ajaxPost_SendMail($fieldName, $page, $postData, $config, &$magicData){
		$formModel = $this->_createAndValidateForm($fieldName, $page, $postData, $config, $magicData);

		if($formModel->success){
			$options = $this->_getFormOptions($page, $fieldName, $config);
			
			$emails = str_replace(';', ',', $options->to_email);
			$emails = explode(',', $emails);
			$messages = [];
			foreach($emails as $index => $email){
				$messages[$index] = Yii::$app->mailer->compose()
					->setFrom($options->from_email)
					->setTo(trim($email))
					->setSubject($options->title);
					//->setTextBody('Текст сообщения');
				
				$mailData = [];
				foreach($formModel->attributes as $field => $value){
					if(!is_array($value)){
						if(in_array($config['fields'][$field][1], [Fields::TYPE_DROPDOWN_LIST,Fields::TYPE_CHECKBOX,Fields::TYPE_RADIO])){
							$list = Yii::$app->dataListComponent->{$config['fields'][$field]['function']}($config['fields'][$field]['params']);
							if(isSet($list[$value])) $value = $list[$value];
							$mailData[$formModel->attributeLabels()[$field]] = $value;
						} else $mailData[$formModel->attributeLabels()[$field]] = $value;
					} else {
						if(isSet($value['tmp_name'])){
							// Прикрепить файл
							$messages[$index]->attach($value['tmp_name'], ['fileName' => $value['name'], 'contentType' => $value['type']]);
						} else {
							if(in_array($config['fields'][$field][1], [Fields::TYPE_DROPDOWN_LIST,Fields::TYPE_CHECKBOX,Fields::TYPE_RADIO])){
								$list = Yii::$app->dataListComponent->{$config['fields'][$field]['function']}($config['fields'][$field]['params']);
								$items = [];
								foreach($value as $item) $items[] = (isSet($list[$item]))?$list[$item]:$item;
								$mailData[$formModel->attributeLabels()[$field]] = $items;
							}
						}
					}
				}
				//die("<pre>".print_r($mailData, true)."</pre>");
				if(isSet($config['letter_view'])){
					$html = Yii::$app->controller->renderPartial('/mail/'.$config['letter_view'], ['options' => $options, 'mailData' => $mailData]);
				} else {
					$html = Yii::$app->controller->renderPartial('/mail/default', ['options' => $options, 'mailData' => $mailData]);
				}
				$messages[$index]->setHtmlBody($html);
			}
			
			//$message->send();
			Yii::$app->mailer->sendMultiple($messages);
			//Добавить вероятность ошибки
			//$formModel->addError($name, Yii::$app->t('error', 'Не верный тип файла: {type}', ['type' => $ext]));
			//$formModel->success = false;
		}
				
		$magicData[$fieldName] = $formModel;
		//return $dynamicData;
	}

	/*
	* Метод вызывается из BaseController->beforeAction метод api или index. 
	* Предназначена для аутентификации пользователя
	* return вывод через magicData
	*/
	public function ajaxPost_FormAuthentication($fieldName, $page, $postData, $config, &$magicData){
		$formModel = $this->_createAndValidateForm($fieldName, $page, $postData, $config, $magicData);
		if($formModel->success){
			$formModel->success = false;
			$user = User::findByUsername($formModel->username);
			if (!$user || !$user->validatePassword($formModel->password)) {
				//$formModel->addError('username', Yii::$app->t('error', 'Не верный логин или пароль!'));
				$magicData[Yii::$app->controller::API_DATA]->success = false;
				$messages['username'] = Yii::$app->t('error', 'Не верный логин или пароль!');
				$magicData[Yii::$app->controller::API_DATA]->messages = $messages;
			} else {
				$GLOBALS['userRole'] = $user->role;
				$login = Yii::$app->user->login($user, $formModel->rememberMe ? 3600 * 24 * 30 : 0);
				if($login) {
					if(!Yii::$app->user->isGuest){
						$formModel->success = true;
						//echo Yii::$app->user->identity->username;
					}
				}
						
				$apiData['html'] = Yii::$app->controller->renderPartial(Yii::$app->controller->pathTemplate.'/_login');
				$magicData[Yii::$app->controller::API_DATA]->addData($apiData);

			}
		}
		$magicData[$fieldName] = $formModel;
	}
	
}
