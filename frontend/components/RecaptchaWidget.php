<?php

namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Html;

class RecaptchaWidget extends Widget
{
    public $model, $attribute, $view, $id;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
		$id = ($this->id)?$this->id:$this->attribute;
		//echo 'model=<pre>'.print_r($this->model->getErrors(), true).'</pre><br/>';
		//echo 'attribute=<pre>'.print_r($this->attribute, true).'</pre><br/>';
		//echo 'view=<pre>'.print_r($this->view, true).'</pre><br/>';
		$this->view->registerJsFile('https://www.google.com/recaptcha/api.js', ['position' => \yii\web\View::POS_HEAD, 'async'=>true, 'defer'=>true], 'recaptcha');
        return '<div class="g-recaptcha" data-sitekey="'.\Yii::$app->actionForm::SITE_KEY.'"></div>';
    }
}