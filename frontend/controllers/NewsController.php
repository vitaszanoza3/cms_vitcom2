<?php
namespace frontend\controllers;

use Yii;
use frontend\system\BaseController;
use yii\data\ActiveDataProvider;
use common\models\Page;

class NewsController extends BaseController
{

	/**
    * Это прослойка вызывается после валидации формы. 
	* Предназначена для подготовки данных.
	* Срабатывает при любом результате валидации. 
     * @return mixed
     */
	 public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) {
			return false;
		}
		
		if (in_array($action->id, ['index', 'api'])) {
			$request = Yii::$app->request;
			$get = $request->get(); 
			if(!isSet($get['func']) && $this->page->level == 0){
			
				$query =$this->page->getChildrenPublishedItem()->orderBy('date_display ASC');
				//die('query<pre>'.print_r($query, true).'</pre>');
						
				$provider = new ActiveDataProvider([
					'query' => $query,
					'pagination' => [
						'pageSize' => 2,
					],
					'sort' => [
						'defaultOrder' => [
							'date_display' => SORT_DESC,
							'title' => SORT_ASC, 
						]
					],
				]);

				$request = Yii::$app->request;
				$provider->pagination->route = $request->pathInfo;
				$provider->pagination->params = [
					'page' => $request->get('page'),
				];
				//$provider->sort->route = Yii::$app->request->pathInfo;
				//$provider->sort->params = [];
				//die('pagination<pre>'.print_r($provider->pagination, true).'</pre>');
				
				// возвращает массив Post объектов
				$pages = $provider->getModels();
		
				$this->magicData[$this::PROVIDER] = $provider;
				$this->magicData['pages'] = $pages;
				
				//Подготовка данных api
				$apiData = [];
				$parentUrl = '/'.Yii::$app->request->pathInfo;
				foreach($pages as $page){ 
					$apiData['items'][] = [
						'id' => $page->id,
						'alias' => $page->alias,
						'title' => $page->title,
						'url' => str_replace('.html', '/'.$page->alias.'.html', $parentUrl),
						'html' => $this->renderPartial($this->pathTemplate.'/_post', ['model' => $page]),
					];
				}
				$this->magicData[$this::API_DATA]->success = true;
				$this->magicData[$this::API_DATA]->addData($apiData);
				$this->magicData[$this::API_DATA]->messages = null;
				
			}
		}
		
		//die('magicData<pre>'.print_r(array_keys($this->magicData), true).'</pre>');
		return true;
	}
	/**
     * Входной скрипт
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render($this->pathTemplate, ['page' => $this->page, 'magicData' => $this->magicData]);
    }
	
	
}
