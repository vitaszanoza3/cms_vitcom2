<?php
namespace frontend\controllers;

use Yii;
use frontend\system\BaseController;
use common\models\User;
use common\models\Page;
use common\models\Fields;
use frontend\models\DynamicForm;
use common\components\AccessFilter;

/**
 * Account controller
 */
class AccountController extends BaseController
{
 
/**
    * Это прослойка вызывается после валидации формы. 
	* Предназначена для подготовки данных.
	* Срабатывает при любом результате валидации. 
     * @return mixed
     */
	 public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) {
			return false;
		}
		
		if (in_array($action->id, ['index', 'api'])) {
		
			//Проверка на доступ редакторов
			if(Yii::$app->user->can(User::ROLE_USER)){
				$request = Yii::$app->request;
				$post = $request->post();
				$user = Yii::$app->user->identity;
				
				//Обработка формы подтверждения email
				if($request->isPost && isSet($post['field']) && $post['field'] == 'form_code'){ 
					$model = $this->page->getForm('form_code', $this->magicData);
					if($model->success){
						$model->success = false;
						
						if($user->verify_code && $user->verify_code == $model->code){
							$user->status = User::STATUS_ACTIVE;
							$user->save(false);
							$model->success = true;
						} else {
							$model->addError('code', Yii::$app->t('error', 'Веден не верный код'));
						}
					}
				}
				
				if($user->status == User::STATUS_ACTIVE){
					//die("<pre>".print_r($this->magicData['form_user'], true)."</pre>");

					$model = $this->page->getForm('form_user', $this->magicData);
					
					//Обработка формы пользователя
					if($request->isPost && isSet($post['field']) && $post['field'] == 'form_user'){ 
						if($model->success){
							$model->success = false;
							$this->magicData[$this::API_DATA]->success = false;
							
							if($user->validatePassword($model->old_password)){
								$user->scenario = 'registration';
								$user->name = $model->name;
								$user->phone = $model->phone;
								$user->jsonData = json_encode([
									'name' => $model->name,
									'phone' => $model->phone,
									'address' => $model->address,
								]);
								$user->password = $model->password;
								$user->setPassword($model->password);
									
								if($user->save()){ 
									$model->success = true; 
									$this->magicData[$this::API_DATA]->success = true;
								}
								else{
									foreach($user->getErrors() as $attribute => $errors){
										$model->addError($attribute, $errors[0]);
									}
								}
							} else {
								$model->addError('old_password', Yii::$app->t('error', 'Неверный пароль'));
							}
							
						} 
					} else {
						//Задаем значения по умолчанию
						//Выводятся при старте страницы
						$model->name = $user->name;
						$model->phone = $user->phone;
						$jsonData = json_decode($user->jsonData);
						$model->address = (isSet($jsonData->address))?$jsonData->address:'';
					}
					
					if($model->success == false){
						$messages = [];
						foreach($model->getErrors() as $field => $message){ 
							$messages[$field] = $message[0]; 
						}
						$this->magicData[Yii::$app->controller::API_DATA]->messages = $messages;
					}
				}
				
				
			
				return true;
			} else{ 
				throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'У вас нет доступа к этой странице.')); 
			}
			
			return false;
		}
		
		return true;
	}
	
	 /**
     * Входной скрипт
     * @return mixed
     */
    public function actionIndex()
    {
	//die("<pre>".print_r($this->page->fields_ru->attributes, true)."</pre>");

		return $this->render($this->pathTemplate, ['page' => $this->page, 'magicData' => $this->magicData]);
	}
	
	/**
     * Отправка на почту проверочного кода
     * @return mixed
     */
	public function actionSendCode(){
		$request = Yii::$app->request;
		if ($request->isPut){
			$apiData = $this->magicData[$this::API_DATA];
			if(!Yii::$app->user->isGuest){
				$user = Yii::$app->user->identity;
				$options = Yii::$app->actionForm->getDefaultMailForm();
				$interval = 10;
				$time = strtotime("-$interval second");

				if($user->updated_at < $time){
					$verify_code = rand(0,9999);
					$user->verify_code = $verify_code;
					$user->save(false);

					$html = $this->renderPartial('/templates/account/pageL0/_code', ['code' => $verify_code]);
					$messages = Yii::$app->mailer->compose()
						->setFrom($options->from_email)
						->setTo($user->email)
						->setSubject(Yii::$app->t('custom', 'Код подтверждения с сайта').' '.$request->serverName)
						->setHtmlBody($html)
						->send();
						
					$apiData->success = true;
					$apiData->messages[] = Yii::$app->t('custom', 'Код подтверждения отправлен на почту').' '.$user->email;
				} else $apiData->messages[] = Yii::$app->t('error', 'Повторная отправка будет доступна через {n, plural, =0{# секунд} =1{# секунду} one{# секунда} few{# секунды} many{# секунд} other{# сек.}}', ['n' => $user->updated_at - $time]);
			} else $apiData->messages[] = Yii::$app->t('error', 'Вы не идентифицированы');
			
			Yii::$app->commonCustom->returnApi($apiData->success, $apiData->data, $apiData->messages);
		} else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Страница не найдена.'));
	}
}
