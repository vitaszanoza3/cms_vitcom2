<?php
namespace frontend\controllers;

use frontend\system\BaseController;
use common\models\IpLimiter;
use common\models\Page;

/**
 * Site controller
 */
class SiteController extends BaseController
{	
	//
   public function actionTest()
    {
		return $this->render('/test');
	}
}
