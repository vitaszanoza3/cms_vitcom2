<?php
namespace frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;

use frontend\system\BaseController;
use common\models\Page;
use common\models\Fields;
use frontend\models\DynamicForm;

class ComplexController extends BaseController
{

	/**
    * Это прослойка вызывается после валидации формы. 
	* Предназначена для подготовки данных.
	* Срабатывает при любом результате валидации. 
     * @return mixed
     */
	public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) {
			return false;
		}
		
		if (in_array($action->id, ['index', 'api'])) {
			$request = Yii::$app->request;
			$get = $request->get(); 
			if(!isSet($get['func']) && $this->page->level == 1){
			
				$filterModel = $this->page->getForm('filter_apartment', $this->magicData);
				$startLanguage = Yii::$app->params['startLanguage'];
				$language = Yii::$app->language;
				$query = Page::find()
					->fields($this->page->parentTemplate, $language)
					->where('page.parent_id = :id', [':id' => $this->page->id])
					//->andFilterWhere(['f_city' => $filterModel->city])
					->andFilterWhere(['>=', $startLanguage.'.f_number_rooms', $filterModel->number_rooms_min])
					->andFilterWhere(['<=', $startLanguage.'.f_number_rooms', $filterModel->number_rooms_max])
					->andFilterWhere(['>=', $startLanguage.'.f_floor', $filterModel->floor_min])
					->andFilterWhere(['<=', $startLanguage.'.f_floor', $filterModel->floor_max])
					->andFilterWhere(['>=', $startLanguage.'.f_square',  $filterModel->square_min])
					->andFilterWhere(['<=', $startLanguage.'.f_square',  $filterModel->square_max])
					->andFilterWhere(['>=', $language.'.f_price', $filterModel->price_min])
					->andFilterWhere(['<=', $language.'.f_price', $filterModel->price_max])
					->visiblePublished()
					->typeItem()
					->orderBy($language.'.f_price DESC');
					
				
				$provider = new ActiveDataProvider([
					'query' => $query,
					'pagination' => [
						'pageSize' => 2,
					],
				]);

				$request = Yii::$app->request;
				$provider->pagination->route = $request->pathInfo;
				$provider->pagination->params = [
					'page' => $request->get('page'),
				];
				//die('pagination<pre>'.print_r($provider, true).'</pre>');
				
				$children = $provider->getModels();

				$this->magicData[$this::PROVIDER] = $provider;
				$this->magicData['children'] = $children;
				
				//Подготовка данных api
				$apiData = [];
				$parentUrl = '/'.Yii::$app->request->pathInfo;
				foreach($children as $childrenPage){ 
					$apiData['items'][] = [
						'id' => $childrenPage->id,
						'alias' => $childrenPage->alias,
						'title' => $childrenPage->title,
						'url' => str_replace('.html', '/'.$childrenPage->alias.'.html', $parentUrl),
						'html' => $this->renderPartial($this->pathTemplate.'/_rooms', ['childrenPage' => $childrenPage]),
					];
				}
				$this->magicData[$this::API_DATA]->addData($apiData);
			}
		}
		//die('magicData<pre>'.print_r(array_keys($this->magicData), true).'</pre>');
		
		return true;
	}
	
	
	 /**
     * Входной скрипт
     * @return mixed
     */
    public function actionIndex()
    {
		if($this->page->level == 0){
		///////////////////////////////////////////////////////////////////////////////////////////////////////
			$filterModel = new DynamicForm();
			$filterModel->setFields([
				'city' => ['Город', Fields::TYPE_DROPDOWN_LIST,
					'function' => 'aliasData',
					'params' => [
						'alias' => 'cities',
						'type' => Page::TYPE_ITEM,
					],
				],
			]);
			$filterModel->load(Yii::$app->request->post());

			$children = Page::find()
				->fields($this->page->parentTemplate)
				->where('page.parent_id = :id', [':id' => $this->page->id])
				->andFilterWhere(['f_city' => $filterModel->city])
				->visiblePublished()
				->all();
		
			//die('page<pre>'.print_r($this->page->level, true).'</pre>');
			return $this->render($this->pathTemplate, [
				'page' => $this->page, 
				'filterModel' => $filterModel,
				'children' => $children,
			]);
		} elseif($this->page->level == 1){
		///////////////////////////////////////////////////////////////////////////////////////////////////////
				
			return $this->render($this->pathTemplate, ['page' => $this->page, 'magicData' => $this->magicData]);
		} else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Страница не найдена.'));
    }

}
