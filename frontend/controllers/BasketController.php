<?php
namespace frontend\controllers;

use Yii;
use frontend\system\BaseController;
use common\models\User;
use common\models\Page;
use common\models\Fields;
use frontend\models\DynamicForm;
use common\components\AccessFilter;

/**
 * Basket controller
 */
class BasketController extends BaseController
{

	public function behaviors()
    {
       return [];
    }
   
	/**
     * Входной скрипт
     * @return mixed
     */
    public function actionIndex()
    {	
		//Проверка на доступ
		if(Yii::$app->user->can(User::ROLE_USER)){
			
			$produkts = [];
			if(isSet($_SESSION['basket'])){
				$IDs = [];
				foreach($_SESSION['basket'] as $item){
					$IDs[] = $item['id'];
				}

				$models = Page::find()
					->where(['id' => $IDs])
					->typeItem()
					->visiblePublished()
					->all();
					
				foreach($models as $model){
					$produkts[$model->id] = $model;
				}
				
			}
			$this->magicData['produkts'] = $produkts;
		} else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'У вас нет доступа к этой странице.'));

		//die('page<pre>'.print_r($this->page, true).'</pre>');
        return $this->render($this->pathTemplate, ['page' => $this->page, 'magicData' => $this->magicData]);
    }
	
	/**
     * Входной скрипт для api
     * @return mixed
     */
    public function actionApi()
    {
		//Проверка на доступ
		if(Yii::$app->user->can(User::ROLE_USER)){
			$request = Yii::$app->request;
			$get = $request->get(); 
			if(isSet($get['func']) && in_array($get['func'], ['addItem', 'removeItem'])){
				if(isSet($get['id']) && (int)$get['id']){
					//Проверяем товар
					$id = (int)$get['id'];
					//$produkt = Page::easyFindModel($id, 'id');
					$produkt = Page::find()
						->where(['id' => $id])
						->typeItem()
						->visiblePublished()
						->one();
					
					if($produkt){
						//die("<pre>".print_r($produkt, true)."</pre>");
						$messages = [];
						$apiData = [];
						//Добавляем товар
						if($get['func'] == 'addItem'){
							if(isSet($_SESSION['basket'][$id])){
								$_SESSION['basket'][$id]['count']++;
							} else {
								$_SESSION['basket'][$id] = [
									'id' => $produkt->id,
									'parent_id' => $produkt->parent_id,
									'title' => $produkt->title,
									'price' => $produkt->price,
									'count' => 1,
								];
								
							}
							$messages[] = Yii::$app->t('basket', 'Товар добавлен');
							$apiData['count'] = $_SESSION['basket'][$id]['count'];
						} 
						//Убираем товар
						elseif($get['func'] == 'removeItem'){
							if(isSet($_SESSION['basket'][$id]['count'])){
								$_SESSION['basket'][$id]['count']--;
								if($_SESSION['basket'][$id]['count'] < 1) unset($_SESSION['basket'][$id]);
								$messages[] = Yii::$app->t('basket', 'Товар удален');
							}
							if(isSet($_SESSION['basket'][$id]['count'])) $apiData['count'] = $_SESSION['basket'][$id]['count'];
							else $apiData['count'] = 0;
						}
						
						$this->magicData[$this::API_DATA]->success = true;
						$this->magicData[$this::API_DATA]->addData($apiData);
						$this->magicData[$this::API_DATA]->messages = $messages;
						//die("<pre>".print_r($_SESSION, true)."</pre>");
					} else {
						$this->magicData[$this::API_DATA]->messages = [Yii::$app->t('basket', 'Товар не найден')];
					}
				}
			} 
		} else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Страница не найдена.'));
		
		$apiData = $this->magicData[$this::API_DATA];
		Yii::$app->commonCustom->returnApi($apiData->success, $apiData->data, $apiData->messages);
    }
	
	public function actionGetTotalPrice(){
		//Подсчет финальной стоимости
		$total_price = 0;
		if(isSet($_SESSION['basket'])){
			foreach($_SESSION['basket'] as $item){
				$total_price += $item['price'] * $item['count'];
			}
			$apiData['total_price'] = $total_price;
			$this->magicData[$this::API_DATA]->success = true;
			$this->magicData[$this::API_DATA]->addData($apiData);
		}
		
		$apiData = $this->magicData[$this::API_DATA];
		Yii::$app->commonCustom->returnApi($apiData->success, $apiData->data, $apiData->messages);
	}
}
