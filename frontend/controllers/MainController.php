<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\LoginForm;
use frontend\system\BaseController;

/**
 * MainController
 */
class MainController extends BaseController
{

     public function actionLanguage($language)
    {
		if(isSet(Yii::$app->params['language'][$language])){
			Yii::$app->language = $language;
			Yii::$app->session->set('language', Yii::$app->language);
		}
		
		if(Yii::$app->user->returnUrl != '/') 
			return $this->goBack();
		else 
			return Yii::$app->request->referrer ? $this->redirect(Yii::$app->request->referrer) : $this->goHome();
	}

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
