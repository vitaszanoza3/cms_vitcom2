<?php
namespace frontend\controllers;

use Yii;
use frontend\system\BaseController;
use common\models\IpLimiter;
use yii\data\ActiveDataProvider;

use backend\modules\pages\components\UrlTransliterate;
use common\models\Page;
use common\models\Fields;

/**
 * Shop controller
 */
class ShopController extends BaseController
{	
	private $returnApi = null;
		
	public function init()
	{
		parent::init();

		$this->returnApi = (object)[
			'success' => true,
			'data' => [],
			'messages' => [],
		];
	}

	/**
    * Это прослойка вызывается после валидации формы. 
	* Предназначена для подготовки данных.
	* Срабатывает при любом результате валидации. 
     * @return mixed
     */
	public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) {
			return false;
		}
		if (in_array($action->id, ['index', 'api'])) {
		
			if($this->page->type != PAGE::TYPE_ITEM){
				if(!isSet($get['func']) && in_array($this->page->level, [2,3])){

					$filterModel = $this->page->getForm('filter_produkt', $this->magicData);
					$startLanguage = Yii::$app->params['startLanguage'];
					$language = Yii::$app->language;

					$inAlias = [];
					$modelsLink = [];
					if($filterModel->region) $inAlias[] = $filterModel->region;
					if($filterModel->city) $inAlias[] = $filterModel->city;
					if(count($inAlias)){
						$modelsLink = Page::find()->where(['alias' => $inAlias])->all();
					}
					//die("modelsLink<pre>".print_r($modelsLink, true)."</pre>");

					$query = Page::find()
						->fields($this->page->parentTemplate, $language)
						->where('page.parent_id = :id', [':id' => $this->page->id])
						->multyLink($this->page->id, true);
					
					foreach($modelsLink as $model){
						$query->multyLink($model->id);
					}
						
					$query->visiblePublished()
						->typeItem()
						->orderBy($language.'.f_price DESC')
						->andFilterWhere(['like', 'title', '%'.$filterModel->title.'%', false])
						->andFilterWhere(['>=', $language.'.f_price', $filterModel->price_min])
						->andFilterWhere(['<=', $language.'.f_price', $filterModel->price_max]);
					//die("<pre>".print_r($query, true)."</pre>");

					$provider = new ActiveDataProvider([
						'query' => $query,
						'pagination' => [
							'pageSize' => 2,
						],
					]);
						
					$request = Yii::$app->request;
					$provider->pagination->route = $request->pathInfo;
					$provider->pagination->params = [
						'page' => $request->get('page'),
					];
					//die('pagination<pre>'.print_r($provider, true).'</pre>');
					
					$children = $provider->getModels();
					
					//Коректитовка. Или синхронизация с апи
					$IDs = [];
					$models = [];
					foreach($children as $item){
						$IDs[] = $item->card_id;
						$models[$item->card_id] = $item;
					}
					self::syncApi($IDs, $models);

					//Данные адреса для мультисвязки
					$addressServerData = [];
					$address = Page::easyFindModel('address', 'alias');
					$childrenL1 = Page::easyChildren($address->id, Page::VISIBLE_PUBLISHED);
					foreach($childrenL1 as $pageL1){
						$addressServerData[$pageL1->id] = $pageL1;
						$childrenL2 = Page::easyChildren($pageL1->id, Page::VISIBLE_PUBLISHED);
						foreach($childrenL2 as $pageL2){
							$addressServerData[$pageL2->id] = $pageL2;
						}
					}
					//die("addressServerData<pre>".print_r($addressServerData, true)."</pre>");
					foreach($models as $model){
						$region = '';
						$city = '';
						foreach($model->multyLink as $id){
							if(isSet($addressServerData[$id])){
								if($addressServerData[$id]->parent_id == $address->id) $region = $addressServerData[$id]->title;
								else $city = $addressServerData[$id]->title;
							}
						}
						$model->dynamicData['address'] = $region.', '.$city;
					}

					
					$this->magicData[$this::PROVIDER] = $provider;
					$this->magicData['items'] = $models;
					
					//Подготовка данных api
					$apiData = [];
					$parentUrl = '/'.Yii::$app->request->pathInfo;
					foreach($models as $childrenPage){ 
						$apiData['items'][] = [
							'id' => $childrenPage->id,
							'alias' => $childrenPage->alias,
							'title' => $childrenPage->title,
							'url' => str_replace('.html', '/'.$childrenPage->alias.'.html', $parentUrl),
							'html' => $this->renderPartial($this->pathTemplate.'/_produkt', ['page' => $childrenPage]),
						];
					}
					$this->magicData[$this::API_DATA]->addData($apiData);
					//die("apiData<pre>".print_r($apiData, true)."</pre>");

				}
			}else{
				//Если элемент
				//Коректитовка. Или синхронизация с апи
				$IDs = [$this->page->card_id];
				$models[$this->page->card_id] = $this->page;
				self::syncApi($IDs, $models);
				
				//Связываем данные мультисвязки
				$region = '';
				$city = '';
				$IDs = $this->page->multyLink;
				if(count($IDs)){
					$address = Page::easyFindModel('address', 'alias');
					$models = (new \yii\db\Query())
					->select(['id', 
						'parent_id', 
						'title',
						"JSON_EXTRACT(`jsonTitle` , '$.".Yii::$app->language."') as jsonTitle", 
						'path'
					])
					->from(Page::tableName())
					->where(['id' => $IDs])
					->andWhere(['LIKE', 'path', '%|'.$address->id.'|%', false])
					->all();
					//die("models<pre>".print_r($models, true)."</pre>");

					foreach($models as $model){
						if($model['parent_id'] == $address->id) $region = json_decode($model['jsonTitle']);
						else $city = json_decode($model['jsonTitle']);
					}
				}
				$this->page->dynamicData['address'] = $region.', '.$city;
				
			}
		}
		
		return true;
	}

		
	//Импорт с внешнего api
	public function actionUpdate()
    {
	    ini_set('max_execution_time', 20000);
        set_time_limit (20000);
        ini_set('memory_limit', '8192M');
		
		//$update_time = 1617000053;
		$request = Yii::$app->request;
		$get = $request->get(); 
		$page_shop = Page::findPage('shop');
		
		if(isSet($get['update_time'])) $update_time = $get['update_time'];
		else $update_time = $page_shop->update_time;
		
		if(isSet($get['faza'])) $faza = $get['faza'];
		else $faza = 1;
			
		//Собираем данные сервера
		$serverData = [];
		$catalog = Page::findPage('catalog');
		$card_id = $catalog->card_id;
		
		$serverData[$card_id] = [
			'id' => $catalog->id,
			'card_id' => $card_id,
			'tytle' => $catalog->title,
			'model' => $catalog,
		];
		self::addServerData($catalog, $serverData);
	
		$address = Page::findPage('address');
		$card_id = $address->card_id;
		$serverData[$card_id] = [
			'id' => $address->id,
			'card_id' => $card_id,
			'tytle' => $address->title,
			'model' => $address,
		];
		self::addServerData($address, $serverData);
		//die("<pre>".print_r($serverData, true)."</pre>");

		if($faza == 1){
			//Обновление каталога
			$url =  Yii::$app->params['urlApi'].'/shop/catalog.api';
			$curl_get_data = ['update_time' => $update_time];
			$result = Yii::$app->commonCustom->curl_get($url, $curl_get_data);
			//echo "<pre>".print_r(json_decode($result), true)."</pre>";
			$result = json_decode($result);
			//die("<pre>".print_r([$update_time, $result], true)."</pre>");

			if($result->success){
				$root_id = $result->data->root->id;
				$card_id = $root_id;
				if($catalog->card_id != $card_id){
					$catalog->card_id = $card_id;
					$catalog->save();
				}
				
				$success = true;
				if(isSet($result->data->catalog)){
					foreach($result->data->catalog as $apiPageData){
						if(self::saveData($apiPageData->id, $serverData, $result->data->catalog, $root_id) == false){
							$success = false;
							break;
						}
					}
				}
				
				//Если в базе не найден родитель запрашиваем полный каталог
				if(!$success && $update_time > 1){
					return $this->redirect(['/shop/update?faza=1&update_time=1&old_update_time='.$update_time]);
				}
				//die("success=<pre>".print_r([$success, $update_time], true)."</pre>");
				$faza++;
				if(isSet($get['old_update_time'])) $update_time = $get['old_update_time'];

			} else echo "<pre>".print_r($result, true)."</pre>";
			
		} 
		
		if($faza == 2){
			//Обновление адресов
			$url =  Yii::$app->params['urlApi'].'/shop/address.api';
			$curl_get_data = ['update_time' => $update_time];
			$result = Yii::$app->commonCustom->curl_get($url, $curl_get_data);
			//echo "<pre>".print_r(json_decode($result), true)."</pre>"; die();
			$result = json_decode($result);
			if($result->success){
				$root_id = $result->data->root->id;
				$card_id = $root_id;
				if($address->card_id != $card_id){
					$address->card_id = $card_id;
					$address->save();
				}
				
				$success = true;
				if(isSet($result->data->address)){
					foreach($result->data->address as $apiPageData){
						if(self::saveData($apiPageData->id, $serverData, $result->data->address, $root_id) == false){
							$success = false;
							break;
						}
					}
				}

				//Если в базе не найден родитель запрашиваем полный каталог
				if(!$success && $update_time > 1){
					return $this->redirect(['/shop/update?faza=2&update_time=1&old_update_time='.$update_time]);
				}
				//die("success=<pre>".print_r([$success, $update_time], true)."</pre>");
			} else echo "<pre>".print_r($result, true)."</pre>";
			$faza++;
			if(isSet($get['old_update_time'])) $update_time = $get['old_update_time'];
		} 
		
		if($faza == 3){
			//$update_time = 1;
			//Обновление новых и удаленных товаров. А так же товаров у которых смонились родители. Включая multyLink
			$url =  Yii::$app->params['urlApi'].'/shop.api';
			$maxPage = 1;
			$n_page = 1;
			$data = [];
			while ($n_page <= $maxPage) {
				//echo 'n_page='.$n_page.'</br>';
				$curl_get_data = ['update_time' => $update_time, 'page' => $n_page];
				$result = Yii::$app->commonCustom->curl_get($url, $curl_get_data);
				$result = json_decode($result);
				if($result->success){
				
					if(isSet($result->data->items)){
						$apiItems = $result->data->items;
						$serverItems = [];
						
						//Собираем данные сервера
						$IDs = [];
						foreach($apiItems as $item){$IDs[] = $item->id;}
						$models = Page::find()->where(['card_id' => $IDs])->typeItem()->all();
						foreach($models as $model){ $serverItems[$model->card_id] = $model;}

						foreach($apiItems as $apiPageData){
							//Если не найден родитель в базе то ошибка
							if(!isSet($serverData[$apiPageData->parent_id])){
								$this->returnMessage(false, 'Ошибка! '.$apiPageData->title.' Не найдена родительская категория parent_id='.$apiPageData->parent_id, __LINE__);
								continue;
							} 
							
							$parent_id = $serverData[$apiPageData->parent_id]['model']->id;
							
							//multyLink
							$multyLink = [];
							foreach($apiPageData->multy_link as $link_card_id){
								if(isSet($serverData[$link_card_id])){
									$multyLink[] = $serverData[$link_card_id]['model']->id;
								}
							}

							if(isSet($serverItems[$apiPageData->id])){
								//если есть товар в базе сервера
								$model = $serverItems[$apiPageData->id];
								$model->parent_id = $parent_id;
								$model->card_id = $apiPageData->id;
								$model->title = $apiPageData->title;
								$model->position = $apiPageData->position;
								$model->visible = $apiPageData->visible;
								$model->type = Page::TYPE_ITEM;
								$model->multyLink = $multyLink;
								
								$model->save();
								$model = self::attributeBridgeProduct($model, $apiPageData);
								$model->fields->save();
								
								if(count($model->getErrors())){ $this->returnMessage(false, 'Ошибка! '."<pre>".print_r($model->getErrors(), true)."</pre>", __LINE__); die();}//ошибка
								elseif(count($model->fields->getErrors())){ $this->returnMessage(false, 'Ошибка! '."<pre>".print_r($model->fields->getErrors(), true)."</pre>", __LINE__); die();}//ошибка
								else { $this->returnMessage(true, 'Обновлен товар '.$model->title, __LINE__); }
								
							} else {
								//Создаем товар
								$title = $apiPageData->title;
								$model = new Page();
								$model->parent_id = $parent_id;
								$model->card_id = $apiPageData->id;
								$model->title = $title;
								$model->type = Page::TYPE_ITEM;
								$model->multyLink = $multyLink;

								$alias = UrlTransliterate::convertToChpu($title);
								$duplicate = Page::find()
									->where('page.alias = :alias', [':alias' => $alias])
									->one();
								if($duplicate) $alias = UrlTransliterate::convertToChpu($title).strtotime("now");
								$model->alias = $alias;
								
								$model->position = $apiPageData->position;
								$model->visible = $apiPageData->visible;
					
								$model->save();
								$model = self::attributeBridgeProduct($model, $apiPageData);
								$model->fields->save();
								
								if(count($model->getErrors())){ $this->returnMessage(false, 'Ошибка! '."<pre>".print_r($model->getErrors(), true)."</pre>", __LINE__); die();}//ошибка
								elseif(count($model->fields->getErrors())){ $this->returnMessage(false, 'Ошибка! '."<pre>".print_r($model->fields->getErrors(), true)."</pre>", __LINE__); die();}//ошибка
								else { $this->returnMessage(true, 'Создан товар '.$model->title, __LINE__); }
							}
						}
						$maxPage = $result->data->_meta->pageCount;
					}
				}
				$n_page++;
			}
			
			//echo "<pre>".print_r($data[5], true)."</pre>"; 
		}
		
		$page_shop->f_update_time = strtotime('now');
		$page_shop->fields->save(false);
//die("<pre>".print_r($page_shop, true)."</pre>");

		Yii::$app->commonCustom->returnApi($this->returnApi->success, $this->returnApi->data, $this->returnApi->messages);

	}
	
	//Для сохранения страниц каталога и адресов
	private function saveData($card_id, &$serverData, $apiData, $root_id){
		//$this->returnMessage(true, 'card_id '.$card_id, __LINE__);
		if(isSet($serverData[$card_id]) && isSet($apiData->{$card_id})){
			$apiPageData = $apiData->{$card_id};
			$model = $serverData[$apiPageData->id]['model'];
			//die($model->title." <pre>".print_r([$model->hashUpdate, md5(print_r($apiPageData, true)) ], true)."</pre>");
			if($model->hashUpdate != md5(print_r($apiPageData, true))){
			
				$model = self::attributeBridge($model, $apiPageData);
				$model->on_itemParentsID = true;
				
				//Поиск родителя
				if(isSet($serverData[$apiPageData->parent_id])){
					$parentModel = $serverData[$apiPageData->parent_id]['model'];
					$model->parent_id = $parentModel->id;
					$model->save();
					$model->fields->save();
					//die("model<pre>".print_r($model, true)."</pre>");

					if(count($model->getErrors())){ $this->returnMessage(false, 'Ошибка! '."<pre>".print_r($model->getErrors(), true)."</pre>", __LINE__); die();}//ошибка
					elseif(count($model->fields->getErrors())){ $this->returnMessage(false, 'Ошибка! '."<pre>".print_r($model->fields->getErrors(), true)."</pre>", __LINE__); die();}//ошибка
					else { $this->returnMessage(true, 'Обновлен '.$model->title.' card_id='.$card_id, __LINE__); }
					
					//сохраняем hashUpdate
					$model->hashUpdate = md5(print_r($apiPageData, true));
					$model->save(false);
					
				} elseif(isSet($data[$apiPageData->parent_id])) {
					$parent_id = self::saveData($apiPageData->parent_id, $serverData, $apiData, $root_id);
					if($parent_id){
						$model->parent_id = $parent_id;
						$model->save();
						$model->fields->save();
						
						if(count($model->getErrors())){ $this->returnMessage(false, 'Ошибка! '."<pre>".print_r($model->getErrors(), true)."</pre>", __LINE__); die();}//ошибка
						elseif(count($model->fields->getErrors())){ $this->returnMessage(false, 'Ошибка! '."<pre>".print_r($model->fields->getErrors(), true)."</pre>", __LINE__); die();}//ошибка
						else { $this->returnMessage(true, 'Обновлен '.$model->title.' card_id='.$card_id, __LINE__); }

						//сохраняем hashUpdate
						$model->hashUpdate = md5(print_r($apiPageData, true));
						$model->save(false);
						
					} else {
						//ошибка
						$this->returnMessage(false, 'Ошибка! Line 156. Не найден parent_id='.$parent_id, __LINE__);
					}
				} else {
						//ошибка
						$this->returnMessage(false, 'Ошибка! Line 160. Не найден parent_id='.$parent_id, __LINE__);
				}
			}
			
			return $model->id;
			
		} elseif(isSet($apiData->{$card_id})) {
			$apiPageData = $apiData->{$card_id};
			$parent_id = self::saveData($apiPageData->parent_id, $serverData, $apiData, $root_id);
			if($parent_id){
				//Создаем новую
				$title = $apiPageData->title;
				$model = new Page();
				$model->parent_id = $parent_id;
				$model->card_id = $apiPageData->id;
				$model->title = $title;
				
				$alias = UrlTransliterate::convertToChpu($title);
				$duplicate = Page::find()
					->where('page.alias = :alias', [':alias' => $alias])
					->one();
				if($duplicate) $alias = UrlTransliterate::convertToChpu($title).strtotime("now");
				$model->alias = $alias;
				
				$model->position = $apiPageData->position;
				$model->visible = $apiPageData->visible;

				$model->on_itemParentsID = true;

				$model->save();
				$model = self::attributeBridge($model, $apiPageData);
				$model->fields->save();
				//die("<pre>".print_r($model, true)."</pre>");
				if(count($model->getErrors())){ $this->returnMessage(false, 'Ошибка! '."<pre>".print_r($model->getErrors(), true)."</pre>", __LINE__); die();}//ошибка
				elseif(count($model->fields->getErrors())){ $this->returnMessage(false, 'Ошибка! '."<pre>".print_r($model->fields->getErrors(), true)."</pre>", __LINE__); die();}//ошибка
				else { $this->returnMessage(true, 'Создан '.$model->title, __LINE__); }
				
				//сохраняем hashUpdate
				$model->hashUpdate = md5(print_r($apiPageData, true));
				$model->save(false);
					
				$serverData[$card_id] = [
					'id' => $model->id,
					'card_id' => $card_id,
					'tytle' => $model->title,
					'model' => $model,
				];
				
				return $model->id;
			} else {
				//ошибка
				$this->returnMessage(false, 'Ошибка! Не найден parent_id='.$parent_id, __LINE__);
			}
		} elseif($card_id == $root_id){ 
			return $serverData[$card_id]['id'];
		}
		else return false;
		
	}
	
	//Заполняет данные с сервера. Каталог и адрес
	private function addServerData($model, &$serverData, $keyAttr = 'card_id'){
		$children = $model->childrenPublishedPage;
		foreach($children as $page){
			$key = $page->{$keyAttr};
			$serverData[$key] = [
				'id' => $page->id,
				'card_id' => $page->card_id,
				'tytle' => $page->title,
				'model' => $page,
			];
			self::addServerData($page, $serverData);
		}
	}
	
	//Сопоставляет атрибуты апи и серверной модели для каталога
	private function attributeBridge($model, $apiPageData){		
		//key - атрибут апи
		//val - атрибут модели
		//Тут перечисляются только дополнительные поля
		$bridge = [
			'inner_title_api' => 'inner_title',
			'short_text_api' => 'short_text',
			'pic_top_api' => 'pic_top',
		];
		
		foreach($apiPageData as $key => $val){
			if(isSet($bridge[$key])){
				$attr = $bridge[$key];
				if(!$model->fields){
					$model->testingTemplate();
					unset($model->fields);
				}

				$confog = $model->templateConfig();
				if(array_key_exists('f_'.$attr, (array)$model->fields->attributes)){
					if($confog['fields'][$attr]['type'] == Fields::TYPE_URL){
						if($val){ 
							$model->{'f_'.$attr} = ['ссылка на картинку',$val]; 
							//die("<pre>".print_r($model, true)."</pre>");

						}
						else $model->{'f_'.$attr} = null;
					}
					else $model->{'f_'.$attr} = $val;
				}
			}
		}

		return $model;
	}
	
	//Сопоставляет атрибуты апи и серверной модели для товаров
	private function attributeBridgeProduct($model, $apiPageData){		
		//key - атрибут апи
		//val - атрибут модели
		//Тут перечисляются только дополнительные поля
		$bridge = [
			'price' => 'price',
		];
		
		foreach($apiPageData as $key => $val){
			if(isSet($bridge[$key])){
				$attr = $bridge[$key];
				if(!$model->fields){
					$model->testingTemplate();
					unset($model->fields);
				}

				$confog = $model->templateConfig();
				if(array_key_exists('f_'.$attr, (array)$model->fields->attributes)){
					if($confog['fields'][$attr]['type'] == Fields::TYPE_URL){
						if($val){ 
							$model->{'f_'.$attr} = ['ссылка на картинку',$val]; 
							//die("<pre>".print_r($model, true)."</pre>");

						}
						else $model->{'f_'.$attr} = null;
					}
					else $model->{'f_'.$attr} = $val;
				}
			}
		}

		return $model;
	}
	
	private function returnMessage($success, $message, $line){
		//echo '<br/>line:'.$line.' '.$message;
		if($success == false) $this->returnApi->success = false;
		$this->returnApi->messages[] = $message;		
		$this->returnApi->data[] = ['line' => $line, $message];		
	}
	
	//Коректитовка или синхронизация с апи
	private function syncApi($IDs, $models){
		$url = Yii::$app->params['urlApi'].'/shop.api';
		$curl_get_data = ['IDs' => implode(',',$IDs)];
		$result = Yii::$app->commonCustom->curl_get($url, $curl_get_data);
		//echo "<pre>".print_r(json_decode($result), true)."</pre>";
		$result = json_decode($result);
		if($result->success){
			if(isSet($result->data->items)){
				foreach($result->data->items as $apiPageData){
					if(isSet($models[$apiPageData->id])){
						$model = $models[$apiPageData->id];
						if($model->hashUpdate != md5(print_r($apiPageData, true))){
							//Родители не обновляются. Обновление родителей должно происходить в общем обновдении
							$model->title = $apiPageData->title;
							$model->position = $apiPageData->position;
							$model->visible = $apiPageData->visible;
							$model->f_price = $apiPageData->price;
							//сохраняем hashUpdate
							$model->hashUpdate = md5(print_r($apiPageData, true));
							$model->save();
							$model->fields->save();
							//die("<pre>".print_r($model, true)."</pre>");
						}
					}
				}
			}
		
		}
	}
	
	public function actionTest(){
		$page = Page::findPage('address');
		$page->dynamicData['address'] = 'Алматы';
		die("address=<pre>".print_r($page->address, true)."</pre>");
	}
}
