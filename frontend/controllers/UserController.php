<?php
namespace frontend\controllers;

use Yii;
use frontend\system\BaseController;
use common\models\User;
use common\models\Page;
use common\models\Fields;
use frontend\models\DynamicForm;

/**
 * Site controller
 */
class UserController extends BaseController
{
   private $_user;

	 /**
     * Входной скрипт
     * @return mixed
     */
    public function actionIndex()
    {
		$request = Yii::$app->request;
		$post = $request->post(); 
		
		//Обработка формы регистрации
		if($request->isPost && isSet($post['field']) && $post['field'] == 'form_check_in'){ 
			$model = $this->page->getForm('form_check_in', $this->magicData);
			if($model->success){
				$user = new User(['scenario' => 'registration']);
				$user->username = $model->username;
				$user->name = $model->name;
				$user->email = $model->email;
				$user->phone = $model->phone;
				$user->password = $model->password;
				$user->passwordConfirm = $model->password_repeat;
				$user->setPassword($model->password);
				$user->generateAuthKey();
				$user->role = User::ROLE_USER;
				$user->status = User::STATUS_NOT_ACTIVE;
				$user->jsonData = json_encode([
					'name' => $model->name,
					'phone' => $model->phone,
				]);
				if($user->save()){  }
				else{
					//die("<pre>".print_r($user->getErrors(), true)."</pre>");
					$model->success = false;
					foreach($user->getErrors() as $attribute => $errors){
						$model->addError($attribute, $errors[0]);
					}
				}
			}
		}
		
		//Обработка формы аутентификации
		if($request->isPost && isSet($post['field']) && $post['field'] == 'form_authentication'){ 
			$model = $this->page->getForm('form_authentication', $this->magicData);
			if($model->success){
				$model->success = false;
				$user = User::findByUsername($model->username);
				if (!$user || !$user->validatePassword($model->password)) {
					$model->addError('username', Yii::$app->t('error', 'Не верный логин или пароль!'));
				} else {
					$GLOBALS['userRole'] = $user->role;
					$login = Yii::$app->user->login($user, $model->rememberMe ? 3600 * 24 * 30 : 0);
					if($login) {
						if(!Yii::$app->user->isGuest){
							$model->success = true;
							//echo Yii::$app->user->identity->username;
						}
					}
				}
			}
		}
		
		return $this->render($this->pathTemplate, ['page' => $this->page, 'magicData' => $this->magicData]);
	}
	
}
