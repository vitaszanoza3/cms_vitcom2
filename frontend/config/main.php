<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$config = [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
       'request' => [
			'baseUrl'=> '',
            'csrfParam' => '_csrf-frontend',
			'cookieValidationKey' => 'hqIrsdQI_KDrAFk_4g4g-uUGzIaemntx',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
		'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
			'enableStrictParsing' => false,
            'rules' => [
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
				[
					'pattern' => '<controller:\w+>/<action:\w+>/<urlL1>/<urlL2>/<urlL3>/<urlL4>/<urlL5>/<urlL6>',
					'route' => '<controller>/index',
					'defaults' => ['urlL2' => null, 'urlL3' => null, 'urlL4' => null, 'urlL5' => null, 'urlL6' => null],
					'suffix' => '.html',
				],
				[
					'pattern' => '/<urlL1>/<urlL2>/<urlL3>/<urlL4>/<urlL5>/<urlL6>',
					'route' => 'site/index',
					'defaults' => ['urlL2' => null, 'urlL3' => null, 'urlL4' => null, 'urlL5' => null, 'urlL6' => null],
					'suffix' => '.html',
				],
				[
					'pattern' => '<controller:\w+>/<action:\w+>/<urlL1>/<urlL2>/<urlL3>/<urlL4>/<urlL5>/<urlL6>',
					'route' => '<controller>/api',
					'defaults' => ['urlL2' => null, 'urlL3' => null, 'urlL4' => null, 'urlL5' => null, 'urlL6' => null],
					'suffix' => '.api',
				],
				[
					'pattern' => '/<urlL1>/<urlL2>/<urlL3>/<urlL4>/<urlL5>/<urlL6>',
					'route' => 'site/api',
					'defaults' => ['urlL2' => null, 'urlL3' => null, 'urlL4' => null, 'urlL5' => null, 'urlL6' => null],
					'suffix' => '.api',
				],

				'/' => 'site/index',
           ],
		],
		'custom' => [
            'class' => 'frontend\components\CustomComponent',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    //$config['bootstrap'][] = 'gii';
    //$config['modules']['gii'] = [
    //    'class' => 'yii\gii\Module',
    //];
	
}

return $config;
