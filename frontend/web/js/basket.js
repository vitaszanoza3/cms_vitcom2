$(function() {
	MyBasket.updatePrice();
	$('.basket_counter a').bind("click", function (e) {
		var produktId = $(this).parent().attr('data-produktId');
		//console.log(produktId);
		var act = $(this).attr('data-act');
		if(act == 'addItem'){
			MyBasket.addItem(produktId,1);
		}
		else if(act == 'removeItem'){
			MyBasket.removeItem(produktId,1);
		}
	});
});

class MyBasket {
  // методы класса
  constructor() {}
  
  static addItem(id, val){
	var url = '/basket/api/basket.api?func=addItem&id='+id;
	$.get(url, function( data ) {
		if(data.success){
			$('.basket_counter[data-produktid="'+id+'"] span').text(data.data.count);
			MyBasket.updatePrice();
		}
		$('#basket_counter_message').text(data.messages[0]);
	});
	
  }
  
  static removeItem(id, val){
	var url = '/basket/api/basket.api?func=removeItem&id='+id;
	$.get(url, function( data ) {
		if(data.success){
			$('.basket_counter[data-produktid="'+id+'"] span').text(data.data.count);
			MyBasket.updatePrice();
		}
		$('#basket_counter_message').text(data.messages[0]);
	});
  }
  
  static updatePrice(){
	var url = '/basket/get-total-price';
	$.get(url, function( data ) {
		if(data.success){
			$('#basket').removeClass('hidden');
			$('.totalPrice').text(data.data.total_price.toLocaleString());
			//console.log(data.data.total_price);
		} else $('#basket').addClass('hidden');
	});
  }
 }