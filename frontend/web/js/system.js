$(function() {
	
	$( "a[data-metod='autoAjax']" ).on( "click", function() {
		  //console.log( $( this ).attr('href') );
		  var id = $( this ).attr("id");
		  var functionName = $( this ).attr("data-func");
		  console.log(functionName);
		  
		  $.ajax({
			type: "PUT",
			url: $( this ).attr('href'),
			cache: false,
			contentType: false,
			processData: false,
			//data: formData,
			dataType : 'json',
			//dataType: 'html',
			success: function(data){
				//var msg = JSON.parse(msg);
				if (typeof window[functionName] == 'function'){
					//console.log('Да! Функция существует!');
					eval(functionName+'(id, data)');
				} else {
					alert('Создайте функцию '+functionName+'(id, data) для вывода данных');
				}
				
				if (data.success) {
					
				} else {
					//console.log( data );
				}
			}
		});
		return false;
	});
});

function viewMessage(id, data){
	var message = $( '#'+id+'Message' );
	if(message.length && data.messages[0].length){
		message.text(data.messages[0]);
	}
}
