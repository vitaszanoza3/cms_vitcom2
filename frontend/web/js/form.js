$(function() {
	$('input[type_field="TYPE_PHONE"]').inputmask({"mask": "+7 (999) 999-99-99"});
	$('input[type_field="TYPE_DATE"]').inputmask("datetime", {
		inputFormat: "dd/mm/yyyy",
		outputFormat: "dd/mm/yyyy",
		inputEventOnly: true
	});
	
	//Пагинация через ajax. Через форму или напрямую
	//pagination_type_numbers
	var pagination = $('#pagination_type_numbers');
	var n_page = 0;
	var count = 0;
	if (pagination.length){
		var field = pagination.attr('data-field');
		var ajax = pagination.attr('data-ajax');
		
		if(ajax){
			if ($('#'+field).length){
				//Отправка через форму
				$('#pagination_type_numbers li a').bind("click", function (e) {
					if(!$(this).parent().hasClass('active') && !$(this).parent().hasClass('disabled')){
						//console.log($(this).attr('data-page'));
						count = parseInt(pagination.attr('data-count'))-1;
						n_page = parseInt($(this).attr('data-page'));
						MyForm.n_page = n_page;			
						MyForm.view_type = 'set';						
						MyForm.sendForm(field, n_page);
					}
					return false;
				});
			} else {
				//Отправка напрямую
				$('#pagination_type_numbers li a').bind("click", function (e) {
					if(!$(this).parent().hasClass('active') && !$(this).parent().hasClass('disabled')){
						count = parseInt(pagination.attr('data-count'))-1;
						n_page = parseInt($(this).attr('data-page'));
						MyForm.n_page = n_page;
						MyForm.view_type = 'set';
					
						//Меняем стили////////////////////////////////////////////////////////////////////////////////////
						MyForm.paginationCss(n_page);
						/////////////////////////////////////////////////////////////////////////////////////////////////
						
						var urlAction = window.location.pathname;
						urlAction = urlAction.substring(0, urlAction.indexOf('.html')+5); //Обрезаем лишнее
						urlAction = urlAction.replace('.html', '.api');
						urlAction = urlAction.replace('/index/', '/api/');
						
						$.ajax({
							type: "GET",
							url: urlAction+'?page='+(n_page+1),
							cache: false,
							contentType: false,
							processData: false,
							//data: formData,
							dataType : 'json',
							//dataType: 'html',
							success: function(msg){
								//var msg = JSON.parse(msg);
								if (msg.success) {
									var functionName = 'success_'+field;
									if (typeof window[functionName] == 'function'){
										//console.log('Да! Функция существует!');
										eval(functionName+'(msg)');
									} else {
										alert('Создайте функцию '+functionName+'(data) для вывода данных');
									}
								} else {
									console.log( msg );
								}
							}
						});
				
					}
					return false;
				});
				
			}
		}
	}

	//Пагинация через ajax. Через форму или напрямую
	//pagination_type_loader
	var pagination = $('#pagination_type_loader');
	var n_page = 0;
	var count = 0;
	if (pagination.length){
		var field = pagination.attr('data-field');
				
		if ($('#'+field).length){
			//Отправка через форму
			$('#pagination_type_loader').bind("click", function (e) {
				count = parseInt(pagination.attr('data-count'))-1;
				if(MyForm.n_page+1 >= count){ console.log('hide'); pagination.hide(); }

				MyForm.n_page++;
				n_page = MyForm.n_page;
				MyForm.view_type = 'add';
				MyForm.sendForm(field, n_page);
					
				return false;
			});
		} else {
			//Отправка напрямую
			$('#pagination_type_loader').bind("click", function (e) {
				
				count = parseInt(pagination.attr('data-count'))-1;
				if(MyForm.n_page+1 >= count){ console.log('hide'); pagination.hide(); }

				MyForm.n_page++;
				n_page = MyForm.n_page;
				MyForm.view_type = 'add';
				
				var urlAction = window.location.pathname;
				urlAction = urlAction.substring(0, urlAction.indexOf('.html')+5); //Обрезаем лишнее
				urlAction = urlAction.replace('.html', '.api');
				urlAction = urlAction.replace('/index/', '/api/');
				
				$.ajax({
					type: "GET",
					url: urlAction+'?page='+(n_page+1),
					cache: false,
					contentType: false,
					processData: false,
					//data: formData,
					dataType : 'json',
					//dataType: 'html',
					success: function(msg){
						//var msg = JSON.parse(msg);
						if (msg.success) {
							var functionName = 'success_'+field;
							if (typeof window[functionName] == 'function'){
								//console.log('Да! Функция существует!');
								eval(functionName+'(msg)');
							} else {
								alert('Создайте функцию '+functionName+'(data) для вывода данных');
							}
						} else {
							console.log( msg );
						}
					}
				});

				return false;
			});
			
		}
		
	}
	
});

class MyForm {
  // методы класса
  constructor() {}
  static n_page = 0;
  static view_type = 'set';
  static buttonClick(id) {
	return true;
  }
  static buttonClickAjax(id) {
	MyForm.n_page = 0; 
	MyForm.view_type = "set"; 
	MyForm.sendForm(id); 
	return false;
  }
  static paginationCss(n_page) {
		var count = parseInt($('#pagination_type_numbers').attr('data-count'))-1;
		$('#pagination_type_numbers li').removeClass('active');
		$('#pagination_type_numbers li:not(.prev):not(.next) a[data-page='+n_page+']').parent().addClass('active');
		
		var prev = $('#pagination_type_numbers .prev');
		if(n_page == 0) prev.addClass('disabled');
		else prev.removeClass('disabled');
		$('#pagination_type_numbers .prev a').attr('data-page', n_page-1);
		
		var next = $('#pagination_type_numbers .next');
		if(n_page == count) next.addClass('disabled');
		else next.removeClass('disabled');
		$('#pagination_type_numbers .next a').attr('data-page', n_page+1);
		
		var limit = 1;
		var add_start = 0;
		var add_end = 0;
		
		var n_start = n_page - limit;
		if(n_start < 0){ add_end = n_start * -1; n_start = 0;}
		
		var n_end = n_page + limit;
		if(n_end > count){ add_start = n_end - count; n_end = count;}
		
		if(n_start - add_start >= 0) n_start = n_start - add_start;
		if(n_end + add_end <= count) n_end = n_end + add_end;
		
		//console.log(n_start, n_end, count, add_start, add_end, n_page);
		$('#pagination_type_numbers li:not(.prev):not(.next) a').parent().removeClass('hide');
		$('#pagination_type_numbers li:not(.prev):not(.next) a').filter(function () {
			return $(this).data('page') < n_start || $(this).data('page') > n_end;
		}).parent().addClass('hide');
	}
	
  static sendForm(formId, n_page = null) {
	var form = $('#'+formId);
    form.yiiActiveForm('validate', true);
	var modal_but_save = true
			
		// afterValidate
		form.on('afterValidate', function (e, m, eattr) {
			//e.preventDefault();
			if(eattr.length > 0) {
				modal_but_save = false;
			}
			else {
				if(modal_but_save){
					modal_but_save = false;
					
					var urlAction = $("#"+formId).attr('action');
					urlAction = urlAction.substring(0, urlAction.indexOf('.html')+5); //Обрезаем лишнее
					urlAction = urlAction.replace('.html', '.api');
					urlAction = urlAction.replace('/index/', '/api/');
					if(n_page != null) urlAction += '?page='+(n_page+1);
					//console.log('urlAction='+urlAction);

					if (window.FormData === undefined) {
						alert('В вашем браузере FormData не поддерживается')
					} else {
						var formData = new FormData();
						
						$("#"+formId+" textarea[data-type='editor']").each(function( index, element ) { 
							var id = $(this).attr('id');
							var val = CKEDITOR.instances[id].getData();
							$(this).val(val);
							//console.log(element);
						});
		  
						var params = $("#"+formId).serializeArray();
						$.each(params, function (i, val) {
							formData.append(val.name, val.value);
						});
						$("#"+formId+" input[type = 'file']").each(function( index, val ) {
							//console.log( index + ": " + $( this ).text() );
							//console.log( $( this )[0].files );
							formData.append(val.name, $( this )[0].files[0]);
						});
						formData.append('field', formId);
						//console.log( formData );
				 
						$.ajax({
							type: "POST",
							url: urlAction,
							cache: false,
							contentType: false,
							processData: false,
							data: formData,
							dataType : 'json',
							//dataType: 'html',
							success: function(msg){
								//var msg = JSON.parse(msg);
								//console.log( msg );
								$('#'+formId+' .help-block').html('');
								if (msg.success) {
									var type = $('#'+formId).attr('data-type');
									if(type == 'mail'){
										$.each(msg.messages, function (key, messages) {
											if(Array.isArray(messages)){
												$.each(messages, function (i, message) {
													$('#'+formId).append(message+'<br/>');
												});
											} else {
												$('#'+formId).html(messages);
											}
										});
									} else if(type == 'form'){
										var functionName = 'success_'+formId;
										if (typeof window[functionName] == 'function'){
											//console.log('Да! Функция существует!');
											eval(functionName+'(msg)');
											
											//При выводе через форму фильтра меняется количество страниц
											//Поэтому нужно обновить пагинатор
											var pagination = $('#pagination_type_numbers');
											if(pagination.length && msg.data._meta.pageCount !== undefined){
												pagination.attr('data-count', msg.data._meta.pageCount);
												//Меняем стили////////////////////////////////////////////////////////////////////////////////////
												MyForm.paginationCss(MyForm.n_page);
												/////////////////////////////////////////////////////////////////////////////////////////////////
											}
											var pagination = $('#pagination_type_loader');
											if(pagination.length && msg.data._meta.pageCount !== undefined){
												pagination.attr('data-count', msg.data._meta.pageCount);
												if(MyForm.n_page+1 >= msg.data._meta.pageCount){ pagination.hide(); }
												else pagination.show();
											}
										} else {
											alert('Создайте функцию '+functionName+'(data) для вывода данных');
										}
									}
								} else {
									$.each(msg.messages, function (key, messages) {
										if ($('#'+formId+' .field-dynamicform-'+key).length){
											$('#'+formId+' .field-dynamicform-'+key).addClass('has-error');
											if(Array.isArray(messages)){
												$.each(messages, function (i, message) {
													$('#'+formId+' .field-dynamicform-'+key+' .help-block').append(message+'<br/>');
												});
											} else {
												$('#'+formId+' .field-dynamicform-'+key+' .help-block').append(messages);
											}
										} else {
											if(Array.isArray(messages)){
												$.each(messages, function (i, message) {
													$('#'+formId+' .result').append(message+'<br/>');
												});
											} else {
												$('#'+formId+' .result').append(messages);
											}
										}
									});
								}
							}
						});
						
					}
	
				}
			}
        });
		
		 // остановить submit в beforeSubmit
        form.on('beforeSubmit', function(e) {
            e.preventDefault();
			return false;
        });
		   
	modal_but_save = true;	
	//if($("#"+modalId+" .modal-body .has-error").length == 0) {}
	  
	return false;
  }
  
}