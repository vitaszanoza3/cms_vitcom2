<?= $options->full_text ?>
<hr/>
<?php foreach($mailData as $label => $value): ?>
<?php if(!is_array($value)): ?>
	<b><?= $label ?></b>: <?= $value ?><br/>
<?php else: ?>
	<b><?= $label ?></b>: <ul>
	<?php foreach($value as $item): ?>
		<li><?= $item ?></li>
	<?php endforeach ?>
	</ul>
<?php endif ?>
<?php endforeach ?>