<?php
use common\models\Page;
use common\models\Fields;

//Описание полей шаблона
return [
	'title' => 'По умолчанию',
	'levels' => [
		[Page::TYPE_PAGE],
		[Page::TYPE_PAGE],
	],
	'fields' => [
		'inner_title' => [
			'label' => 'Внутренний заголовок',
			'type' => Fields::TYPE_TEXT,
			'levels' => [
				[Page::TYPE_PAGE],
			],
		],
		'short_text' => [
			'label' => 'Описание',
			'type' => Fields::TYPE_FULL_TEXT,
			'levels' => [
				[Page::TYPE_PAGE],
			],
		],
	],
];

?>

