<?php
use common\models\Page;
use common\models\Fields;

//Описание полей шаблона
return [
	'title' => 'Контакты',
	'levels' => [
		[Page::TYPE_PAGE],
		[Page::TYPE_GROUP, Page::TYPE_PAGE],
		[Page::TYPE_PAGE, Page::TYPE_ITEM],
		[Page::TYPE_PAGE, Page::TYPE_ITEM],
	],
	'fields' => [
		'addtess' => [
			'label' => 'Адрес',
			'type' => Fields::TYPE_FULL_TEXT,
			'levels' => [
				0 => [Page::TYPE_PAGE],
				1 => [Page::TYPE_PAGE, Page::TYPE_ITEM],
			],
		],
		'phone' => [
			'label' => 'Телефон',
			'type' => Fields::TYPE_PHONE,
			'levels' => [
				1 => [Page::TYPE_PAGE],
				2 => [Page::TYPE_ITEM],
			],
		],
		'email' => [
			'label' => 'E-mail',
			'type' => Fields::TYPE_EMAIL,
			'levels' => [
				1 => [Page::TYPE_PAGE],
				2 => [Page::TYPE_ITEM],
			],
		],
		'content' => [
			'label' => 'Контент',
			'type' => Fields::TYPE_CK_EDITOR,
			'levels' => [
				0 => [Page::TYPE_PAGE],
				1 => [Page::TYPE_PAGE],
				2 => [Page::TYPE_PAGE, Page::TYPE_ITEM],
			],
		],
		'city' => [
			'label' => 'Город',
			'type' => Fields::TYPE_DROPDOWN_LIST,
			'levels' => [
				1 => [Page::TYPE_PAGE, Page::TYPE_ITEM],
			],
			'function' => Yii::$app->dataListComponent::FUNC_aliasData,
			'params' => [
				'alias' => 'cities',
				'type' => Page::TYPE_ITEM,
			],
		],
		'mailForm1' => [
			'label' => 'Форма отправки писем 1',
			'type' => Fields::TYPE_MAIL_FORM,
			'levels' => [
				0 => [Page::TYPE_PAGE], //Настройки будут храниться в корневой странице
				1 => [Page::TYPE_PAGE],
			],
			'function' => Yii::$app->actionForm::FUNC_ajaxPost_SendMail, 
			'ajax' => false,
			'aliasOptions' => 'kontakty', //Указывает место от куда брать настройки
			'letter_view' => 'default', //Вьюха письма
			'recaptcha' => true,
			'fields' => [
				'name' => ['ФИО', Fields::TYPE_TEXT, 'required' => false],
				'text' => ['Текст', Fields::TYPE_FULL_TEXT],
				'phone' => ['Телефон', Fields::TYPE_PHONE, 'required' => false],
				'email' => ['Электронный адрес', Fields::TYPE_EMAIL, 'required' => false],
				'file' => ['Файл', Fields::TYPE_FILES, 'types' => ['pdf','rar','zip','doc','docx','xls','xlsx', 'mp3'], 'max_size' => 5],
				'img' => ['Картинка', Fields::TYPE_IMAGES, 'required' => false],
				'city' => ['Город', Fields::TYPE_DROPDOWN_LIST, 
					'function' => Yii::$app->dataListComponent::FUNC_aliasData,
					'params' => ['alias' => 'cities','type' => Page::TYPE_ITEM]
				],
				'content' => ['Контент', Fields::TYPE_CK_EDITOR],
				'cb' => ['cb', Fields::TYPE_CHECKBOX, 
					'function' => Yii::$app->dataListComponent::FUNC_defaultData,
					'params' => [
						'values' => [
							'param1' => 'Параметр 1',
							'param2' => 'Параметр 2',
							'param3' => 'Параметр 3',
						],
					],
				],
				'rb' => ['rb', Fields::TYPE_RADIO, 
					'function' => Yii::$app->dataListComponent::FUNC_defaultData,
					'params' => [
						'values' => [
							'yes' => 'да',
							'no' => 'нет',
						],
					],
				],
			],
		],
	],
];

?>
