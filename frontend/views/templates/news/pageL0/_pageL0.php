<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="post">
    <h2><?= Html::encode($page->title) ?></h2>

    <?= HtmlPurifier::process($page->short_text) ?>
	<p><a href="<?= $page->createUrl()->pathIndex ?>"><?= Yii::$app->t('custom', 'подробнее...') ?></a></p>
</div>