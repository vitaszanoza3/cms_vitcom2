<?php
use yii\widgets\ListView;
use yii\widgets\LinkPager;

use common\models\Fields;

$provider = $magicData['provider'];
$pages = $magicData['pages'];
?>

<h2><?= $page->title ?></h2>
<?= $page->short_text ?>
</br>

<div id="news_container">
	<?php 
	foreach($pages as $page): 
		echo $this->render($this->context->pathTemplate.'/_post', ['model' => $page]);
	endforeach;
	?>
</div>
</p>
<script>
	function success_news(data){
		console.log('success_news', data);
		var container = $('#news_container');
		if(MyForm.view_type == 'set') container.html('');
		$.each(data.data.items, function (key, val) {
			container.append(val.html);
		});
	}
</script>
<?= $this->render('/_pagination_type_numbers', ['pagination' => $provider->pagination, 'fieldName' => 'news', 'ajax' => true]); ?>
<p/>
<?= $this->render('/_pagination_type_loader', ['pagination' => $provider->pagination, 'fieldName' => 'news']); ?>
