<?php
use common\models\Fields;
//die("<pre>".print_r($page->url, true)."</pre>");
?>
<h2><?= $page->title ?></h2>
<h3><?= $page->inner_title ?></h2>
<p><?= $page->short_text ?></p>
<p><?= $page->date_display ?></p>
<p><?=Yii::$app->formatter->asDatetime($page->date_display, 'long')?></p>
<p><?= $page->full_text ?></p>
<p><?= $page->email ?></p>
<?php if($page->url): ?>
	<p><a href="<?= $page->url[1] ?>"><?= $page->url[0] ?></a></p>
<?php endif ?>
<p><?= $page->number ?></p>
<p><?= $page->password ?></p>
<p><?= $page->rb ?></p>
<?php foreach($page->gallery as $item): ?>
	<a href="<?= $item->url ?>">
		<img width="120" src="<?= $item->imgSize([120,120])->url ?>" title="<?= $item->title ?>"/>
	</a>
<?php endforeach ?>
<?php foreach($page->gallery2 as $item): ?>
	<a href="<?= $item->url ?>">
		<img width="120" src="<?= $item->imgSize([120,120])->url ?>" title="<?= $item->title ?>"/>
	</a>
<?php endforeach ?>

<?php foreach($page->getJsonField('jsonCillection') as $obj): ?>
	<div>
		<h4><?= $obj->title ?></h4>
		<p><?= $obj->short_text ?></p>
		<p><?= $obj->full_text ?></p>
		<?php if(isSet($obj->url)): ?>
			<p><a href="<?= $obj->url[1] ?>"><?= $obj->url[0] ?></a></p>
		<?php endif ?>
		<?= "<pre>".print_r($obj->cb, true)."</pre>"; ?>

	</div>
<?php endforeach ?>