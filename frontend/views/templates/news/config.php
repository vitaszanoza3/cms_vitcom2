<?php
use common\models\Page;
use common\models\Fields;

//Описание полей шаблона
return [
	'title' => 'Новости',
	'levels' => [
		[Page::TYPE_PAGE, Page::TYPE_ITEM],
	],
	'fields' => [
		'inner_title' => [
			'label' => 'Внутренний заголовок',
			'type' => Fields::TYPE_TEXT,
			'required' => true,
			'levels' => [
				[Page::TYPE_PAGE, Page::TYPE_ITEM],
			],
		],
		'short_text' => [
			'label' => 'Описание',
			'type' => Fields::TYPE_FULL_TEXT,
			'levels' => [
				[Page::TYPE_PAGE, Page::TYPE_ITEM],
			],
		],
		'date_display' => [
			'label' => 'Дата',
			'type' => Fields::TYPE_DATE,
			'required' => true,
			'levels' => [
				[Page::TYPE_PAGE, Page::TYPE_ITEM],
			],
		],
		'full_text' => [
			'label' => 'Контент',
			'type' => Fields::TYPE_CK_EDITOR,
			'required' => true,
			'levels' => [
				[Page::TYPE_ITEM],
			],
		],
		'full_text2' => [
			'label' => 'Контент 2',
			'type' => Fields::TYPE_CK_EDITOR,
			'levels' => [
				[Page::TYPE_ITEM],
			],
		],
		'gallery' => [
			'label' => 'Галлерея',
			'type' => Fields::TYPE_IMAGES,
			'required' => true,
			'levels' => [
				[Page::TYPE_ITEM],
			],
			'limit' => 0,
		],
		'gallery2' => [
			'label' => 'Галлерея 2',
			'type' => Fields::TYPE_IMAGES,
			'required' => false,
			'levels' => [
				[Page::TYPE_ITEM],
			],
			'limit' => 3,
		],
		'docs' => [
			'label' => 'Документы',
			'type' => Fields::TYPE_FILES,
			'levels' => [
				[Page::TYPE_ITEM],
			],
			'limit' => 1,
		],
		'jsonCillection' => [
			'label' => 'Коллекция',
			'type' => Fields::TYPE_JSON,
			'levels' => [
				[Page::TYPE_ITEM],
			],
			'fields' => [
				'date' => ['Дата', Fields::TYPE_DATE, 'required' => true],
				'short_text' => ['Описание', Fields::TYPE_FULL_TEXT, 'required' => true],
				'full_text' => ['Контент', Fields::TYPE_CK_EDITOR, 'required' => true],
				'city' => ['Город', Fields::TYPE_DROPDOWN_LIST, 
					'function' => Yii::$app->dataListComponent::FUNC_aliasData,
					'params' => [
						'alias' => 'cities',
						'type' => Page::TYPE_ITEM,
					],
					//'required' => true,
				],
				'cb' => ['cb', Fields::TYPE_CHECKBOX,
					'function' => Yii::$app->dataListComponent::FUNC_defaultData,
					'params' => [
						'values' => [
							'param1' => 'Параметр 1',
							'param2' => 'Параметр 2',
							'param3' => 'Параметр 3',
						],
					],
					//'required' => true,
				],
				'rb' => ['rb', Fields::TYPE_RADIO,
					'function' => Yii::$app->dataListComponent::FUNC_defaultData,
					'params' => [
						'values' => [
							'yes' => 'да',
							'no' => 'нет',
						],
					],
					//'required' => true,
				],
				'img' => ['Картинка', Fields::TYPE_FILE, 'required' => false],
				'email' => ['Почта', Fields::TYPE_EMAIL, 'required' => false],
				'url' => ['Ссылка', Fields::TYPE_URL, 'required' => false],
				'number' => ['Номер', Fields::TYPE_INT, 'required' => false],
				'password' => ['Пароль', Fields::TYPE_PASSWORD, 'required' => false],

			],
		],
		'jsonCollection2' => [
			'label' => 'Коллекция 2',
			'type' => Fields::TYPE_JSON,
			//'required' => true,
			'levels' => [
				[Page::TYPE_ITEM],
			],
			'fields' => [
				'date' => ['Дата', Fields::TYPE_DATE, 'required' => true],
				'full_text' => ['Контент', Fields::TYPE_CK_EDITOR, 'required' => false],
				'city' => ['Город', Fields::TYPE_DROPDOWN_LIST, 
					'function' => Yii::$app->dataListComponent::FUNC_aliasData,
					'params' => [
						'alias' => 'cities',
						'type' => Page::TYPE_ITEM,
					],
					//'required' => true,
				],
				'cb' => ['cb', Fields::TYPE_CHECKBOX],
				'cb_list' => ['cb list', Fields::TYPE_CHECKBOX_LIST,
					'function' => Yii::$app->dataListComponent::FUNC_defaultData,
					'params' => [
						'values' => [
							'param1' => 'Параметр 1',
							'param2' => 'Параметр 2',
							'param3' => 'Параметр 3',
						],
					],
					//'required' => true,

				],
				'rb' => ['rb', Fields::TYPE_RADIO,
					'function' => Yii::$app->dataListComponent::FUNC_defaultData,
					'params' => [
						'values' => [
							'yes' => 'да',
							'no' => 'нет',
						],
					],
					//'required' => true,
				],
			],
		],
		'email' => [
			'label' => 'Почта',
			'type' => Fields::TYPE_EMAIL,
			'levels' => [
				[Page::TYPE_ITEM],
			],
		],
		'url' => [
			'label' => 'Ссылка',
			'type' => Fields::TYPE_URL,
			'levels' => [
				[Page::TYPE_ITEM],
			],
		],
		'number' => [
			'label' => 'Номер',
			'type' => Fields::TYPE_INT,
			'levels' => [
				[Page::TYPE_ITEM],
			],
		],
		'password' => [
			'label' => 'Пароль',
			'type' => Fields::TYPE_PASSWORD,
			'levels' => [
				[Page::TYPE_ITEM],
			],
		],
		'cb' => [
			'label' => 'cb',
			'type' => Fields::TYPE_CHECKBOX,
			//'required' => true,
			'levels' => [
				[Page::TYPE_ITEM],
			],
		],
		'cb_list' => [
			'label' => 'cb list',
			'type' => Fields::TYPE_CHECKBOX_LIST,
			//'required' => true,
			'levels' => [
				[Page::TYPE_ITEM],
			],
			'function' => Yii::$app->dataListComponent::FUNC_defaultData,
			'params' => [
				'values' => [
					'param1' => 'Параметр 1',
					'param2' => 'Параметр 2',
					'param3' => 'Параметр 3',
				],
			],
		],
		'rb' => [
			'label' => 'rb',
			'type' => Fields::TYPE_RADIO,
			//'required' => true,
			'levels' => [
				[Page::TYPE_ITEM],
			],
			'function' => Yii::$app->dataListComponent::FUNC_defaultData,
			'params' => [
				'values' => [
					'yes' => 'да',
					'no' => 'нет',
				],
			],
		],

	],
];

?>

