<?php
use common\models\Page;
use common\models\Fields;

//Описание полей шаблона
return [
	'title' => 'Блог',
	'levels' => [
		[Page::TYPE_PAGE],
		[Page::TYPE_PAGE, Page::TYPE_ITEM],
	],
	'fields' => [
		'inner_title' => [
			'label' => 'Внутренний заголовок',
			'type' => Fields::TYPE_TEXT,
			'levels' => [
				1 => [Page::TYPE_PAGE],
			],
		],
		'short_text' => [
			'label' => 'Описание',
			'type' => Fields::TYPE_FULL_TEXT,
			'levels' => [
				0 => [Page::TYPE_PAGE],
				1 => [Page::TYPE_PAGE, Page::TYPE_ITEM],
			],
		],
		'pic_top' => [
			'label' => 'Картинка в шапке',
			'type' => Fields::TYPE_IMAGES,
			'levels' => [
				0 => [Page::TYPE_PAGE],
				1 => [Page::TYPE_PAGE],
			],
			'limit' => 1,
			'required' => true,
		],
	],
];

?>

