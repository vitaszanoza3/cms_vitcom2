<?php
use common\models\Page;
use common\models\Fields;

$provider = $magicData['provider'];
$items = $magicData['items'];
?>
<script>
$(function() {
	//Заполняет выпадающий список городов
	$( "#dynamicform-region" ).change(function() {
		if($( this ).val()){
			var url = '/shop/api/shop/address/' + $( this ).val() + '.api?func=children&pageSize=0';
			$.get(url, function( data ) {
				$('#dynamicform-city option').remove();
				$('#dynamicform-city').append('<option value=""><?= Yii::$app->t('custom', 'Выберите город ...') ?></option>');
				$.each(data.data.children, function(key, value) {
					$('#dynamicform-city').append('<option value="' + value.alias + '">' + value.title + '</option>');
					//console.log(value.title);
				});
			  
			});
		}
	});
});
//Вывод результата фильтра
function success_filter_produkt(data){
	console.log('success_filter_produkt', data);
	var container = $('#produkt_container');
	if(MyForm.view_type == 'set') container.html('');
	$.each(data.data.items, function (key, val) {
		container.append(val.html);
	});
}
</script>

<h2><?= $page->title ?></h2>
<?php if(isSet($page->pic_top)): ?>
<br/><img width="800" src="<?= Yii::$app->commonCustom->imgSize(\Yii::$app->params['urlApi'].$page->pic_top[1], [800, 400])->url; ?>"/>
<?php endif ?>
<br/>
<?= $page->short_text ?>

<hr/>
<?php foreach(Page::easyChildren($page->id) as $children): ?>
	<div>
		<h3><a href="<?= $page->createUrl($children)->pathIndex ?>"><?= $children->title ?></a></h3>
	</div>
<?php endforeach ?>
<hr/>
<?php 
$filterModel = $page->formBegin('filter_produkt',$magicData, "{input}\n{hint}\n{error}"); 
$form = $filterModel->form;
?>
<h4><?= Yii::$app->t('custom', 'Поиск') ?> :</h4>
<p/><br/>
<div class="container">
	<div class="row">
		<div class="col-xs-2">
			<div><b><?= $filterModel->getLabel('region') ?></b>:</div>
		</div>
		<div class="col-xs-4">
			<?= $filterModel->input('region', ['prompt'=>Yii::$app->t('custom', 'Выберите область ...')]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-2">
			<div><b><?= $filterModel->getLabel('city') ?></b>:</div>
		</div>
		<div class="col-xs-4">
			<?= $filterModel->input('city', ['prompt'=>Yii::$app->t('custom', 'Выберите город ...')]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-2">
			<div><b><?= $filterModel->getLabel('title') ?></b>:</div>
		</div>
		<div class="col-xs-4">
			<?= $filterModel->input('title', ['placeholder' => $filterModel->getLabel('title')]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-2">
			<div><b><?= $filterModel->getLabel('price_min') ?></b>:</div>
		</div>
		<div class="col-xs-2">
			<?= $filterModel->input('price_min', ['placeholder' => Yii::$app->t('custom', 'от')]) ?>
		</div>
		<div class="col-xs-2">
			<?= $filterModel->input('price_max', ['placeholder' => Yii::$app->t('custom', 'до')]) ?>
		</div>
	</div>
</div>
<?= $filterModel->submitButton(Yii::$app->t('custom', 'Найти'), ['class' => 'btn btn-success']); ?>
<?php $page->formEnd(); ?>
<hr/>
<div id="produkt_container" class="container">
<?php foreach($items as $item) echo $this->render($this->context->pathTemplate.'/_produkt', ['page' => $item]) ?>
</div>
<?= $this->render('/_pagination_type_numbers', ['pagination' => $provider->pagination, 'fieldName' => 'filter_produkt', 'ajax' => true]); ?>
