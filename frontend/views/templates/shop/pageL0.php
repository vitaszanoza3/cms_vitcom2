<?php
use common\models\Page;
use common\models\Fields;
$catalog = Page::findPage('catalog');

?>
<h2><?= $page->title ?></h2>
<br/>
<?= $page->short_text ?>

<hr/>
<?php foreach(Page::easyChildren($catalog->id) as $children): ?>
	<div>
		<h3><a href="<?= $page->createUrl($children)->pathIndex ?>"><?= $children->title ?></a></h3>
	</div>
<?php endforeach ?>
