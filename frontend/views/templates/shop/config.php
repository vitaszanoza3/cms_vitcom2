<?php
use common\models\Page;
use common\models\Fields;

//Описание полей шаблона
return [
	'title' => 'Магазин2',
	'levels' => [
		[Page::TYPE_PAGE],
		[Page::TYPE_GROUP],
		[Page::TYPE_PAGE],
		[Page::TYPE_PAGE, Page::TYPE_ITEM],
		[Page::TYPE_PAGE],
	],
	'fields' => [
		'inner_title' => [
			'label' => 'Внутренний заголовок',
			'type' => Fields::TYPE_TEXT,
			'levels' => [
				0 => [Page::TYPE_PAGE],
				2 => [Page::TYPE_PAGE],
				3 => [Page::TYPE_PAGE, Page::TYPE_ITEM],
				4 => [Page::TYPE_PAGE],
			],
		],
		'short_text' => [
			'label' => 'Описание',
			'type' => Fields::TYPE_FULL_TEXT,
			'levels' => [
				0 => [Page::TYPE_PAGE],
				2 => [Page::TYPE_PAGE],
				3 => [Page::TYPE_PAGE, Page::TYPE_ITEM],
				4 => [Page::TYPE_PAGE],
			],
		],
		'update_time' => [
			'label' => 'Время последнего обновления',
			'type' => Fields::TYPE_INT,
			'levels' => [
				[Page::TYPE_PAGE],
			],
		],
		'time_created_or_delete' => [
			'label' => 'Время создания или удаления',
			'type' => Fields::TYPE_DATE,
			'levels' => [
				3 => [Page::TYPE_ITEM],
			],
		],
		'filter_produkt' => [
			'label' => 'Фильтр товаров',
			'type' => Fields::TYPE_FORM,
			'levels' => [
				0 => [Page::TYPE_PAGE], //Настроки
				2 => [Page::TYPE_PAGE],
				3 => [Page::TYPE_PAGE],
			],
			'ajax' => true,
			'aliasOptions' => 'shop', //Указывает место от куда брать настройки
			'function' => Yii::$app->actionForm::FUNC_createAndValidateForm, 
			'fields' => [
				'title' => ['Название', Fields::TYPE_TEXT],
				'region' => ['Область', Fields::TYPE_DROPDOWN_LIST,
					'function' => Yii::$app->dataListComponent::FUNC_aliasData,
					'params' => ['alias' => 'address','type' => Page::TYPE_PAGE, 'key' => 'alias']
				],
				'city' => ['Город', Fields::TYPE_DROPDOWN_LIST,
					'function' => Yii::$app->dataListComponent::FUNC_defaultData,
					'params' => [
						'values' => [
						],
					],
				],
				'price_min' => ['Цена', Fields::TYPE_TEXT],
				'price_max' => ['Цена', Fields::TYPE_TEXT],
			],
		],
		'pic_top' => [
			'label' => 'Картинка в шапке',
			'type' => Fields::TYPE_URL,
			'levels' => [
				2 => [Page::TYPE_PAGE],
				3 => [Page::TYPE_PAGE, Page::TYPE_ITEM],
				4 => [Page::TYPE_PAGE],
			],
			'limit' => 1,
			'required' => false,
		],
		'price' => [
			'label' => 'Цена',
			'type' => Fields::TYPE_DOUBLE,
			'levels' => [
				3 => [Page::TYPE_ITEM],
			],
		],
	],
];

?>

