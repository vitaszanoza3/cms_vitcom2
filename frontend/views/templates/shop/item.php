<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h2><?= $page->title ?></h2>
<?= $page->address ?>
<p><b><?= $page->getLabel('price') ?></b>: <?= $page->price ?></p>

<?php if(!Yii::$app->user->isGuest): ?>
<h3>
<div class="basket_counter" data-produktId = "<?= $page->id ?>">

	<a href="#" title="Убрать из корзины" data-act="removeItem"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-cart-dash-fill" viewBox="0 0 16 16">
	  <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM6.5 7h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1 0-1z"></path>
	</svg></a>
	< <b><span><?= (isSet($_SESSION['basket'][$page->id]['count']))?$_SESSION['basket'][$page->id]['count']:0 ?></span></b> >
	<a href="#" title="Добавить в корзину" data-act="addItem"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-cart-plus-fill" viewBox="0 0 16 16">
	  <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM9 5.5V7h1.5a.5.5 0 0 1 0 1H9v1.5a.5.5 0 0 1-1 0V8H6.5a.5.5 0 0 1 0-1H8V5.5a.5.5 0 0 1 1 0z"></path>
	</svg></a>

</div>
</h3>
<div id="basket_counter_message"></div>

<?php else: ?>
	Для добавления товара в корзину пройдите аутентификацию
	<a data-toggle="modal" data-target="#modal_login" style="cursor: pointer;">Вход</a>
<?php endif ?>