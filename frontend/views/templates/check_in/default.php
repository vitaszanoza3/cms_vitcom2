<?php
use common\models\Fields;
?>
<h2><?= $page->title ?></h2>
<?= $page->short_text ?>

<?php
$formModel = $page->formBegin('form_check_in',$magicData, "{label}\n{input}\n{hint}\n{error}"); 
if($formModel->success): echo $formModel->options->success;
echo '<p><a href="/login.html">Вход</a>';
else:
//die("<pre>".print_r($formModel, true)."</pre>");
foreach($formModel->attributes as $fieldName => $value) echo $formModel->input($fieldName, ['placeholder' => $formModel->getLabel($fieldName)]);
?>
<?= $formModel->input('recaptcha'); ?>
<?= ''//$formModel->form->errorSummary($formModel); ?>
<?= $formModel->submitButton(Yii::$app->t('custom', 'Сохранить'), ['class' => 'btn btn-success']); ?>
<?php 
endif;
$page->formEnd();
