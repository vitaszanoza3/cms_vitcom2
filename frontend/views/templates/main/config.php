<?php
use common\models\Page;
use common\models\Fields;

//Описание полей шаблона
return [
	'title' => 'Главный',
	'levels' => [
		[Page::TYPE_PAGE],
		[Page::TYPE_PAGE],
	],
	'fields' => [
		'defaultMailForm' => [
			'label' => 'Форма отправки писем (по умолчанию)',
			'type' => Fields::TYPE_MAIL_FORM,
			'levels' => [
				0 => [Page::TYPE_PAGE],
			],
			'function' => Yii::$app->actionForm::FUNC_ajaxPost_SendMail, //Функция обработки POST запроса
			'ajax' => true,
			'recaptcha' => true,
			'fields' => [
				'name' => ['ФИО', Fields::TYPE_TEXT, 'required' => true],
				'text' => ['Текст', Fields::TYPE_FULL_TEXT],
				'doc' => ['Контент', Fields::TYPE_CK_EDITOR],
				'phone' => ['Телефон', Fields::TYPE_PHONE, 'required' => true],
				'email' => ['Электронный адрес', Fields::TYPE_EMAIL],
				'file' => ['Файл', Fields::TYPE_FILES, 'max_size' => 5],
			],
		],
		'form_authentication' => [
			'label' => 'Вход',
			'type' => Fields::TYPE_FORM,
			'levels' => [
				0 => [Page::TYPE_PAGE],
			],
			'ajax' => true,
			'recaptcha' => true,
			//'aliasOptions' => 'complex', //Указывает место от куда брать настройки
			'function' => Yii::$app->actionForm::FUNC_ajaxPost_FormAuthentication, 
			'fields' => [
				'username' => ['Логин', Fields::TYPE_TEXT, 'required' => true],
				'password' => ['Пароль', Fields::TYPE_PASSWORD, 'required' => true],
				'rememberMe' => ['Запомнить', Fields::TYPE_CHECKBOX, 'required' => false],
			],
			//'rules' => [
			//],
		],
		'inner_title' => [
			'label' => 'Внутренний заголовок',
			'type' => Fields::TYPE_TEXT,
			'levels' => [
				0 => [Page::TYPE_PAGE],
				1 => [Page::TYPE_PAGE],
			],
		],
		'short_text' => [
			'label' => 'Описание',
			'type' => Fields::TYPE_FULL_TEXT,
			'levels' => [
				0 => [Page::TYPE_PAGE],
				1 => [Page::TYPE_PAGE],
			],
		],
		'city' => [
			'label' => 'Город',
			'type' => Fields::TYPE_DROPDOWN_LIST,
			'levels' => [
				0 => [Page::TYPE_PAGE],
			],
			'function' => Yii::$app->dataListComponent::FUNC_aliasData,
			'params' => [
				'alias' => 'cities',
				'type' => Page::TYPE_ITEM,
			],
		],
	],
];

?>
