<?php
use common\models\Page;
use common\models\Fields;

//Описание полей шаблона
return [
	'title' => 'Вход',
	'levels' => [
		[Page::TYPE_PAGE],
	],
	'fields' => [
		'inner_title' => [
			'label' => 'Внутренний заголовок',
			'type' => Fields::TYPE_TEXT,
			'levels' => [
				[Page::TYPE_PAGE],
			],
		],
		'short_text' => [
			'label' => 'Описание',
			'type' => Fields::TYPE_FULL_TEXT,
			'levels' => [
				[Page::TYPE_PAGE],
			],
		],
		'form_authentication' => [
			'label' => 'Вход',
			'type' => Fields::TYPE_FORM,
			'levels' => [
				0 => [Page::TYPE_PAGE],
			],
			'ajax' => false,
			'recaptcha' => true,
			//'aliasOptions' => 'complex', //Указывает место от куда брать настройки
			'function' => Yii::$app->actionForm::FUNC_createAndValidateForm, 
			'fields' => [
				'username' => ['Логин', Fields::TYPE_TEXT, 'required' => true],
				'password' => ['Пароль', Fields::TYPE_PASSWORD, 'required' => true],
				'rememberMe' => ['Запомнить', Fields::TYPE_CHECKBOX, 'required' => false],
			],
			//'rules' => [
			//],
		],
	],
];

?>

