<?php
use common\models\Fields;
?>
<h2><?= $page->title ?></h2>
<?= $page->short_text ?>

<?php
$formModel = $page->formBegin('form_authentication',$magicData, "{label}\n{input}\n{hint}\n{error}"); 
if($formModel->success): echo $formModel->options->success.' '.Yii::$app->user->identity->username;
else:
//die("<pre>".print_r($formModel, true)."</pre>");
foreach($formModel->attributes as $fieldName => $value) echo $formModel->input($fieldName, ['placeholder' => $formModel->getLabel($fieldName)]);
?>
<?= $formModel->input('recaptcha'); ?>
<?= ''//$formModel->form->errorSummary($formModel); ?>
<?= $formModel->submitButton(Yii::$app->t('custom', 'Войти'), ['class' => 'btn btn-success']); ?>
<?php 
endif;
$page->formEnd();
?>

<p>
<?php if(Yii::$app->user->isGuest): ?>
<a href="/check_in.html">Регистрация</a>
<?php else: ?>
<a href="/account.html">Личный кабинет</a>
<?php endif; ?>