<?php
use common\models\Page;
use common\models\Fields;

//Описание полей шаблона
return [
	'title' => 'Личный кабинет',
	'levels' => [
		[Page::TYPE_PAGE],
	],
	'fields' => [
		'inner_title' => [
			'label' => 'Внутренний заголовок',
			'type' => Fields::TYPE_TEXT,
			'levels' => [
				[Page::TYPE_PAGE],
			],
		],
		'short_text' => [
			'label' => 'Описание',
			'type' => Fields::TYPE_FULL_TEXT,
			'levels' => [
				[Page::TYPE_PAGE],
			],
		],
		'form_code' => [
			'label' => 'Форма кода подтверждения',
			'type' => Fields::TYPE_FORM,
			'levels' => [
				0 => [Page::TYPE_PAGE],
			],
			'ajax' => false,
			'recaptcha' => true,
			//'aliasOptions' => 'complex', //Указывает место от куда брать настройки
			'function' => Yii::$app->actionForm::FUNC_createAndValidateForm, 
			'fields' => [
				'code' => ['Код потверждения', Fields::TYPE_TEXT, 'required' => true],
			],
			//'rules' => [
			//],
		],
		
		'form_user' => [
			'label' => 'Форма пользователя',
			'type' => Fields::TYPE_FORM,
			'levels' => [
				0 => [Page::TYPE_PAGE],
			],
			'ajax' => true,
			'recaptcha' => true,
			//'aliasOptions' => 'complex', //Указывает место от куда брать настройки
			'function' => Yii::$app->actionForm::FUNC_createAndValidateForm, 
			'fields' => [
				'name' => ['ФИО', Fields::TYPE_TEXT, 'required' => true],
				'phone' => ['Телефон', Fields::TYPE_PHONE, 'required' => true],
				'address' => ['Адрес', Fields::TYPE_FULL_TEXT, 'required' => true],
				'old_password' => ['Текущий пароль', Fields::TYPE_PASSWORD, 'required' => true],
				'password' => ['Новый пароль', Fields::TYPE_PASSWORD, 'required' => true],
				'password_repeat' => ['Повтор пароля', Fields::TYPE_PASSWORD, 'required' => true],
			],
			'rules' => [
				['password_repeat', 'compare', 'compareAttribute'=>'password', 'message' => 'Пароли не совпадают!'],
			],
		],
	],
];

?>

