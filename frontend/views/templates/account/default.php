<?php
use common\models\User;
?>
<h2><?= $page->title ?>: <?= Yii::$app->user->identity->username ?></h2>
<?= $page->short_text ?>
<?php if(Yii::$app->user->identity->status == User::STATUS_NOT_ACTIVE): ?>
Ваш электронный адрес не подтвержден. 
<p><a id="sendCode" data-metod="autoAjax" data-func="viewMessage" href="/account/send-code">Отправить письмо с кодом подтверждения.</a> 
<br/><span class="text-danger" id="sendCodeMessage"></span>
<p>
<?php
$formModel = $page->formBegin('form_code',$magicData, "{label}\n{input}\n{hint}\n{error}"); 
if($formModel->success): echo $formModel->options->success.' '.Yii::$app->user->identity->username;
else:
//die("<pre>".print_r($formModel, true)."</pre>");
foreach($formModel->attributes as $fieldName => $value) echo $formModel->input($fieldName, ['placeholder' => $formModel->getLabel($fieldName)]);
?>
<?= $formModel->input('recaptcha'); ?>
<?= ''//$formModel->form->errorSummary($formModel); ?>
<?= $formModel->submitButton(Yii::$app->t('custom', 'Отправить'), ['class' => 'btn btn-success']); ?>
<?php 
endif;
$page->formEnd();
?>
<?php else: ?>

<script>
	function success_form_user(data){
		//console.log('success_form_user', data);
		var container = $('#form_userMessage');
		container.html(data.messages[0]);
	}
</script>
<?php
$formModel = $page->formBegin('form_user',$magicData, "{label}\n{input}\n{hint}\n{error}"); 
if($formModel->success): 
?>
<span class="text-success"><?= $formModel->options->success ?></span><p/>
<?php endif ?>
<?php 
//die("<pre>".print_r($formModel, true)."</pre>");
foreach($formModel->attributes as $fieldName => $value) echo $formModel->input($fieldName, ['placeholder' => $formModel->getLabel($fieldName)]);
?>
<?= $formModel->input('recaptcha'); ?>
<?= ''//$formModel->form->errorSummary($formModel); ?>
<?= $formModel->submitButton(Yii::$app->t('custom', 'Сохранить'), ['class' => 'btn btn-success']); ?>
<span class="text-success" id="form_userMessage"></span>
<?php 

$page->formEnd();
?>

<?php endif ?>
