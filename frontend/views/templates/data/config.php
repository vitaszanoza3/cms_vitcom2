<?php
use common\models\Page;
use common\models\Fields;

//Описание полей шаблона
return [
	'title' => 'Данные',
	'levels' => [
		[Page::TYPE_GROUP],
		[Page::TYPE_GROUP, Page::TYPE_ITEM],
	],
	'fields' => [
		'inner_title' => [
			'label' => 'Внутренний заголовок',
			'type' => Fields::TYPE_TEXT,
			'levels' => [
				1 =>[Page::TYPE_ITEM],
			],
		],
		'short_text' => [
			'label' => 'Описание',
			'type' => Fields::TYPE_FULL_TEXT,
			'levels' => [
				1 =>[Page::TYPE_ITEM],
			],
		],
	],
];

?>

