<?php
use common\models\Page;
use common\models\Fields;

//Описание полей шаблона
return [
	'title' => 'Жилые комплексы',
	'levels' => [
		[Page::TYPE_PAGE],
		[Page::TYPE_PAGE, Page::TYPE_ITEM],
	],
	'fields' => [
		'slider' => [
			'label' => 'Слайдер',
			'type' => Fields::TYPE_IMAGES,
			'levels' => [
				0 => [Page::TYPE_PAGE],
			],
			'limit' => 0,
		],
		'inner_title' => [
			'label' => 'Внутренний заголовок',
			'type' => Fields::TYPE_TEXT,
			'levels' => [
				0 => [Page::TYPE_PAGE],
				1 => [Page::TYPE_PAGE],
			],
		],
		'short_text' => [
			'label' => 'Описание',
			'type' => Fields::TYPE_FULL_TEXT,
			'levels' => [
				0 => [Page::TYPE_PAGE],
				1 => [Page::TYPE_PAGE],
			],
		],
		'advertising' => [
			'label' => 'Реклама',
			'type' => Fields::TYPE_JSON,
			'levels' => [
				0 => [Page::TYPE_PAGE],
			],
			'fields' => [
				'short_text' => ['Описание', Fields::TYPE_FULL_TEXT, 'required' => true],
				'pic1' => ['Картинка 1', Fields::TYPE_FILE, 'required' => true],
				'pic2' => ['Картинка 2', Fields::TYPE_FILE, 'required' => true],
				'url' => ['Ссылка', Fields::TYPE_URL, 'required' => true],

			],
			'limit' => 3,
		],
		'pic_top' => [
			'label' => 'Картинка в шапке',
			'type' => Fields::TYPE_IMAGES,
			'levels' => [
				1 => [Page::TYPE_PAGE],
			],
			'limit' => 1,
			'required' => true,
		],
		'city' => [
			'label' => 'Город',
			'type' => Fields::TYPE_DROPDOWN_LIST,
			'levels' => [
				1 => [Page::TYPE_PAGE],
			],
			'function' => Yii::$app->dataListComponent::FUNC_aliasData,
			'params' => [
				'alias' => 'cities',
				'type' => Page::TYPE_ITEM,
			],
			'required' => true,
		],
		'filter_apartment' => [
			'label' => 'Фильтр квартир',
			'type' => Fields::TYPE_FORM,
			'levels' => [
				0 => [Page::TYPE_PAGE], //Настроки
				1 => [Page::TYPE_PAGE],
			],
			'ajax' => true,
			'aliasOptions' => 'complex', //Указывает место от куда брать настройки
			'function' => Yii::$app->actionForm::FUNC_createAndValidateForm, 
			'fields' => [
				'number_rooms_min' => ['Кол-во комнат', Fields::TYPE_TEXT],
				'number_rooms_max' => ['Кол-во комнат', Fields::TYPE_TEXT],
				'floor_min' => ['Этаж', Fields::TYPE_TEXT],
				'floor_max' => ['Этаж', Fields::TYPE_TEXT],
				'square_min' => ['Площадь', Fields::TYPE_TEXT],
				'square_max' => ['Площадь', Fields::TYPE_TEXT],
				'price_min' => ['Цена', Fields::TYPE_TEXT],
				'price_max' => ['Цена', Fields::TYPE_TEXT],
			],
		],
		'number_rooms' => [
			'label' => 'Кол-во комнат',
			'type' => Fields::TYPE_INT,
			'levels' => [
				1 => [Page::TYPE_ITEM],
			],
		],
		'floor' => [
			'label' => 'Этаж',
			'type' => Fields::TYPE_INT,
			'levels' => [
				1 => [Page::TYPE_ITEM],
			],
		],
		'square' => [
			'label' => 'Площадь',
			'type' => Fields::TYPE_DOUBLE,
			'levels' => [
				1 => [Page::TYPE_ITEM],
			],
		],
		'price' => [
			'label' => 'Цена',
			'type' => Fields::TYPE_INT,
			'levels' => [
				1 => [Page::TYPE_ITEM],
			],
		],
		'pic' => [
			'label' => 'Картинка',
			'type' => Fields::TYPE_IMAGES,
			'levels' => [
				1 => [Page::TYPE_ITEM],
			],
			'limit' => 1,
		],
	],
];

?>

