<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$provider = $magicData['provider'];
$children = $magicData['children'];
//die("filterModel<pre>".print_r($filterModel->getListVal('city'), true)."</pre>");
?>
<?php foreach($page->pic_top as $item): ?>
	<img width="1100" src="<?= $item->imgSize([1100,300])->url ?>" title="<?= $item->title ?>"/>
<?php endforeach ?>
<h2><?= $page->inner_title ?></h2>
<p><?= $page->short_text ?></p>

<hr/>

<?php 
$filterModel = $page->formBegin('filter_apartment',$magicData, "{input}\n{hint}\n{error}"); 
$form = $filterModel->form;
?>
<script>
	function success_filter_apartment(data){
		console.log('success_filter_apartment', data);
		var container = $('#rooms_container');
		if(MyForm.view_type == 'set') container.html('');
		$.each(data.data.items, function (key, val) {
			container.append(val.html);
		});
	}
</script>
<div class="container">
	<div class="row">
		<div class="col-xs-2">
			<div><b><?= $filterModel->getLabel('number_rooms_min') ?></b>:</div>
			<?= $filterModel->input('number_rooms_min', ['placeholder' => Yii::$app->t('custom', 'от')]) ?>
			<?= $filterModel->input('number_rooms_max', ['placeholder' => Yii::$app->t('custom', 'до')]) ?>
		</div>
		<div class="col-xs-2">
			<div><b><?= $filterModel->getLabel('floor_min') ?></b>:</div>
			<?= $filterModel->input('floor_min', ['placeholder' => Yii::$app->t('custom', 'от')]) ?>
			<?= $filterModel->input('floor_max', ['placeholder' => Yii::$app->t('custom', 'до')]) ?>
		</div>
		<div class="col-xs-2">
			<div><b><?= $filterModel->getLabel('square_min') ?></b>:</div>
			<?= $filterModel->input('square_min', ['placeholder' => Yii::$app->t('custom', 'от')]) ?>
			<?= $filterModel->input('square_max', ['placeholder' => Yii::$app->t('custom', 'до')]) ?>
		</div>
		<div class="col-xs-2">
			<div><b><?= $filterModel->getLabel('price_min') ?></b>:</div>
			<?= $filterModel->input('price_min', ['placeholder' => Yii::$app->t('custom', 'от')]) ?>
			<?= $filterModel->input('price_max', ['placeholder' => Yii::$app->t('custom', 'до')]) ?>
		</div>
	</div>
</div>
<?= $filterModel->submitButton('Отправить', ['class' => 'btn btn-success']); ?>
<?php $page->formEnd(); ?>

<hr/>
<h2><?= Yii::$app->t('custom', 'Планировки') ?></h2>
<hr/>

<div id="rooms_container" class="container">
<?php foreach($children as $childrenPage) echo $this->render($this->context->pathTemplate.'/_rooms', ['childrenPage' => $childrenPage]) ?>
</div>

<?= $this->render('/_pagination_type_numbers', ['pagination' => $provider->pagination, 'fieldName' => 'filter_apartment', 'ajax' => true]); ?>
<p/>
<?= $this->render('/_pagination_type_loader', ['pagination' => $provider->pagination, 'fieldName' => 'filter_apartment']); ?>
