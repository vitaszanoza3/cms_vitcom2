<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$cities = $filterModel->getListVal('city');
//die("filterModel<pre>".print_r($filterModel->getListVal('city'), true)."</pre>");

?>
<h2><?= $page->inner_title ?></h2>
<p><?= $page->short_text ?></p>

<?php $form = ActiveForm::begin([]); ?>
<?= $form->field($filterModel, 'city')->dropDownList($cities, ['prompt' => 'все']); ?>
<?= Html::submitButton('Поиск', ['class' => 'btn btn-success']); ?>
<?php ActiveForm::end(); ?>

<hr/>

<?php foreach($children as $childrenPage): ?>
	<div>
		<h3><a href="<?= $childrenPage->createUrl()->path ?>"><?= $childrenPage->inner_title ?></a></h3>
		<p><?= $childrenPage->short_text ?></p>
		<p><?= Yii::$app->t('custom', 'Город') ?>: <?= $cities[$childrenPage->city] ?></p>
		<?php foreach($childrenPage->pic_top as $item): ?>
			<a href="<?= $item->url ?>">
				<img width="150" src="<?= $item->imgSize([150,150])->url ?>" title="<?= $item->title ?>"/>
			</a>
		<?php endforeach ?>
	</div>
<?php endforeach ?>

<hr/>
<h2><?= Yii::$app->t('custom', 'Реклама') ?></h2>
<hr/>
<?php foreach($page->getJsonField('advertising') as $obj): ?>
	<div>
		<h4><?= $obj->title ?></h4>
		<p><?= $obj->short_text ?></p>
		<?php if($obj->pic1): ?>
			<img width="100" src="<?= Yii::$app->commonCustom->imgSize($obj->pic1, [100])->url ?>" title="<?= $obj->title ?>"/>
		<?php endif ?>
		<?php if($obj->url): ?>
			<p><a href="<?= $obj->url[1] ?>"><?= $obj->url[0] ?></a></p>
		<?php endif ?>
	</div>
<?php endforeach ?>