<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;


//die("filterModel<pre>".print_r($filterModel->getListVal('city'), true)."</pre>");
?>
<?php foreach($page->pic_top as $item): ?>
	<img width="1100" src="<?= $item->imgSize([1100,300])->url ?>" title="<?= $item->title ?>"/>
<?php endforeach ?>
<h2><?= $page->title ?></h2>
<?php foreach($page->pic as $item): ?>
	<a href="<?= $item->url ?>">
		<img width="500" src="<?= $item->imgSize([500])->url ?>" title="<?= $item->title ?>" align="left"/>
	</a><p/>
<?php endforeach ?>
<p><b><?= $page->getLabel('number_rooms') ?></b>: <?= $page->number_rooms ?></p>
<p><b><?= $page->getLabel('floor') ?></b>: <?= $page->floor ?></p>
<p><b><?= $page->getLabel('square') ?></b>: <?= $page->square ?></p>
<p><b><?= $page->getLabel('price') ?></b>: <?= $page->price ?></p>
