<div class="row">
	<div class="col-xs-2">
	<?php foreach($childrenPage->pic as $item): ?>
		<a href="<?= $item->url ?>">
			<img width="200" src="<?= $item->imgSize([200])->url ?>" title="<?= $item->title ?>"/>
		</a><p/>
	<?php endforeach ?>
	</div>
	<div class="col-xs-3">
		<h3><a href="<?= $childrenPage->createUrl()->pathIndex ?>"><?= $childrenPage->title ?></a></h3>
		<p><b><?= $childrenPage->getLabel('number_rooms') ?></b>: <?= $childrenPage->number_rooms ?></p>
		<p><b><?= $childrenPage->getLabel('floor') ?></b>: <?= $childrenPage->floor ?></p>
		<p><b><?= $childrenPage->getLabel('square') ?></b>: <?= $childrenPage->square ?></p>
		<p><b><?= $childrenPage->getLabel('price') ?></b>: <?= $childrenPage->price ?></p>
	</div>
</div>