<?php
$limit = 1;
$add_start = 0;
$add_end = 0;
$n_page = $pagination->page;
$count = ceil($pagination->totalCount/$pagination->pageSize) - 1;

$n_start = $n_page - $limit;
if($n_start < 0){ $add_end = $n_start * -1; $n_start = 0;}

$n_end = $n_page + $limit;
if($n_end > $count){ $add_start = $n_end - $count; $n_end = $count;}

if($n_start - $add_start >= 0) $n_start = $n_start - $add_start;
if($n_end + $add_end <= $count) $n_end = $n_end + $add_end;
//die("<pre>".print_r([$n_start, $n_end, $count], true)."</pre>");

Yii::$app->view->registerJsFile('/js/form.js', ['position' => \yii\web\View::POS_END], 'form');
?>
<ul id="pagination_type_numbers" class="pagination" data-field="<?= $fieldName ?>" data-ajax="<?= $ajax ?>" data-count="<?= ceil($pagination->totalCount/$pagination->pageSize) ?>">
	<li class="prev<?= ($pagination->page == 0)?' disabled':'' ?>"><a href="/<?= $pagination->route ?>?page=<?= ($pagination->page) ?>" data-page="<?= ($pagination->page) ?>">«</a></li>		
	<?php for ($n = 0; $n < ceil($pagination->totalCount/$pagination->pageSize); $n++):?>
		<li class="<?= ($n == $pagination->page)?'active':'' ?><?= ($n < $n_start || $n > $n_end)?' hide':'' ?>"><a href="/<?= $pagination->route ?>?page=<?= ($n+1) ?>" data-page="<?= $n ?>"><?= ($n+1) ?></a></li>
	<?php endfor; ?>
	<li class="next<?= ($pagination->page == ceil($pagination->totalCount/$pagination->pageSize)-1)?' disabled':'' ?>"><a href="/<?= $pagination->route ?>?page=<?= ($pagination->page+2) ?>" data-page="<?= ($pagination->page+1) ?>">»</a></li>
</ul>