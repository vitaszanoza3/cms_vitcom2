<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\Fields;
use frontend\models\DynamicForm;

$filterModel = new DynamicForm();
			$filterModel->setFields([
				'file' => ['Файл', Fields::TYPE_FILES, 'types' => ['pdf','rar','zip','doc','docx','xls','xlsx', 'mp3'], 'max_size' => 5],
			]);
?>

<?php $form = ActiveForm::begin([]); ?>
<?= $form->field($filterModel, 'file')->fileInput(); ?>
<?= Html::submitButton('Поиск', ['class' => 'btn btn-success']); ?>
<?php ActiveForm::end(); 
?>