<?php
Yii::$app->view->registerJsFile('/js/form.js', ['position' => \yii\web\View::POS_END], 'form');
?>
<div id="pagination_type_loader" class="pagination" data-field="<?= $fieldName ?>" data-count="<?= ceil($pagination->totalCount/$pagination->pageSize) ?>">
<span style="cursor: pointer;">Загрузить</span>
</div>