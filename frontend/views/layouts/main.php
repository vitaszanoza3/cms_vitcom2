<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

use common\models\Page;
use frontend\models\DynamicForm;

AppAsset::register($this);

$this->registerJsFile('/js/system.js', ['position' => \yii\web\View::POS_END], 'system');
$this->registerJsFile('/js/basket.js', ['position' => \yii\web\View::POS_END], 'basket');
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<script src="/backend/web/js/jquery.min.js"></script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->t('custom', 'Моя компания'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
	$menuItems = [];
	if($this->context->mainPage){
		foreach(Page::easyChildren($this->context->mainPage->id, Page::VISIBLE_PUBLISHED) as $model){
			$model = (object)$model;
			$createUrl = $this->context->mainPage->createUrl($model);
			if($createUrl->thisAlias == $this->context->page->alias) $menuItems[] = '<li><div>'.$model->title.'</div></li>';
			else $menuItems[] = ['label' => $model->title, 'url' => ['/'.$createUrl->path]];
		}
	}
	/*
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'About', 'url' => ['/site/about']],
        ['label' => 'Contact', 'url' => ['/site/contact']],
    ];
	*/
    if (Yii::$app->user->isGuest) {
        //$menuItems[] = ['label' => 'Вход', 'url' => ['/login.html']];
		$menuItems[] = '<li><a data-toggle="modal" data-target="#modal_login" style="cursor: pointer;">Вход</a></li>';
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выход (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>';
    }
	
	$menuItems[] = '<li><div>'
		. ((Yii::$app->language == 'ru')?'RU':Html::a('RU', Url::to(['/site/language', 'language' => 'ru'])))
		. '|'.((Yii::$app->language == 'en')?'EN':Html::a('EN', Url::to(['/site/language', 'language' => 'en'])))
		. '</div></li>';

	$menuItems[] = '<li><a><svg  data-toggle="modal" data-target="#modal_main" style="cursor: pointer;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
		  <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
		</svg></a></li>';
	
	if(!Yii::$app->user->isGuest){
		$menuItems[] = '<li><a href="/account/index/account.html"><svg style="cursor: pointer;" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
			  <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"></path>
			</svg></a></li>';
	}

	$basket_visible = 'hidden';
	if(isSet($_SESSION['basket'])) $basket_visible = '';
		$menuItems[] = '<li class="'.$basket_visible.'" id="basket"><a href="/basket/index/basket.html"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart2" viewBox="0 0 16 16">
		  <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l1.25 5h8.22l1.25-5H3.14zM5 13a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
		</svg> <span class="totalPrice">0</span> '.Yii::$app->t('custom', 'тг.').'</a> </li>';
	

	
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
	
	

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset(Yii::$app->params['breadcrumbs']) ? Yii::$app->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php
if($this->context->mainPage) $mainPage = $this->context->mainPage;
else $mainPage = Page::findPage('main');

Modal::begin([
	'header' => '<h2>Почтовая форма</h2>',
	//'toggleButton' => [
		//'label' => 'click Modal',
		//'class' => 'btn btn-success',
	//],
	'options' => [
		'id' => 'modal_main',
	],
	'footer' => '',
]);
?>
<?php  $formModel = $mainPage->formBegin('defaultMailForm',[], "{input}\n{hint}\n{error}"); 
foreach($formModel->attributes as $fieldName => $value) echo $formModel->input($fieldName, ['placeholder' => $formModel->getLabel($fieldName)]);
?>
<?= $formModel->input('recaptcha'); ?>
<?= $formModel->submitButton('Отправить', ['class' => 'btn btn-success']); ?>
<?php $mainPage->formEnd() ?>

<?php 
Modal::end(); 

?>


<?php
Modal::begin([
	'header' => '<h2>Вход</h2>',
	//'toggleButton' => [
		//'label' => 'click Modal',
		//'class' => 'btn btn-success',
	//],
	'options' => [
		'id' => 'modal_login',
	],
	'footer' => '',
]);
?>
<script>
	function success_form_authentication(data){
		console.log('success_form_authentication', data);
		var container = $('#form_authentication');
		container.html(data.data.html);
	}
</script>
<?php  $formModel = $mainPage->formBegin('form_authentication',[], "{input}\n{hint}\n{error}"); 
foreach($formModel->attributes as $fieldName => $value) echo $formModel->input($fieldName, ['placeholder' => $formModel->getLabel($fieldName)]);
?>
<?= $formModel->input('recaptcha'); ?>
<?= $formModel->submitButton(Yii::$app->t('custom', 'Вход'), ['class' => 'btn btn-success']); ?>
<?php $mainPage->formEnd() ?>
<p>
<?php if(Yii::$app->user->isGuest): ?>
<a href="/check_in.html">Регистрация</a>
<?php else: ?>
<a href="/account.html">Личный кабинет</a>
<?php endif; ?>
<?php 
Modal::end(); 
?>

<?php $this->endBody() ?>
</body>



</html>
<?php $this->endPage() ?>

