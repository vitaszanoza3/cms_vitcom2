<?php
namespace frontend\system;

use Yii;
use yii\web\TooManyRequestsHttpException;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\filters\RateLimiter;

use common\models\User;
use common\models\IpLimiter;
use common\models\Page;
use common\models\Fields;
use frontend\models\DynamicForm;
use common\components\AnObj;

use yii\db\Expression;
use yii\web\NotFoundHttpException;

/**
 * BaseController
 */
class BaseController extends Controller
{
	const API_DATA = 'apiData';
	const PROVIDER = 'provider';

	public $mainPage;
	public $page;
	protected $pathTemplate;
	protected $magicData = []; //Результат POST запросов
	
	public function getPathTemplate(){
		return $this->pathTemplate;
	}
	
	public function behaviors()
   {
       return [
           'rateLimiter' => [
				'class' => RateLimiter::className(),
				'user' => function() { 
					return new IpLimiter(); 
				},
				'except' => ['error']
           ],
        ];
   }
   
	/**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            //'error' => [
            //    'class' => '\yii\web\ErrorAction',
            //],
        ];
    }
	
	public function actionError()
	{
		$errorHandler = Yii::$app->errorHandler;
		$exception = $errorHandler->exception;
		$message = $exception->getMessage();

		$pos = strrpos(Yii::$app->request->url, '.api');
		if ($pos === false){
			//die("<pre>".print_r($message, true)."</pre>");
			if ($exception !== null) {
				return $this->render('error', ['message' => $message]);
			}
		} else {
			$apiData = [
				'statusCode' => $exception->statusCode,
			];
			Yii::$app->commonCustom->returnApi(false, $apiData, $message);
		}
	}

	public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) {
			return false;
		}
	
		//По умолчанию
		$apiData=new AnObj();
		$apiData->success = false;
		$apiData->data = null;
		$apiData->messages = [];
		$apiData->magicData = &$this->magicData;
		$apiData->addData = function($data){
			if(isSet($this->magicData['provider']) && !isSet($this->magicData['_meta'])){
				$get = Yii::$app->request->get(); 
				$pagination = $this->magicData['provider']->pagination;
				
				header('X-Pagination-Current-Page: '.((int)(isSet($get['page'])?$get['page']:1)));
				header('X-Pagination-Page-Count: '.$pagination->pageCount);
				header('X-Pagination-Per-Page: '.$pagination->pageSize);
				header('X-Pagination-Total-Count: '.$pagination->totalCount);
				
				$data['_meta']['totalCount'] = $pagination->totalCount;
				$data['_meta']['pageCount'] = $pagination->pageCount;
				$data['_meta']['currentPage'] = (int)(isSet($get['page'])?$get['page']:1);
				$data['_meta']['perPage'] = $pagination->pageSize;
				
			}
			if($this->data && $data) $this->data = array_merge($this->data, $data);
			elseif($data) $this->data = $data;
			
		};
		$this->magicData[$this::API_DATA] = $apiData;
			
		//die(Yii::$app->controller->id);
		//die($action->id);
		if (in_array($action->id, ['index', 'api'])) { 	

			$request = Yii::$app->request;
			$pathInfo = str_replace(Yii::$app->controller->id.'/'.$action->id.'/', '', $request->pathInfo);
			$pathInfo = str_replace(['.html', '.api'], '', $pathInfo);
			$url = explode('/', $pathInfo);
			if(!$url[0]) $url[0] = 'main';
			//die('beforeAction<pre>'.print_r($url, true).'</pre>');
			
			$alias = end($url);
			$this->page = Page::findPage($alias);

			if(!$this->page) throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Страница не найдена.'));

			//Редирект на текущий контроллер
			if(Yii::$app->controller->id != $this->page->parentController){
				//die('parentController='.$this->page->parentController);
				//die('/'.$this->page->parentController.'/index/'.$request->pathInfo);
				//Yii::$app->controller->id = $this->page->parentController;
				return $this->redirect(['/'.$this->page->parentController.'/'.$action->id.'/'.$request->pathInfo], 301)->send();
			}

			if($alias != 'main'){
				$this->mainPage = Page::findPage('main');
				if(!$this->mainPage) throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не найдена главная страница с псевдонимом main'));

				$pageUrlObj = $this->page->createUrl();
				if($action->id == 'api') $urlPath = $pageUrlObj->pathApi;
				elseif($action->id == 'index') $urlPath = $pageUrlObj->pathIndex;
				else $urlPath = $pageUrlObj->path;
				//die('beforeAction<pre>'.print_r([$urlPath, '/'.$request->pathInfo], true).'</pre>');
				if($urlPath != '/'.$request->pathInfo) throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Страница не найдена.'));
				
				//breadcrumbs
				//die("<pre>".print_r($pageUrlObj, true)."</pre>");
				if($pageUrlObj->controller == 'site') $urlItem = '';
				else $urlItem = '/'.$pageUrlObj->controller.'/index';
				
				foreach($pageUrlObj->data as $item){
					if($pageUrlObj->thisAlias == $item['alias']){
						Yii::$app->params['breadcrumbs'][] = $item['title'];
					} else {
						$urlItem .= '/'.$item['alias'];
						if($item['type'] != Page::TYPE_GROUP){
							Yii::$app->params['breadcrumbs'][] = ['label' => $item['title'], 'url' => $urlItem.'.html'];
						}
					}
				}

			} else {
				$this->mainPage = $this->page;
			}
				
			//Если группа то берем первого потомка
			if($this->page->type == Page::TYPE_GROUP && $action->id == 'index'){
				$this->page = $this->page->childrenFirst;
				if(!$this->page) throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Страница не найдена.'));
			}

			if(in_array($this->page->visible, [Page::VISIBLE_HIDDEN, Page::VISIBLE_DATA])
				|| in_array($this->page->parentVisible, [Page::VISIBLE_HIDDEN, Page::VISIBLE_DATA])
			){ 
				throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Страница не найдена.'));
			}

			$level = $this->page->type.'L'.$this->page->level;
			
			$path = Yii::getAlias("@frontend/views/templates/").$this->page->parentTemplate.'/'.$level.'.php';
			$pathDefaultItem = Yii::getAlias("@frontend/views/templates/").$this->page->parentTemplate.'/item.php';
			if(file_exists($path)) $this->pathTemplate = '//templates/'.$this->page->parentTemplate.'/'.$level;
			elseif($this->page->type == PAGE::TYPE_ITEM && file_exists($pathDefaultItem)) $this->pathTemplate = '//templates/'.$this->page->parentTemplate.'/item';
			else $this->pathTemplate = '//templates/'.$this->page->parentTemplate.'/default';
			//die($this->pathTemplate);	

			//Вызов функции указанной в конфиге
			if($request->isPost){ 
				//die('post<pre>'.print_r($request->post(), true).'</pre>');
				$post = $request->post(); 
				if(isSet($post['field'])){
					$fieldName = $post['field'];
					$config = $this->page->templateConfig();
					if(isSet($config['fields'][$fieldName])) $config = $config['fields'][$fieldName];
					else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не верное поле '.$fieldName));

					if(isSet($config['function'])){
						$func = $config['function'];
						if(!method_exists(Yii::$app->actionForm, $func)) throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не найден метод '.$func));
						$func = $func;
						Yii::$app->actionForm->$func($fieldName, $this->page, $post, $config, $this->magicData);
					} else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'В конфиге '.$fieldName.' не указанна функция function'));
				} 
			}
			elseif($action->id == 'api' && $request->isGet){
				$get = $request->get(); 
				if(isSet($get['func']) && in_array($get['func'], ['this', 'children', 'items', 'parent'])){
					$func = $get['func'];
					$apiData = [];
					if($func){
						//this. Всегда выводим текущую
						$data = self::_getApiData($this->page, $level, $get);
						$apiData['this'] = $data;
						$this->magicData[$this::API_DATA]->success = true;
						$this->magicData[$this::API_DATA]->addData($apiData);
					}
					
					if($func == 'parent'){
						$data = self::_getApiData($this->page->parent, $level, $get);
						$apiData[$func] = $data;
						$this->magicData[$this::API_DATA]->success = true;
						$this->magicData[$this::API_DATA]->addData($apiData);
					} elseif($func == 'children'){
						$language = Yii::$app->language;
						$query = Page::find()
							->fields($this->page->parentTemplate, $language)
							->where('page.parent_id = :id', [':id' => $this->page->id])
							->typePage()
							->visiblePublishedAndData();
					
						$provider = new ActiveDataProvider([
							'query' => $query,
							'pagination' => [
								'pageSize' => (isSet($get['pageSize']))?(int)$get['pageSize']:Yii::$app->params['pageSize'],
							],
						]);
						if(isSet($get['page'])){
							$provider->pagination->params = [
								'page' => $get['page'],
							];
						}
						
						$children = $provider->getModels();
						$apiData[$func] = [];
						foreach($children as $childrenPage){
							$data = self::_getApiData($childrenPage, $level, $get);
							$apiData[$func][] = $data;
						}
						header('X-Pagination-Current-Page: '.((int)(isSet($get['page'])?$get['page']:1)));
						header('X-Pagination-Page-Count: '.$provider->pagination->pageCount);
						header('X-Pagination-Per-Page: '.$provider->pagination->pageSize);
						header('X-Pagination-Total-Count: '.$provider->pagination->totalCount);
						
						$apiData['_meta']['totalCount'] = $provider->pagination->totalCount;
						$apiData['_meta']['pageCount'] = $provider->pagination->pageCount;
						$apiData['_meta']['currentPage'] = (int)(isSet($get['page'])?$get['page']:1);
						$apiData['_meta']['perPage'] = $provider->pagination->pageSize;
						
						$this->magicData[$this::API_DATA]->success = true;
						$this->magicData[$this::API_DATA]->addData($apiData);
						
					} elseif($func == 'items'){
						$language = Yii::$app->language;
						$query = Page::find()
							->fields($this->page->parentTemplate, $language)
							->where('page.parent_id = :id', [':id' => $this->page->id])
							->typeItem()
							->visiblePublishedAndData();
					
						$provider = new ActiveDataProvider([
							'query' => $query,
							'pagination' => [
								'pageSize' => (isSet($get['pageSize']))?(int)$get['pageSize']:Yii::$app->params['pageSize'],
							],
						]);
						if(isSet($get['page'])){
							$provider->pagination->params = [
								'page' => $get['page'],
							];
						}
						
						$children = $provider->getModels();
						$apiData[$func] = [];
						foreach($children as $childrenPage){
							$data = self::_getApiData($childrenPage, $level, $get);
							$apiData[$func][] = $data;
						}
						header('X-Pagination-Current-Page: '.((int)(isSet($get['page'])?$get['page']:1)));
						header('X-Pagination-Page-Count: '.$provider->pagination->pageCount);
						header('X-Pagination-Per-Page: '.$provider->pagination->pageSize);
						header('X-Pagination-Total-Count: '.$provider->pagination->totalCount);
						
						$apiData['_meta']['totalCount'] = $provider->pagination->totalCount;
						$apiData['_meta']['pageCount'] = $provider->pagination->pageCount;
						$apiData['_meta']['currentPage'] = (int)(isSet($get['page'])?$get['page']:1);
						$apiData['_meta']['perPage'] = $provider->pagination->pageSize;
						
						$this->magicData[$this::API_DATA]->success = true;
						$this->magicData[$this::API_DATA]->addData($apiData);
					}
				}
			}elseif($action->id == 'api' && $request->isOptions){
				$apiData = [];
				$apiData['options'] = [
					'func' => [
						'this',
						'parent',
						'children',
						'items',
					],
					'fields' => [
						'this' => self::_getKeyFields($this->page),
						'parent' => self::_getKeyFields($this->page->parent),
						'children' => self::_getKeyFields($this->page->childrenFirstPage),
						'items' => self::_getKeyFields($this->page->childrenFirstItem),
					],
					'page' => 1,
					'pageSize' => Yii::$app->params['pageSize'],
				];
				$this->magicData[$this::API_DATA]->success = true;
				$this->magicData[$this::API_DATA]->addData($apiData);
			}
			
		}
		
		return true; 
	}
	
    /**
     * Входной скрипт
     * @return mixed
     */
    public function actionIndex()
    {	
		//die('page<pre>'.print_r($this->page, true).'</pre>');
        return $this->render($this->pathTemplate, ['page' => $this->page, 'magicData' => $this->magicData]);
    }

	/**
     * Входной скрипт для api
     * @return mixed
     */
    public function actionApi()
    {
		$apiData = $this->magicData[$this::API_DATA];
		Yii::$app->commonCustom->returnApi($apiData->success, $apiData->data, $apiData->messages);
    }
	
	/*
	* Выводит данные страницы в api
	* @return array
	*/
	private function _getApiData($page, $level, $get){
	
		$data = [
			'id' => $page->id,
			'alias' => $page->alias,
			'title' => $page->title,
			'url' => $page->createUrl()->path,
			'type' => $page->type,
			'visible' => $page->visible,
			'date_display' => $page->date_display,
		];
		$path = Yii::getAlias("@frontend/views/templates/").$page->parentTemplate.'/'.$level.'/_'.$level.'.php';
		if (is_file($path)) {
			$data['html'] = $this->renderPartial($this->pathTemplate.'/_'.$level, ['page' => $page]);
		}
		
		//Вывод дополнительных полей, переданных в запросе
		if(isSet($get['fields'])){
			$fields = explode(',', $get['fields']);
			//die("<pre>".print_r($fields, true)."</pre>");
			$config = $page->templateConfig();
			foreach($fields as $field){
				if(isSet($config['fields'][$field])){
					if($config['fields'][$field]['type'] == Fields::TYPE_JSON){
						$data[$field] = json_decode(\yii\helpers\Html::decode($page->{$field}));
						if($data[$field]){
							foreach($data[$field] as &$collection){
								foreach($collection as $key => $item){
									$collection->{$key} = urldecode($item);
								}
							}
						}
					} else $data[$field] = $page->{$field};
				}
			}
		}
		return $data;				
	}
	
	/*
	* Возвращает имена доступных полей
	* Для api isOptions
	* @return array
	*/
	private function _getKeyFields($page){
		$fields = [];
		if($page){
			$config = $page->templateConfig();
			foreach($config['fields'] as $key => $value){
				$fields[] = $key;
			}
		}
		return $fields;
	}
	
	public function actionLogout()
    {
        \Yii::$app->user->logout();
		return $this->goHome();
    }
	
	public function actionLanguage($language)
    {
		//die("<pre>".print_r(Yii::$app->request, true)."</pre>");

		if(isSet(Yii::$app->params['language'][$language])) Yii::$app->language = $language;
		return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        //return $this->goBack();
    }
}
