<?php
namespace frontend\models;

use Yii;
use yii\jui\DatePicker;
use yii\helpers\Html;

use yii\base\Model;
use common\models\Fields;
use frontend\components\CKEditorWidget;
use frontend\components\RecaptchaWidget;

/**
 * Dynamic Form
 */
class DynamicForm extends Model
{
    public $recaptcha = null;
    public $attributes = array();
	public $form;
	public $success = false; //Говорит что модель прошла валидацию и выполнила все действия
	public $config = null;
	public $fieldName = null; //По сути идентификатор модели. Он же id формы
	public $options = null;
	private $fields = array();

    /**
     * @inheritdoc
     */
    public function rules()
    {		
		$rules = [];

		$new_rules = [];
		$new_rules['safe'][] = 'recaptcha';
		foreach($this->attributes as $fieldName => $value){
			$type = $this->fields[$fieldName][1];
			
			switch($type){
				case 'TYPE_INT': $new_rules['integer'][] = $fieldName; break;
				case 'TYPE_FILES': $new_rules['gallery'][] = $fieldName; break; 
				case 'TYPE_IMAGES': $new_rules['gallery'][] = $fieldName; break; 
				case 'TYPE_DATE': $new_rules['validationDate_display'][] = $fieldName;  break;
				case 'TYPE_DROPDOWN_LIST': $new_rules['in'][] = $fieldName; break;
				case 'TYPE_RADIO': $new_rules['in'][] = $fieldName; break;
				case 'TYPE_CHECKBOX_LIST': $new_rules['arrayVal'][] = $fieldName; break;
				case 'TYPE_EMAIL': $new_rules['email'][] = $fieldName; break;
				case 'TYPE_URL': $new_rules['safe'][] = $fieldName; break;
				case 'TYPE_PASSWORD': $new_rules['password'][] = $fieldName; break;
				//case 'TYPE_CAPTCHA': $new_rules['captcha'][] = $fieldName; break;
				default: $new_rules['string'][] = $fieldName; break;
			}
							
			if(isSet($this->fields[$fieldName]['required']) && $this->fields[$fieldName]['required']){
				$new_rules['required'][] = $fieldName;
			}
		}
		
		foreach($new_rules as $preset => $fields){
			if($preset == 'in'){
				foreach($fields as $fieldName){
					//Если список пуст не делаем валидацию. На случай динамического заполнения
					$range = array_keys(Yii::$app->dataListComponent->{$this->fields[$fieldName]['function']}($this->fields[$fieldName]['params']));
					if(count($range)){
						$rules[] = [$fieldName, $preset, 'range'=> $range];
					} else {
						$rules[] = [$fieldName, 'string', 'min'=>1, 'max'=>40];
					}
				}
			}elseif($preset == 'url'){
				$rules[] = [$fields, $preset, 'defaultScheme' => 'http'];
			}elseif($preset == 'required'){
				$rules[] = [$fields, 'required'];
			}elseif($preset == 'gallery'){
				//Поля галлереи заполняются в контроллере update
			}elseif($preset == 'password'){
				$rules[] = [$fields, 'string', 'min'=>6, 'max'=>40];
			} else{
				$rules[] = [$fields, $preset];
			}
		}
		
		if(isSet($this->config['rules'])){
			$rules = array_merge($rules, $this->config['rules']);
		}
		//die("fields<pre>".print_r($this->fields, true)."</pre>");
		//die("rules<pre>".print_r($rules, true)."</pre>");
        return $rules;
    }
	
	/**
     * Валидатор для сохранения дат
     */
	public function validationDate_display($attribute, $params) 
	{
		//Если не число
		if(!preg_match("|^[\d]+$|", $this->$attribute)){
	
			$date =  explode("/", $this->$attribute);
			//Если содержит 3 элемента то текст. Иначе число
			if(count($date)==3){
				$this->$attribute = strtotime($date[2].'-'.$date[1].'-'.$date[0]);
			}
			else{
				$this->$attribute = strtotime("now");
			}
		}
		else{
			$this->$attribute = strtotime("now");
		}
	}
	
	public function arrayVal($attribute, $params) {
		if (!is_array($this->$attribute)) {
			$this->addError($attribute, 'Значение должно быть массивом!');
		}
	}

	public function setFields($fields){
		$this->fields = $this->fields + $fields;
		foreach($this->fields as $key => $value) $this->attributes[$key] = '';
	}

	public function __get($name) {
		$local_attributes = [
			'isNewRecord',
			'recaptcha',
		];
		if(!in_array($name,$local_attributes)){
			return (isSet($this->attributes[$name]))?$this->attributes[$name]:'';
		}
		else{
			return parent::__get($name);
		}
    }

    public function __set($name, $value) {
		$local_attributes = [
			'isNewRecord',
		];
		if(!in_array($name,$local_attributes) && isSet($this->attributes[$name])){
			$this->attributes[$name] = $value;
		}
		else{
			parent::__set($name, $value);
		}
        
    }

 	public function attributeLabels(){
		$labels = array();
		foreach($this->fields as $key => $value){
			$labels[$key] = Yii::$app->t('fields_DynamicForm', $value[0]);
		}
		return $labels;
	}
	
	public function getLabel($name){
		return $this->attributeLabels()[$name];
	}

	
	/*
	* Получает массив значений соглано кофига
	*/
	public function getListVal($name){
		if(isSet($this->fields[$name]) && isSet($this->fields[$name]['function']) && isSet($this->fields[$name]['params'])){
			return Yii::$app->dataListComponent->{$this->fields[$name]['function']}($this->fields[$name]['params']);
		} else [];
	}
	
	/*
	* Выводит input согласно конфига
	*/
	public function input($fieldName, $options = []){
	
		$result = '';
		if(isSet($this->fields[$fieldName][1])){
			$type = $this->fields[$fieldName][1];
			$form = $this->form;
			
			if(in_array($type, ['TYPE_FILES', 'TYPE_IMAGES', 'TYPE_FILE']) && is_array($this->{$fieldName})){
				$this->{$fieldName} = $this->{$fieldName}['name'];
			}
			
			switch(Fields::preset($type)['field']){
				case 'textInput': $result = $form->field($this, $fieldName)->textInput(['maxlength' => true]+ $options); break;
				case 'urlInput': $result = $form->field($this, $fieldName.'[0]')->textInput(['maxlength' => true, 'placeholder' => 'название ссылки']+ $options); 
					$result = $form->field($this, $fieldName.'[1]')->textInput(['maxlength' => true, 'placeholder' => 'ссылка']+ $options)->label(false);
					break;
				case 'phoneInput': $result = $form->field($this, $fieldName)->textInput(['maxlength' => true, 'type_field' => $type]+ $options); break;
				case 'passwordInput': $result = $form->field($this, $fieldName)->passwordInput($options); break;
				case 'textarea': $result = $form->field($this, $fieldName)->textarea(['rows'=>3,'cols'=>5]+ $options); break;
				case 'ck_editor': $result = $form->field($this, $fieldName)->widget(CKEditorWidget::className(),[]); break;
				case 'dropDownList': $result = $form->field($this, $fieldName)->dropDownList(Yii::$app->dataListComponent->{$this->fields[$fieldName]['function']}($this->fields[$fieldName]['params']), $options); break;
				case 'checkbox': $result = $form->field($this, $fieldName)->checkbox($options); break;
				case 'checkboxList': $result = $form->field($this, $fieldName)->checkboxList(Yii::$app->dataListComponent->{$this->fields[$fieldName]['function']}($this->fields[$fieldName]['params']), $options); break;
				case 'radioList': $result = $form->field($this, $fieldName)->radioList(Yii::$app->dataListComponent->{$this->fields[$fieldName]['function']}($this->fields[$fieldName]['params']), $options); break;
				case 'ckfinder': $result = $form->field($this, $fieldName)->fileInput(); break;
				case 'fileInput': $result = $form->field($this, $fieldName)->fileInput(); break;
				case 'datePicker': $result = $form->field($this, $fieldName)->widget(DatePicker::classname(), [
						 'language' => 'ru',
						 'dateFormat' => 'php:d/m/Y',
						 'options' => [
							'class' => 'form-control',
							'enctype'=>'multipart/form-data',
							'type_field' => $type
						 ],
					 ]); break;

			}
		}
		
		if($fieldName == 'recaptcha' && isSet($this->config['recaptcha']) && $this->config['recaptcha']){
			$result = $this->form->field($this, 'recaptcha')->widget(RecaptchaWidget::className(),[])->label(false);
		}
		
		return $result;
	}
	
	/*
	* Выводит input согласно конфига
	*/
	public function submitButton($name, $options = [], $recaptchaKey = null){
		$result = '';
		if(isSet($this->config['ajax']) && $this->config['ajax'] == true){
			$options['id'] = 'submit';
			$options['onclick'] = 'return MyForm.buttonClickAjax($(this).parent().attr("id"));';
		} else {
			$result .= '<input type="hidden" name="field" value="'.$this->fieldName.'">';
		}
		$result .=  Html::submitButton($name, $options);
		$result .= '<div class="has-error"><div class="result help-block"></div></div>';
		return $result;

	}
}
