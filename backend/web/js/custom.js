$(function() {
	$('input[type_field="TYPE_PHONE"]').inputmask({"mask": "+7 (999) 999-99-99"});
	$('input[type_field="TYPE_DATE"]').inputmask("datetime", {
		inputFormat: "dd/mm/yyyy",
		outputFormat: "dd/mm/yyyy",
		inputEventOnly: true
	});
});

