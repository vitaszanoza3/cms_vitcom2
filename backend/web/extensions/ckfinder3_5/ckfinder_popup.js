$(function() {
	var sortable = $(".fieldGallery > .container");
	
	var options = {
		handle: '.bi-arrow-down-up',
		axis: "y",
		//opacity: 0.5,
		//revert: true,
		//containment: "div",
		//delay: 500,
		//grid: [0,37],
		//tolerance: "pointer",
	};
	sortable.sortable(options);
	
	//Скрываем кнопки загрузки если привышен лимит
	var fieldGallery = $(".fieldGallery");
	var list = Array.prototype.slice.call(fieldGallery);
	list.forEach((element) => {
	  //console.log(element.id);
	  limitControl(element.id);
	});
});
function galleryOpenPopup(id, n, type) {
	var galleryDiv = document.getElementById(id);
	//console.log('openPopup', galleryDiv);
	
	var type2 = '';
	if(type == 'TYPE_IMAGES') type2 = 'Images';
	else type2 = 'Files';
	
	CKFinder.modal( {
		chooseFiles: true,
		type: type2,
		width: 800,
		height: 600,
		onInit: function( finder ) {
			 finder.on( 'files:choose', function( evt ) {
				 var file = evt.data.files.first();
				 //console.log('evt.data', evt.data);
				 console.log('file', file.attributes.name);
				 //console.log('folder', file.attributes.folder.attributes.name);
				 //console.log('_extenstion', file._extenstion);
				 //console.log('limit=', eval('limit_'+ new String(id)));
				 				
				let divRow = document.createElement('div');
				divRow.id = 'row_'+id+'_'+n;
				divRow.className = 'row';
	
				let divCol1 = document.createElement('div');
				divCol1.className = 'col-xs-2';
	
				var folder = file.attributes.folder.attributes.name;
				if(folder == 'Images') folder = '%2F';
				
				let a = document.createElement('a');
				a.href = file.getUrl();

				a.target = '_blank';
				
				let img = document.createElement('img');

				let imgType = ['bmp','gif','jpeg','jpg','png'];
				if(imgType.indexOf(file._extenstion.toLowerCase()) != -1){
					img.src = '/backend/web/site/ckfinder?command=Thumbnail&type=Images&currentFolder='+folder+'&fileName='+file.attributes.name+'&size=150x150';
					if(type != 'TYPE_IMAGES') return;
				} else if(file._extenstion.toLowerCase() == 'svg'){
					img.src = file.getUrl();
					img.width = 130;
					img.title = file.getUrl();
					img.alt = file.getUrl();
				} else {
					img.src = '/backend/web/extensions/ckfinder3_5/skins/neko/file-icons/64/readme.png';
					img.title = file.getUrl();
					img.alt = file.getUrl();
					if(type == 'TYPE_IMAGES') return;
				}

				//galleryDiv.append(img);
				
				let spanSwg = document.createElement('span');
				spanSwg.innerHTML = ' &nbsp<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-down-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.5 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L11 2.707V14.5a.5.5 0 0 0 .5.5zm-7-14a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L4 13.293V1.5a.5.5 0 0 1 .5-.5z"></path></svg>';
				
				a.append(img);
				divCol1.append(a);
				divCol1.append(spanSwg);

				let divCol2 = document.createElement('div');
				divCol2.className = 'col-lg-4';

				let inputHidden = document.createElement('input');
				inputHidden.type = "hidden";
				inputHidden.name = 'gallery['+id+'][url][]';
				inputHidden.id = 'url_'+id+'_'+n;
				//input.innerHTML = "<strong>Всем привет!</strong> Вы прочитали важное сообщение.";
				inputHidden.value = file.getUrl();
				divCol2.append(inputHidden);
				
				let inputHiddenThumbnail = document.createElement('input');
				inputHiddenThumbnail.type = "hidden";
				inputHiddenThumbnail.name = 'gallery['+id+'][thumbnail][]';
				inputHiddenThumbnail.id = 'thumbnail_'+id+'_'+n;
				//input.innerHTML = "<strong>Всем привет!</strong> Вы прочитали важное сообщение.";
				inputHiddenThumbnail.value = img.src;
				divCol2.append(inputHiddenThumbnail);
				//document.getElementById( 'url_'+id+'_'+n ).value = file.getUrl();

				let inputTitle = document.createElement('input');
				inputTitle.type = "text";
				inputTitle.name = 'gallery['+id+'][title][]';
				inputTitle.id = 'title_'+id+'_'+n;
				inputTitle.placeholder = "Заголовок";
				if(type != 'TYPE_IMAGES') inputTitle.value = file.attributes.name;
				divCol2.append(inputTitle);

				let textarea = document.createElement('textarea');
				textarea.rows = "4";
				textarea.name = 'gallery['+id+'][text][]';
				textarea.id = 'text_'+id+'_'+n;
				textarea.placeholder = "Описание";
				divCol2.append(textarea);

				let divCol3 = document.createElement('div');
				divCol3.className = 'col-md-6';
				//divCol3.style.text-align = 'right';
				divCol3.innerHTML = '<svg style="cursor: pointer;" onclick="galleryItemRemove(\'row_'+id+'_'+n+'\')" width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="red" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path></svg>';

				divRow.append(divCol1);
				divRow.append(divCol2);
				divRow.append(divCol3);
				
				var container = galleryDiv.getElementsByClassName('container');
				container[0].append(divRow);
				//console.log('childElementCount', container[0].childElementCount);
				
				//Скрываем кнопку загрузки если достигнут лимит
				limitControl(id);
			
			 } );
			 finder.on( 'file:choose:resizedImage', function( evt ) {
			 	//console.log('evt.data', evt.data);
				//document.getElementById( 'url' ).value = evt.data.resizedUrl;
			 } );
		 }
    } );
			 
}

function galleryItemRemove(id){
	if(confirm("Вы действительно хотите удалить фотографию?")) {
		//console.log('galleryItemRemove id', id);
		var elem = document.getElementById(id);
		var parent_id = elem.parentNode.parentNode.id;
		elem.parentNode.removeChild(elem);
		limitControl(parent_id);
	}
}

//Скрываем кнопку загрузки если достигнут лимит
function limitControl(id){
	//console.log('limitControl ', id);
	var but = document.getElementById('but_'+id);
	//console.log('limitControl but', but);
	if(but){
		var limit = eval('limit_'+ new String(id));
		
		var galleryDiv = document.getElementById(id);
		var container = galleryDiv.getElementsByClassName('container');
		var count = container[0].childElementCount;
		
		var input = document.getElementById('page-'+id);
		
		if(count > 0) input.value = count;
		else input.value = null;
		
		if(limit > 0 && count >= limit){
			but.hidden = true;
		} else {
			but.hidden = false;
		}
	}
}

//Multiple Popups
function selectFileWithCKFinder(elementId) {
	CKFinder.popup( {
		chooseFiles: true,
		width: 800,
		height: 600,
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				//var output = document.getElementById( elementId );
				console.log(elementId);
				var output = elementId;
				output.value = file.getUrl();
			} );

			finder.on( 'file:choose:resizedImage', function( evt ) {
				//var output = document.getElementById( elementId );
				var output = elementId;
				output.value = evt.data.resizedUrl;
			} );
		}
	} );
}