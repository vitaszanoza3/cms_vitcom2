$(function() {
	//console.log();

	var sortable = $(".sortable").find("tbody");
	sortable.attr('id', 'sortable');
	sortable.find("tr").each(function(){
		$(this).attr('id', this.children[0].innerText);
	});
	var options = {
		handle: '.bi-arrow-down-up',
		axis: "y",
		opacity: 0.5,
		revert: true,
		containment: "tbody",
		delay: 500,
		grid: [0,37],
		tolerance: "pointer",
		update: function(event, ui) {
			//alert(ui.item[0].id); 
			//console.log(event); 
			//console.log(ui);
			
			var items = $( "#sortable" ).sortable("toArray");
			var url = '/backend/web/pages/default/update-position-details';
			
			var post = {'items':items, 'nPage':nPage, 'pageSize':pageSize};
			$.post(url, post, function(response){
				console.log(response);
			});			
		}
	};
	
	//Блокируем перенос узлов для едиторов
	if(blockDragAndDrop){options.disabled = true;}

	$( "#sortable" ).sortable(options);

});