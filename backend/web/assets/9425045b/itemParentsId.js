$(function() {
//console.log($('form').attr('action'));

});

function addItemParentId(id) {
console.log(id);
	var val = $('#newParentsId').val();
	if(val.length){
		$.ajax({
			type: "GET",
			url: '/backend/web/pages/default/add-item-parent-id',
			data: 'id='+id+'&addAlias='+val,
			cache: false,
			contentType: false,
			processData: false,
			//data: formData,
			dataType : 'json',
			//dataType: 'html',
			success: function(data){
				//var msg = JSON.parse(msg);
				var container = $('#itemParentsIdMessage');
				if (data.success) {
					container.removeClass("text-danger").addClass("text-success");
					$('#containerItemParentsId').append(data.data.html);
					$('#newParentsId').val('');
					//console.log(data);
				} else {
					container.removeClass("text-success").addClass("text-danger");
					//console.log( data );
				}
				console.log( data );
				container.html(data.messages[0]);
			}
		});
	}
}

function removeItemParentId(id, removeId) {
	if(confirm("Вы действительно хотите удалить связку?")) {
		$.ajax({
			type: "GET",
			url: '/backend/web/pages/default/remove-item-parent-id',
			data: 'id='+id+'&removeId='+removeId,
			cache: false,
			contentType: false,
			processData: false,
			//data: formData,
			dataType : 'json',
			//dataType: 'html',
			success: function(data){
				//var msg = JSON.parse(msg);
				if (data.success) {
					var container = $('#containerItemParentsId .row[data-id="'+data.data.removeId+'"]')
					//console.log(container);
					container.remove();
				} else {
					//container.removeClass("text-success").addClass("text-danger");
					console.log( data );
				}
				
			}
		});
	}
}