$(function() {
//console.log($('form').attr('action'));
//Херов глюк. При добавлении form в модальное окно селектор его не видит. Пришлось подменять div
//$("#f_jsonCillection .modal-body").replaceWith("<form id='test3' action='test' class='modal-body'>"+$('#f_jsonCillection .modal-body').html()+'</form>');

	var sortable = $(".jsonField");
	
	var options = {
		handle: '.bi-arrow-down-up',
		axis: "y",
		start: function(event, ui) {
            var start_pos = ui.item.index();
            ui.item.data('start_pos', start_pos);
        },
		change: function( event, ui ) {
			var new_pos = ui.placeholder.index();
            ui.item.data('new_pos', new_pos);
		},
		update: function(event, ui) {
			var start_pos = ui.item.data('start_pos');
 			var new_pos = ui.item.data('new_pos');
            //console.log('start_pos = '+start_pos +' new_pos='+new_pos);
			//console.log(event.target.getAttribute('data-field'));
			var field_id = event.target.getAttribute('data-field');
			var collection = eval('collection_'+field_id);
			var changeItem = collection[start_pos];
			var newCollection = {};
			var n=0;
			for (var index in collection) {
				if(index == new_pos){ newCollection[n] = collection[start_pos]; n++;}
				if(index != start_pos){ newCollection[n] = collection[index]; n++; }
			}
			index++;
			if(index == new_pos){ newCollection[n] = collection[start_pos]; n++;}
			
			//console.log(newCollection);
			collection = newCollection;
			eval('collection_'+field_id+' = newCollection;') ;
			saveJsonField(field_id, collection);
       }
		//opacity: 0.5,
		//revert: true,
		//containment: "div",
		//delay: 500,
		//grid: [0,37],
		//tolerance: "pointer",
	};
	sortable.sortable(options);
});

function initJsonField(field_id, json) {
	var re = /\n/gi;
	json = json.replace(re,'');
	//console.log(json);
	var collection = {};
	if(json){ 
		try {
			collection = JSON.parse(json);
		} catch (e) {
			console.log('Ошибка парсинга JSON');
		}
	}
	viewJsonFieldPanel(field_id, collection);
	return collection;
}

function viewJsonFieldPanel(field_id, collection) {
	field_id = field_id.toLowerCase();
	var container = $('.field-page-'+field_id+' > .container');
	container.html('');
	
	$.each(collection, function( key, value ) { 
		var rowHtml = '<div class="row" data-index="'+key+'"><div class="col-lg-6">'+decodeURI(value.title)+'</div><div class="col-xs-2"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-down-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="margin: 7px; vertical-align: top;"><path fill-rule="evenodd" d="M11.5 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L11 2.707V14.5a.5.5 0 0 0 .5.5zm-7-14a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L4 13.293V1.5a.5.5 0 0 1 .5-.5z"></path></svg><a data-toggle="modal" data-target="#modal_'+field_id+'" onclick="loadModal(\''+field_id+'\', '+key+', eval(\'collection_'+field_id+'\'))" style="margin: 7px;vertical-align: top;" class="glyphicon glyphicon-pencil"></a><svg style="cursor: pointer;"  onclick="jsonItemRemove(\''+field_id+'\', \''+key+'\', eval(\'collection_'+field_id+'\'))" width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="red" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path></svg></div></div>';
		container.append(rowHtml);
	});
	
	jsonLimitControl(field_id);
}

function loadModal(field_id, index_collection, collection) {
	modal_but_save = false;
	//if(collection.hasOwnProperty(index)) console.log(collection[index]);
	var data = {};
	if(index_collection != -1) data = collection[index_collection];
	
	$("#modal_"+field_id+" .modal-body #dynamicform-"+field_id+"_index").val(index_collection);
	//console.log($("#modal_"+field_id+" .modal-body #dynamicform-"+field_id+"_index").val());
	
	 $("#modal_"+field_id+" .modal-body input[type='text']").each(function( index, element ) { 
			//console.log($( this ).val());
			id = $(this).attr('id');
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id)) $(this).val(decodeURI(data[short_id]));
			else $(this).val('');
	  });
	  
	 $("#modal_"+field_id+" .modal-body input[type='password']").each(function( index, element ) { 
			//console.log($( this ).val());
			id = $(this).attr('id');
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id)) $(this).val(decodeURI(data[short_id]));
			else $(this).val('');
	  });

	  
	   $("#modal_"+field_id+" .modal-body textarea").each(function( index, element ) { 
			id = $(this).attr('id');
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id)){ 
				if(id.indexOf('dynamicform') >= 0) $(this).val(decodeURI(data[short_id]));
				else CKEDITOR.instances[id].setData(decodeURI(data[short_id]));
			} else {
				if(id.indexOf('dynamicform') >= 0)$(this).val('');
				else CKEDITOR.instances[id].setData('');
			}
	  });
	  
	   $("#modal_"+field_id+" .modal-body input[type='radio']").each(function( index, element ) { 
			id = $( this ).parent().parent().attr('id');
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id) && decodeURI(data[short_id]) == $( this ).val()) element.checked = true;
			if(index_collection == -1) element.checked = false;
	  });
	  
	  $("#modal_"+field_id+" .modal-body input[type='checkbox']").each(function( index, element ) { 
			id = $( this ).attr('id');
			if(id == undefined){ 
				id = $( this ).parent().parent().attr('id'); 
			}
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id)){
				var val = decodeURI(data[short_id])
				val =  val.replace('undefined|','');
				var arrayVals = val.split('|');
				if(arrayVals.includes($( this ).val())) element.checked = true;
				else element.checked = false;
			} else element.checked = false;
	  });

	  $("#modal_"+field_id+" .modal-body select").each(function( index, element ) { 
			id = $(this).attr('id');
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id)) $(this).val(decodeURI(data[short_id]));
			else $(this).val(null);
	  });
}

var modal_but_save = false;
function serializeJsonForm(modalId) {
	  
	var form = $("#"+modalId+" .modal-body form");
    form.yiiActiveForm('validate', true);
			
		// afterValidate
		form.on('afterValidate', function (e, m, eattr) {
			//e.preventDefault();
			if(eattr.length > 0) {
				modal_but_save = false;
			}
			else {
				if(modal_but_save){
					modal_but_save = false;
					_clickButSave(modalId);
				}
			}
        });
		
		 // остановить submit в beforeSubmit
        form.on('beforeSubmit', function(e) {
            e.preventDefault();
			return false;
        });
		   
	modal_but_save = true;	
	//if($("#"+modalId+" .modal-body .has-error").length == 0) {}
	  
	return false;
}

function _clickButSave(modalId){
	var data = {};
	var id;
	var field_id = modalId.replace('modal_','');
	var re = /'|&#39;|&quot;/gi;

		  $("#"+modalId+" .modal-body input[type='text']").each(function( index, element ) { 
				//console.log($( this ).val());
				id = $(this).attr('id');
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				var val = $( this ).val();
				if(val){ 
					val = val.replace(re, '').replace(/script/gi, 'sсriрt').replace(/on/gi, 'оn'); //Магия =)) подмена латинских букв на русские
					data[id] = encodeURI(val);
				}
				//console.log(element);
		  });
		  
		  $("#"+modalId+" .modal-body input[type='password']").each(function( index, element ) { 
				//console.log($( this ).val());
				id = $(this).attr('id');
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				var val = $( this ).val();
				if(val){ 
					val = val.replace(re, '').replace(/script/gi, 'sсriрt').replace(/on/gi, 'оn');
					data[id] = encodeURI(val);
				}
				//console.log(element);
		  });
		  
		  $("#"+modalId+" .modal-body textarea").each(function( index, element ) { 
				id = $(this).attr('id');
				var val;
				if(id.indexOf('dynamicform') >= 0) val = $( this ).val();
				else { 
					val = CKEDITOR.instances[id].getData(); 
					//console.log(val);
				}
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				if(val){ 
					val = val.replace(re, '').replace(/script/gi, 'sсriрt').replace(/on/gi, 'оn');
					data[id] = encodeURI(val);
				}
				//console.log(val);
				//console.log(element);
		  });
		  
		  $("#"+modalId+" .modal-body input[type='radio']").each(function( index, element ) { 
				//console.log($( this ).val());
				//console.log($( this ).parent().parent().attr('id'));
				//console.log(element.checked);
				id = $( this ).parent().parent().attr('id');
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				if(element.checked){ 
					var val = $( this ).val();
					if(val){ 
						val = val.replace(re, '').replace(/script/gi, 'sсriрt').replace(/on/gi, 'оn');
						data[id] = encodeURI(val);
					}
				}
		  });
		  
		  $("#"+modalId+" .modal-body input[type='checkbox']").each(function( index, element ) { 
				//console.log($( this ).val());
				//console.log($( this ).parent().parent().attr('id'));
				//console.log(element.checked);
				id = $( this ).attr('id');
				if(id == undefined){ 
					id = $( this ).parent().parent().attr('id'); 
				}
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				if(element.checked){ 
					var val = $( this ).val();
					val = val.replace(re, '').replace(/script/gi, 'sсriрt').replace(/on/gi, 'оn');
					val = encodeURI(val);
					data[id] = data[id] + '|' + val; 
				}
		  });

		  $("#"+modalId+" .modal-body select").each(function( index, element ) { 
				//console.log($( this ).val());
				id = $(this).attr('id');
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				var val = $( this ).val();
				if(val){ 
					val = val.replace(re, '').replace(/script/gi, 'sсriрt').replace(/on/gi, 'оn');
					data[id] = encodeURI(val);
				}

				//console.log(element);
		  });

		  var index = $("#"+modalId+" .modal-body #dynamicform-"+field_id+"_index").val();
		  
			var collection = {};
			try {
				collection = eval('collection_'+field_id);
			} catch (err) {
				collection = {};
			}
		  //console.log(collection);
		  
		  //console.log('length='+Object.keys(collection).length);
		  
		  if(index == -1) collection[Object.keys(collection).length] = data;
		  else collection[index] = data;
		  
		  //console.log(data);
		  //console.log(collection);
		  
		  saveJsonField(field_id, collection);
		  viewJsonFieldPanel(field_id, collection);
		  $("#"+modalId+" .close").trigger('click');
}

function saveJsonField(field_id, collection) {
	field_id = field_id.toLowerCase();
	var json = JSON.stringify(collection);
	$("#page-"+field_id).val(json);
	//console.log($("#page-"+field_id).val());
}

function jsonItemRemove(field_id, index, collection){
	if(confirm("Вы действительно хотите удалить данные?")) {
		$('.field-page-'+field_id+' .container .row[data-index='+index+']').remove();
		delete collection[index];
		saveJsonField(field_id, collection);
		jsonLimitControl(field_id);
	}
}

//Скрываем кнопку загрузки если достигнут лимит
function jsonLimitControl(id){
	var but = document.getElementById('but_'+id);
	if(but){
		var limit = 0;
		try {
			limit = eval('limit_'+ id);
		} catch (err) {
			limit = 0;
		}
		var count = $('div[data-field="'+id+'"] .row').length;
		
		if(limit > 0 && count >= limit){
			but.hidden = true;
		} else {
			but.hidden = false;
		}
	}
}