<?php
namespace backend\components;

use Yii;
use yii\base\Component;

class CustomComponent extends Component
{

    /**
     * Переводит строку в нижний регистр. Ставя перед заглавными буквами символ '-'
     * @param string $name
     * @return string
     */
	public function converterItemUrl($name)
    {
		$name = str_replace("action", '', $name);
		$name = str_replace("Controller", '', $name);
		$name = str_replace("Module", '', $name);
		
		$result = '';
		for($i=0; $i<strlen($name); $i++){
			if (ctype_upper($name{$i}) && $i == 0){$result .= strtolower($name{$i});}
			elseif (ctype_upper($name{$i}) && $i > 0){$result .= '-'.strtolower($name{$i});}
			else{$result .= $name{$i};}
		}
		return $result;
    }

}
