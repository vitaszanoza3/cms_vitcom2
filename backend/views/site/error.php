<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
//die("<pre>".print_r($exception, true)."</pre>");
$this->title = $name;
?>
<div class="site-error">

    <!--h1><?= Html::encode($this->title) ?></h1-->
    <h1><?= Yii::$app->t('error', 'Ошибка!') ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>


</div>
