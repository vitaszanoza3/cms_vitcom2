<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use common\widgets\LanguageWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="/backend/web/js/jquery.min.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->t('custom', 'Моя компания'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        //['label' => Yii::$app->t('menu', 'Главная'), 'url' => ['/site/index']],
        ['label' => Yii::$app->t('menu', 'Пользователи'), 'items' => [
			['label' => Yii::$app->t('menu', 'Пользователи'), 'url' => ['/users/user']],
			['label' => Yii::$app->t('menu', 'Редакторы'), 'url' => ['/users/role']],
		]],
		['label' => Yii::$app->t('menu', 'Страницы'), 'url' => ['/pages/default']],
 		['label' => Yii::$app->t('menu', 'Переводы'), 'url' => ['/system/translations']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => Yii::$app->t('menu', 'Вход'), 'url' => ['//site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                Yii::$app->t('menu', 'Выход ({name})', ['name' => Yii::$app->user->identity->username]),
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>';
		
		$menuItems[] = '<li><a href="/backend/web/site/delete-cache" onclick="return confirm(\''.Yii::$app->t('menu', 'Вы действительно хотите удалить кэш с картинками?   ').'\');">'.Yii::$app->t('menu', 'Кэш').'</a></li>';
    }
	
	$languageHtml = '';
	$n=0;
	foreach(Yii::$app->params['language'] as $key => $value){
		$n++;
		if($n != 1) $languageHtml .= '<span> | </span>';
		$languageHtml .= ((Yii::$app->language == $key)? mb_strtoupper($key):Html::a(mb_strtoupper($key), Url::to(['/site/language', 'language' => $key])));	
	}
	
	$menuItems[] = '<li><div>'.$languageHtml.'</div></li>';
			
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
	
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php if($this->blocks): ?>
<?php foreach($this->blocks as $key => $value): ?>
	<?= $value ?>
<?php endforeach; ?>
<?php endif ?>
		
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
