<?php

namespace backend\modules\users\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use common\models\RoleEditor;
use common\components\AccessFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class RoleController extends Controller
{
    public function behaviors()
    {
        return [
		    [
                'class' => AccessFilter::className(),
				'accessRoles' => [					
					User::ROLE_ADMIN, 
					User::ROLE_MODERATOR, 
				]
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoleEditor();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RoleEditor();
		
        if ($model->load(Yii::$app->request->post()) && $model->validate() && self::addParams($model)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
	
    /**
     * Добавляет параметры к модели и задает роли
     * @param RoleEditor $model
     * @return boolean 
     */
	private function addParams(&$model)
	{
		$model->save();
		
		//Добавление роли
		$auth = Yii::$app->authManager;
		if(!$auth->getRole($model->role))
		{
			$editor = $auth->getRole('editor');
			$new_editor = $auth->createRole($model->role);
			$auth->add($new_editor);
			$auth->addChild($new_editor, $editor);
 		}
		
		return true;
	}


    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$model = $this->findModel($id);
		$auth = Yii::$app->authManager;
		$auth->remove($auth->getRole($model->role));
        $model->delete();

        return $this->redirect(['index']);
    }
	
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RoleEditor::findOne($id)) !== null) {
            return $model;
        } else {
 			throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не найдена страница.'));
        }
    }
}
