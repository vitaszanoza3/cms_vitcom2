<?php

namespace backend\modules\users\controllers;

use Yii;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
		echo 'common='.Yii::getAlias('@common');
		echo '<br/>backend='.Yii::getAlias('@backend');
		
        //return $this->render('index');
    }
}
