<?php

namespace backend\modules\users\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
		    [
                'class' => AccessFilter::className(),
				'accessRoles' => [
					User::ROLE_ADMIN, 
					User::ROLE_MODERATOR, 
					User::ROLE_EDITOR => ['index', 'update']
				]
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

 	public function beforeAction($action)
    {
		if($action->id == 'index'){$start = true;}
		elseif(Yii::$app->user->can(User::ROLE_ADMIN)){$start = true;}
		elseif(
			$action->id == 'update' 
			&& ($id = Yii::$app->request->get('id')) 
			&& ($model = $this->findModel($id))
			&& Yii::$app->user->identity
			&& ($model->id == Yii::$app->user->identity->id)
		){
			$start = true;
		}
		else{
			throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'У вас нет доступа к этой странице.'));
		}
		
		return parent::beforeAction($action);
    }


    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
		
		//Если не админ выводим только запись пользователя
		if(!Yii::$app->user->can(User::ROLE_ADMIN)){
			$queryParams = Yii::$app->request->queryParams;
			$queryParams['UserSearch']['id'] = Yii::$app->user->identity->id;
			Yii::$app->request->queryParams = $queryParams;
		}

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User(['scenario' => 'system_registration']);

		$model->role = User::ROLE_USER;
		
        if ($model->load(Yii::$app->request->post()) && $model->validate() && self::addParams($model)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
	
    /**
     * Добавляет параметры к модели и задает роли
     * @param User $model
     * @return boolean 
     */
	private function addParams(&$model)
	{
		if($model->password){$model->setPassword($model->password);}
		//$model->generateAuthKey();
		
		//Добавление роли
		$auth = Yii::$app->authManager;
		if($role = $auth->getRole($model->role)){
			$model->save();
			/*
			//Добавление роли просходит в afterSave
			if(!$auth->getAssignment($model->role, $model->getId())){
				$auth->revokeAll($model->getId());
				$auth->assign($role, $model->getId());
			}
			*/
			return true;
		}
		elseif(isSet(User::getAllRoles()[$model->role])){
			$editor = $auth->getRole($model->role);
			$auth->assign($editor, $model->getId());
		}
		else
		{
			$model->addError('role', 'Не найдена роль.');
			return false;
		}
 		
	}

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		//Ограничение для редакторов
		if(!Yii::$app->user->can(User::ROLE_MODERATOR) && $id != Yii::$app->user->identity->id){
			throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'У вас нет доступа к этой странице..'));
		}
		//die('jkjjk');
		$model->scenario = 'system_registration';

        if ($model->load(Yii::$app->request->post()) && $model->validate() && self::addParams($model)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
 		$model = $this->findModel($id);
		$auth = Yii::$app->authManager;
		$auth->revokeAll($model->id);
        $model->delete();
		
        return $this->redirect(['index']);
    }
	
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
 			throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не найдена страница.'));
        }
    }
}
