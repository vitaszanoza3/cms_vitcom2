<?php

namespace backend\modules\users;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\users\controllers';

    public function init()
    {
        parent::init();

		//Проверяем есть ли роль 'admin', если нет создаем
		$auth = Yii::$app->authManager;
		if(!$auth->getRole('admin')){
			$admin = $auth->createRole('admin');
			$auth->add($admin);
			
			if(!$auth->getRole('moderator')){
				$moderator = $auth->createRole('moderator');
				$auth->add($moderator);
				$auth->addChild($admin, $moderator);
			}

			if(!$auth->getRole('editor')){
				$editor = $auth->createRole('editor');
				$auth->add($editor);
				$auth->addChild($moderator, $editor);
			}
			
			if(!$auth->getRole('user')){
				$user = $auth->createRole('user');
				$auth->add($user);
				$auth->addChild($editor, $user);
			}

			if(!$auth->getRole('baned')){
				$baned = $auth->createRole('baned');
				$auth->add($baned);
			}
		}
        // custom initialization code goes here
	}
}
