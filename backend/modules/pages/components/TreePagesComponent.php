<?php
namespace backend\modules\pages\components;

use Yii;
use yii\base\Component;
use yii\db\Query;

use common\models\Page;
use common\models\Fields;

class TreePagesComponent extends Component
{	
    public function init()
    {
        parent::init();
    }
	
	/**
     * Добавляет новый узел в дерево страниц. 
	 * Модель передается по ссылке
     * @param Page $model
     * @return null
     */
	public function addNode(Page &$model, $parentPage)
	{
		$table = Page::tableName();
		$maxPosition =	Yii::$app->db->createCommand("SELECT max(position) AS position FROM $table WHERE parent_id = :parent_id")
			->bindValue(':parent_id', (int)$model->parent_id)
			->queryScalar();
		$model->position = (int)$maxPosition + 1;
		
		if($model->type != Page::TYPE_ITEM) $model->path = $parentPage->path.$parentPage->id.'|';
		else $model->path = '|';
		
		return $model->save();
	}
	
	/** 
     * Добавляет новый узел в дерево страниц. 
	 * Модель передается по ссылке
     * @param Page $model
     * @return null
     */
	public function updateNode(Page &$model)
	{	
		//Смена пути для этой страницы и ее потомков
		if($model->parent_id != $model->old_parent_id){
			//Правим путь на случай переноса
			if($model->type != Page::TYPE_ITEM){ 
			
				//Проверка дочернего узла
				if((int)$model->parent_id != 0){
					$model_new_parent = Page::easyFindModel($model->parent_id, 'id');
				}
				else{
					$model_new_parent = Page::easyFindModel('root', 'alias');
				}
				if (!$model_new_parent){throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не верный корневой узел'));}
				else if ($model_new_parent->type == Page::TYPE_ITEM){throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Нельзя переносить узел элементов.'));}
			
				//Проверка. Нельзя переносить узел своим потомкам
				//Собираем всех потомков
				$models = Yii::$app->db->createCommand('SELECT id FROM '.Page::tableName().' WHERE path LIKE "%|'.(int)$model->id.'|%"')->queryAll();
				$IDs = [$model->id];	
				foreach($models as $childsModel){$IDs[] = $childsModel['id'];}
				if (in_array($model->parent_id, $IDs)){throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Нельзя переносить узел своим потомкам.'));}
				
				$model->path = $model->parent->path.$model->parent->id.'|';
		
				Yii::$app->db->createCommand("
						UPDATE ".Page::tableName()."
						SET path = CONCAT('".$model->path."', SUBSTRING(path, LOCATE('".$model->id."|', path)) )
						WHERE path LIKE '%|".$model->id."|%'
					")
					->execute();
			} else $model->path = '|';
		} 
		
		$validate = $model->save();
			
		//Пересчет позицый для страниц
		//if($model->position != $model->old_position){
			Yii::$app->db->createCommand("SET @position=:position")
				->bindValue(':position', (int)$model->position)
				->execute();
				
			//Пересчет позиций
			Yii::$app->db->createCommand("
				UPDATE ".Page::tableName()." 
				SET position = @position:=@position+1 
				WHERE type != :type AND parent_id = :parent_id AND position > :position AND id != :id
				ORDER BY position
				")
				->bindValue(':id', $model->id)
				->bindValue(':parent_id', $model->parent_id)
				->bindValue(':type', Page::TYPE_ITEM)
				->bindValue(':position', $model->position-1)
				->execute();
		//}
		
		return $validate;
	}
	
	/**
     * Указываем настройки страницы
	public function settingsPage($alias, $id = 0){
		$settings = Yii::$app->db->createCommand("
				call find_settings(:alias,:id);
			")
			->bindValue(':alias', $alias)
			->bindValue(':id', $id)
			->queryOne();
			
		//Указываем родительский шаблон. Он нужен в Page afterFind
		$this->parentTemplate = $settings['template'];
		$this->parentController = $settings['controller'];
		$this->parentEditors = $settings['editors'];
		$this->parentFieldsGroup = $settings['fieldsGroup'];
			
		return $settings;
	}
     */



}
