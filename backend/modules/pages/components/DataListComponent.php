<?php
namespace backend\modules\pages\components;

use Yii;
use yii\base\Component;
use yii\db\Query;
use yii\db\Expression;

use common\models\Page;
use common\models\Fields;

class DataListComponent extends Component
{	
	const FUNC_defaultData = 'defaultData';
    const FUNC_aliasData = 'aliasData';

	private $cache = null;
	
    public function init()
    {
        parent::init();
    }
	
	/**
     * Функция для жестко заданных значений
     */
	public function defaultData($params){
		return $params['values'];
	}
	
	/**
     * Получают данные из базы. По alias
     */
	public function aliasData($params){
		$alias = $params['alias'];
		$type = $params['type'];
		
		if(isSet($params['key'])) ($params['key'] == 'id')?$key = 'id':$key = 'alias';
		else $key = 'alias';
		
		$model = (new \yii\db\Query())->select('t1.id')->from(Page::tableName().' as t1')->where('t1.alias = :alias', [':alias' => $alias])->limit(1)->one();
		if(!$model) return [];
		
		$keyCache = md5('aliasData'.print_r($params, true));
		if(isSet($this->cache[$keyCache])) $models = $this->cache[$keyCache];
		else {
			$models = (new \yii\db\Query())
				->select(['id', 
					'title', 
					"JSON_EXTRACT(`jsonTitle` , '$.".Yii::$app->language."') as jsonTitle", 
					'alias', 
					'position'])
				->from(Page::tableName())
				->where('parent_id=:parent_id AND type=:type', [':parent_id' => $model['id'], ':type' => $type])
				->andWhere(['OR',
					['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), Page::VISIBLE_PUBLISHED],
					['AND',
						['IS', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), null],
						['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.ru')"), Page::VISIBLE_PUBLISHED]
					],
				])
				->orderBy('position ASC')
				->all();
				$this->cache[$keyCache] = $models;
		}
	
		//die("<pre>".print_r($models, true)."</pre>");
		//die("<pre>".print_r($models->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql, true)."</pre>");

		if($models){
			$list = [];
			foreach($models as $childrenPage){
				if(isSet($childrenPage['jsonTitle'])) $childrenPage['title'] = json_decode($childrenPage['jsonTitle']);
				$childrenPage = (object)$childrenPage;
				if($key == 'id') $list[$childrenPage->id] = $childrenPage->title;
				else $list[$childrenPage->alias] = $childrenPage->title;
			}
			return $list;
		}
		return [];
	}
}
