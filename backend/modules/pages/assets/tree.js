$(function() {
	var tree = $('#tree');
	var selected_node = null;
	var options = {
        //data: data,
		autoOpen: 0,
		saveState: true,
		dragAndDrop: true,
		selectable: false,
		//buttonLeft: false,
		onCreateLi: function(node, $li) {
			if(node.id){
				var element = $li.find('.jqtree-element');
				element.attr('class', 'item');
				
				var elementA = element.find('a');
				var htmlA = '';
				if(elementA.length == 0){
					htmlA = '<svg width="21px" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right-short" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"></path></svg>';
				}
				
				var iconClass = '';
				if(node.type == 'group') iconClass = 'glyphicon glyphicon-folder-open';
				else if(node.type == 'page') iconClass = 'glyphicon glyphicon-list-alt';
				else iconClass = 'glyphicon glyphicon-file';
				
				var elementSpan = element.find('span');
				elementSpan.before(htmlA + '<div class="'+ iconClass +'"></div> &nbsp');
				
				//console.log('node', node);
				if(node.visible == 0) elementSpan.attr('style', 'color: #000000;');//published
				else elementSpan.attr('style', 'color: #7f7f7f;');
				
				if(node.childrenCountItem > 0){
					element.append(
						' &nbsp <a href="/backend/web/pages/default/index-details?id='+ node.id +'&type=item" data-node-id="'+
						node.id +'"><span class="badge">'+ node.childrenCountItem +'</span></a>'
					);
				}
				
				if(node.countMultyItem > 0){
					element.append(
						' &nbsp <a href="/backend/web/pages/default/index-details?id='+ node.id +'&type=item&multyItem=1" data-node-id="'+
						node.id +'"><span class="badge color-blue">'+ node.countMultyItem +'</span></a>'
					);
				}
				
				element.context.innerHTML = '<div class="jqtree-element jqtree_common" role="presentation">' 
					+ element.context.innerHTML 
					+ '<a href="/backend/web/pages/default/update?id='+ node.id +'" class="glyphicon glyphicon-pencil" data-node-id="'+ node.id +'"></a>'
					+ '</div>';
				//console.log('element', element.context.innerHTML);
				

			}
        },
		autoEscape: false,
		//closedIcon: '+',
		//openedIcon: '-',

    };
	
	//Блокируем перенос узлов для едиторов
	if(blockDragAndDrop){options.dragAndDrop = false;}
	
    tree.tree(options);

	tree.bind(
    'tree.init',
    function() {
    }
);
	tree.bind(
        'tree.click',
        function(e) {
            // Disable single selection
            //e.preventDefault();

			if(selected_node){tree.tree('removeFromSelection', selected_node);}
			
            selected_node = e.node;
			//alert(selected_node.id);

            if (selected_node.id == undefined) {
                console.log('The multiple selection functions require that nodes have an id');
            }

			//tree.tree('addToSelection', selected_node);
			updateInformer(selected_node.id);
			
        }
    );
	
	tree.bind(
		'tree.move',
		function(event) {
			//console.log('move_info', event.move_info);
			//console.log('moved_node', event.move_info.moved_node);
			//console.log('target_node', event.move_info.target_node);
			//console.log('position', event.move_info.position);
			//console.log('previous_parent', event.move_info.previous_parent);
			
			var url = '/backend/web/pages/default/update-position';
			
			var get = {};
			get['id'] = event.move_info.moved_node.id;
			if(event.move_info.position == 'inside'){
				get['new_parent_id'] = event.move_info.target_node.id;
				//get['prev_node_id'] = event.move_info.target_node.id;
			}
			else if(event.move_info.position == 'after'){
				get['new_parent_id'] = event.move_info.target_node.parent_id;
				get['prev_node_id'] = event.move_info.target_node.id;
			}
		
			$.get(url, get, function(response){
				//console.log('|'+response+'|');
			});
		}
	);
	
	function updateInformer(id){
		var url = '/backend/web/pages/default/informer';
		var get = {'id':id};
		$.get(url, get, function(response){
			var informer = $('.pageInformer');
			informer.html(response);
			//console.log('|'+response+'|');
		});
		//console.log(url);
	}
	
});