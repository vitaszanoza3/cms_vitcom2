$(function() {

});

function initFormField(field_id, json, type) {
	var re = /\n/gi;
	json = json.replace(re,'\\n');
	var data = {};
	if(json){ 
		try {
			data = JSON.parse(json);
			viewMailFormFieldPanel(field_id, data);
		} catch (e) {
			console.log('Ошибка парсинга JSON');
		}
	}
	return data;
}

function viewMailFormFieldPanel(field_id, data) {
	field_id = field_id.toLowerCase();
	var container = $('.field-page-'+field_id+' > .container');
	container.html('');
	
	//console.log(data.on);
	var on = '';
	if(data.on && data.on.indexOf('on') >= 0){
		on = '<b>Настройки включены</b><br/>';
	} else {on = '<b>Настройки отключены</b><br/>';}

	if(data){
		if($('#form_'+field_id).attr('data-type') == 'mail'){
			var rowHtml = '<div class="row"><div class="col-xs-7">'+on+'<b>title</b>:'+data.title+'<br/><b>from email</b>:'+data.from_email+'<br/><b>to email</b>:'+data.to_email+'<br/><b>success</b>:'+data.success+'</div></div>';
		} else {
			var rowHtml = '<div class="row"><div class="col-xs-7">'+on+'<b>success</b>:'+data.success+'<br/></div></div>';
		}
		container.append(rowHtml);
	}
}

function loadMailFormModal(field_id, data) {
	modal_but_save = false;
	
	 $("#modal_"+field_id+" .modal-body input[type='text']").each(function( index, element ) { 
			//console.log($( this ).val());
			id = $(this).attr('id');
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id)) $(this).val(data[short_id]);
			else $(this).val('');
	  });
	  
	 $("#modal_"+field_id+" .modal-body input[type='password']").each(function( index, element ) { 
			//console.log($( this ).val());
			id = $(this).attr('id');
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id)) $(this).val(data[short_id]);
			else $(this).val('');
	  });

	  
	   $("#modal_"+field_id+" .modal-body textarea").each(function( index, element ) { 
			id = $(this).attr('id');
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id)){ 
				if(id.indexOf('dynamicform') >= 0) $(this).val(data[short_id]);
				else CKEDITOR.instances[id].setData(data[short_id]);
			} else {
				if(id.indexOf('dynamicform') >= 0)$(this).val('');
				else CKEDITOR.instances[id].setData('');
			}
	  });
	  
	   $("#modal_"+field_id+" .modal-body input[type='radio']").each(function( index, element ) { 
			id = $( this ).parent().parent().attr('id');
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id) && data[short_id] == $( this ).val()) element.checked = true;
			if(index_collection == -1) element.checked = false;
	  });
	  
	  $("#modal_"+field_id+" .modal-body input[type='checkbox']").each(function( index, element ) { 
			id = $( this ).parent().parent().attr('id');
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id)){
				var val =  data[short_id].replace('undefined|','');
				var arrayVals = val.split('|');
				if(arrayVals.includes($( this ).val())) element.checked = true;
				else element.checked = false;
			} else element.checked = false;
	  });

	  $("#modal_"+field_id+" .modal-body select").each(function( index, element ) { 
			id = $(this).attr('id');
			var short_id = id.replace('dynamicform-','');
			short_id = short_id.replace(field_id+'_','');
			if(data.hasOwnProperty(short_id)) $(this).val(data[short_id]);
			else $(this).val(null);
	  });
}

var modal_but_save = false;
function serializeMailForm(modalId) {
	  
	var form = $("#"+modalId+" .modal-body form");
    form.yiiActiveForm('validate', true);
			
		// afterValidate
		form.on('afterValidate', function (e, m, eattr) {
			//e.preventDefault();
			if(eattr.length > 0) {
				modal_but_save = false;
			}
			else {
				if(modal_but_save){
					modal_but_save = false;
					_clickButSaveMailForm(modalId);
				}
			}
        });
		
		 // остановить submit в beforeSubmit
        form.on('beforeSubmit', function(e) {
            e.preventDefault();
			return false;
        });
		   
	modal_but_save = true;	
	//if($("#"+modalId+" .modal-body .has-error").length == 0) {}
	  
	return false;
}

function _clickButSaveMailForm(modalId){
	var data = {};
	var id;
	var field_id = modalId.replace('modal_','');

		  $("#"+modalId+" .modal-body input[type='text']").each(function( index, element ) { 
				//console.log($( this ).val());
				id = $(this).attr('id');
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				data[id] = $( this ).val();
				//console.log(element);
		  });
		  
		  $("#"+modalId+" .modal-body input[type='password']").each(function( index, element ) { 
				//console.log($( this ).val());
				id = $(this).attr('id');
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				var val = $( this ).val();
				if(val) data[id] = val;
				//console.log(element);
		  });
		  
		  $("#"+modalId+" .modal-body textarea").each(function( index, element ) { 
				id = $(this).attr('id');
				var val;
				if(id.indexOf('dynamicform') >= 0) val = $( this ).val();
				else val = CKEDITOR.instances[id].getData();
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				data[id] = val;
				//console.log(val);
				//console.log(element);
		  });
		  
		  $("#"+modalId+" .modal-body input[type='radio']").each(function( index, element ) { 
				//console.log($( this ).val());
				//console.log($( this ).parent().parent().attr('id'));
				//console.log(element.checked);
				id = $( this ).parent().parent().attr('id');
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				if(element.checked) data[id] = $( this ).val();
		  });
		  
		  $("#"+modalId+" .modal-body input[type='checkbox']").each(function( index, element ) { 
				//console.log($( this ).val());
				//console.log($( this ).parent().parent().attr('id'));
				//console.log(element.checked);
				id = $( this ).parent().parent().attr('id');
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				if(element.checked) data[id] = data[id] + '|' + $( this ).val();
		  });

		  $("#"+modalId+" .modal-body select").each(function( index, element ) { 
				//console.log($( this ).val());
				id = $(this).attr('id');
				id = id.replace('dynamicform-','');
				id = id.replace(field_id+'_','');
				data[id] = $( this ).val();
				//console.log(element);
		  });

		  console.log(data);
		  		  
		  saveMailFormField(field_id, data);
		  $("#"+modalId+" .close").trigger('click');
}

function saveMailFormField(field_id, data) {
	field_id = field_id.toLowerCase();
	var json = JSON.stringify(data);
	$("#page-"+field_id).val(json);
	viewMailFormFieldPanel(field_id, data);
	console.log($("#page-"+field_id).val());
}
