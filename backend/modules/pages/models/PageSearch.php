<?php

namespace backend\modules\pages\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Page;
use yii\db\Expression;

/**
 * PageSearch represents the model behind the search form about `common\models\Page`.
 */
class PageSearch extends \yii\db\ActiveRecord
{
	public $template = null;
	public $visible = null;
	public $showItem = false;
	public $multyItem = false;

	 /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }
	
	public function attributes()
	{
		// делаем поле зависимости доступным для поиска
		//return array_merge(parent::attributes(), ['title2']);
		return parent::attributes();
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id'], 'integer'],
            [['alias', 'template', 'type', 'visible', 'title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {	
		if($this->showItem){
			$subQueryChildrenCount = 'CONCAT(0)';
			$subQueryChildrenCountItem = 'CONCAT(0)';
			$subQueryCountMultyItem = 'CONCAT(0)';
		} else {
			//$subQueryChildrenCount = (new \yii\db\Query())->select('COUNT(*)')->from(Page::tableName().' as t1')->where('t1.parent_id = t2.id AND t1.type != :type', [':type' =>Page::TYPE_ITEM]);
			$subQueryChildrenCount = (new \yii\db\Query())->select('(1)')->from(Page::tableName().' as t1')->where('t1.parent_id = t2.id AND t1.type != :type', [':type' =>Page::TYPE_ITEM])->limit(1);
			$subQueryChildrenCountItem = (new \yii\db\Query())->select('COUNT(*)')->from(Page::tableName().' as t1')->where('t1.parent_id = t2.id AND t1.type = :type', [':type' =>Page::TYPE_ITEM]);
			$subQueryCountMultyItem = (new \yii\db\Query())->select("(CASE WHEN t2.on_itemParentsID = 1 THEN (SELECT COUNT(*) FROM `page` `t1` WHERE t1.itemParentsID LIKE CONCAT('%|', `t2`.`id`, '|%') AND `t1`.`type` = 'item') ELSE 0 END)");
		}

		$query = (new \yii\db\Query())
			->select(['t2.id', 
				't2.parent_id',
				't2.alias', 
				't2.title', 
				"JSON_EXTRACT(`jsonTitle` , '$.".Yii::$app->language."') as jsonTitle", 
				't2.position', 
				//'t2.visible',
				"JSON_EXTRACT(`jsonVisible` , '$.ru') as visible_ru", 
				"JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."') as visible", 					
				't2.type', 
				//'t2.template', 
				"JSON_EXTRACT(`jsonTemplate` , '$.ru') as template_ru", 
				"JSON_EXTRACT(`jsonTemplate` , '$.".Yii::$app->language."') as template", 					
				//'t2.controller', 
				"JSON_EXTRACT(`jsonController` , '$.ru') as controller_ru", 
				"JSON_EXTRACT(`jsonController` , '$.".Yii::$app->language."') as controller", 					
				't2.editors', 
				't2.path', 
				'childrenCount' => $subQueryChildrenCount, 
				'childrenCountItem' => $subQueryChildrenCountItem,
				'countMultyItem' => $subQueryCountMultyItem, 
			])->from(Page::tableName().' as t2')
			//->where('parent_id=:parent_id', [':parent_id' => $this->parent_id])
			->orderBy('position');
				
		if($this->multyItem && $this->showItem){
			$query->where("itemParentsID LIKE CONCAT('%|', :parent_id, '|%')", [':parent_id' => $this->parent_id]);
		} else {
			$query->where('parent_id=:parent_id', [':parent_id' => $this->parent_id]);
		}
		
		if($this->showItem)	$query->andFilterWhere(['=', 'type', Page::TYPE_ITEM]);
		else $query->andFilterWhere(['!=', 'type', Page::TYPE_ITEM]);

		//die("<pre>".print_r($query->all(), true)."</pre>");
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => Yii::$app->params['paginationPageSize'],
			],
        ]);

        $this->load($params);
		//die("<pre>".print_r($this, true)."</pre>");

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		/*
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            //'created_at' => $this->created_at,
            //'updated_at' => $this->updated_at,
        ]);
		*/
		
        $query
			->andFilterWhere(['like', new Expression("JSON_EXTRACT(`jsonTitle` , '$.".Yii::$app->language."')"), ($this->title)?str_replace('"', '', json_encode($this->title)):null])
			->andFilterWhere(['like', new Expression("JSON_EXTRACT(`jsonTemplate` , '$.".Yii::$app->language."')"), ($this->template)?str_replace('"', '', json_encode($this->template)):null])
			->andFilterWhere(['like', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), ($this->visible)?str_replace('"', '', json_encode($this->visible)):null])
            //->andFilterWhere(['like', 'template', $this->template])
            ->andFilterWhere(['like', 'type', $this->type]);

		$models = $dataProvider->getModels(); //Глюк! Без этой записи пагинация не работает
		//die("<pre>".print_r($models, true)."</pre>");
 
        return $dataProvider;
    }
}
