<?php

namespace backend\modules\pages\models;

use Yii;
use yii\db\ActiveQuery;
use common\models\Page;
use yii\db\Expression;

class PageQuery extends ActiveQuery
{
    public function visiblePublished()
    {
       return $this->andWhere(['OR',
			['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), Page::VISIBLE_PUBLISHED],
			['AND',
				['IS', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), null],
				['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.ru')"), Page::VISIBLE_PUBLISHED],
			],
		]);
    }
	
	public function visibleData()
    {
       return $this->andWhere(['OR',
			['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), Page::VISIBLE_DATA],
			['AND',
				['IS', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), null],
				['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.ru')"), Page::VISIBLE_DATA],
			],
		]);
    }
	
	public function visiblePublishedAndData()
    {
       return $this->andWhere(['OR',
			['IN', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), [Page::VISIBLE_PUBLISHED, Page::VISIBLE_DATA]],
			['AND',
				['IS', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), null],
				['IN', new Expression("JSON_EXTRACT(`jsonVisible` , '$.ru')"), [Page::VISIBLE_PUBLISHED, Page::VISIBLE_DATA]],
			],
		]);
    }
	
	public function typePage()
    {
       return $this->andWhere('type = :type', [':type' => Page::TYPE_PAGE]);
    }
	
	public function typeItem()
    {
       return $this->andWhere('type = :type', [':type' => Page::TYPE_ITEM]);
    }
	
	public function multyLink($parent_id, $boolOr = false)
    {
		if($boolOr) return $this->orWhere(['like', 'itemParentsID', ':parent_id', [':parent_id' => '%' . $parent_id . '%']]);
		else return $this->andWhere(['like', 'itemParentsID', ':parent_id', [':parent_id' => '%' . $parent_id . '%']]);
    }
	
	public function fields($parentTemplate, $language = null)
    {
		if(!$language) $language = Yii::$app->params['startLanguage'];
		if($language == Yii::$app->params['startLanguage']){
			return $this->leftJoin('fields_'.$parentTemplate.'_'.$language.' as '.$language, $language.'.page_id = page.id');
		} else {
			$startLanguage = Yii::$app->params['startLanguage'];
			return $this
				->leftJoin('fields_'.$parentTemplate.'_'.$language.' as '.$language, $language.'.page_id = page.id')
				->leftJoin('fields_'.$parentTemplate.'_'.$startLanguage.' as '.$startLanguage, $startLanguage.'.page_id = page.id');
		}
    }
}