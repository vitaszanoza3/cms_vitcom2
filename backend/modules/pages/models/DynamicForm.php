<?php
namespace backend\modules\pages\models;

use Yii;
use yii\base\Model;
use common\models\Fields;

/**
 * Dynamic Form
 * Заточена под JSON поля
 */
class DynamicForm extends Model
{
    public $attributes = array();
	private $fields = array();
	private $prefix = '';
	
	function __construct($prefix) {
       $this->prefix = $prefix;
	}
   
    /**
     * @inheritdoc
     */
    public function rules()
    {		
		$rules = [];
	    //$rules[] = [[$this->prefix.'_title'], 'required'];

		$new_rules = [];
		foreach($this->attributes as $fieldName => $value){
			$shortFieldName = str_replace($this->prefix.'_', '', $fieldName);
			$type = $this->fields[$shortFieldName][1];
			
			switch($type){
				case 'TYPE_INT': $new_rules['integer'][] = $fieldName; break;
				case 'TYPE_FILES': $new_rules['gallery'][] = $fieldName; break; 
				case 'TYPE_IMAGES': $new_rules['gallery'][] = $fieldName; break; 
				case 'TYPE_DATE': $new_rules['validationDate_display'][] = $fieldName;  break;
				case 'TYPE_DROPDOWN_LIST': $new_rules['in'][] = $fieldName; break;
				case 'TYPE_RADIO': $new_rules['in'][] = $fieldName; break;
				case 'TYPE_CHECKBOX_LIST': $new_rules['arrayVal'][] = $fieldName; break;
				case 'TYPE_EMAIL': $new_rules['email'][] = $fieldName; break;
				case 'TYPE_URL': $new_rules['safe'][] = $fieldName; break;
				case 'TYPE_CAPTCHA': $new_rules['captcha'][] = $fieldName; break;
				default: $new_rules['string'][] = $fieldName; break;
			}
							
			if(isSet($this->fields[$shortFieldName]['required']) && $this->fields[$shortFieldName]['required']){
				$new_rules['required'][] = $fieldName;
			}
		}
		
		foreach($new_rules as $preset => $fields){
			if($preset == 'in'){
				foreach($fields as $fieldName){
					$shortFieldName = str_replace($this->prefix.'_', '', $fieldName);
					$rules[] = [$fieldName, $preset, 'range'=> array_keys(Yii::$app->dataListComponent->{$this->fields[$shortFieldName]['function']}($this->fields[$shortFieldName]['params']))];
				}
			}elseif($preset == 'url'){
				$rules[] = [$fields, $preset, 'defaultScheme' => 'http'];
			}elseif($preset == 'required'){
					$rules[] = [$fields, 'required'];
			} else{
				$rules[] = [$fields, $preset];
			}
		}
		
		//die("fields<pre>".print_r($this->fields, true)."</pre>");
		//die("rules<pre>".print_r($rules, true)."</pre>");
        return $rules;
    }
	
	/**
     * Валидатор для сохранения дат
     */
	public function validationDate_display($attribute, $params) 
	{
		//Если не число
		if(!preg_match("|^[\d]+$|", $this->$attribute)){
	
			$date =  explode("/", $this->$attribute);
			//Если содержит 3 элемента то текст. Иначе число
			if(count($date)==3){
				$this->$attribute = strtotime($date[2].'-'.$date[1].'-'.$date[0]);
			}
			else{
				$this->$attribute = strtotime("now");
			}
		}
		else{
			$this->$attribute = strtotime("now");
		}
	}
	
	public function arrayVal($attribute, $params) {
		if (!is_array($this->$attribute)) {
			$this->addError($attribute, 'Значение должно быть массивом!');
		}
	}

	public function setFields($fields){
		$this->fields = $this->fields + $fields;
		foreach($this->fields as $key => $value) $this->attributes[$this->prefix.'_'.$key] = '';
	}

	public function __get($name) {
		$local_attributes = [
			'isNewRecord',
		];
		if(!in_array($name,$local_attributes) && isSet($this->attributes[$name])){
			return (isSet($this->attributes[$name]))?$this->attributes[$name]:'';
		}
		else{
			return parent::__get($name);
		}
    }

    public function __set($name, $value) {
		$local_attributes = [
			'isNewRecord',
		];
		if(!in_array($name,$local_attributes) && isSet($this->attributes[$name])){
			$this->attributes[$name] = $value;
		}
		else{
			parent::__set($name, $value);
		}
        
    }

 	public function attributeLabels(){
		$labels = array();
		foreach($this->fields as $key => $value){
			$labels[$this->prefix.'_'.$key] = $value[0];
		}
		return $labels;
	}
}
