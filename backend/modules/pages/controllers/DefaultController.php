<?php

namespace backend\modules\pages\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use common\components\AccessFilter;
use backend\modules\pages\components\UrlTransliterate;
use yii\filters\RateLimiter;

use common\models\User;
use common\models\IpLimiter;
use common\models\Page;
use backend\modules\pages\models\PageSearch;
use yii\db\Expression;
use yii\web\NotFoundHttpException;

/**
 * DefaultController implements the CRUD actions for Page model.
 */
class DefaultController extends Controller
{

public $tableFields='fieldsDefault';

    public function behaviors()
    {
        return [
 		    [
                'class' => AccessFilter::className(),
				'accessRoles' => [
					User::ROLE_ADMIN, 
					User::ROLE_MODERATOR, 
					User::ROLE_EDITOR => [
						'index', 
						'view',
						'update', //Ограничен
						'informer',
					]
				]
			],
           'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
           'rateLimiter' => [
				'class' => RateLimiter::className(),
				'user' => function() { return new IpLimiter(); },
				'except' => ['error']
           ],

        ];
    }

	/**
     * Выводит лимитированую информацию о узле
     * @param int $node Идентификатор узла
     * @param int $limit
	 * @return null
     */
    public function actionIndex($node = null, $limit = null)
    {
		if(!$limit) $limit = Yii::$app->params['paginationPageSize'];
		
		$test = false;
		if($test ||
		Yii::$app->request->isAjax){
			if(!$test) Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			
			if($node){
				$parentModel = Page::easyFindModel($node);
				
				if($parentModel)
				{
					$models = Page::getChildrenNode($parentModel->id, $limit);
				}
				else{
					throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не верный корневой узел.'));
				}
			}
			else{

				$parentModel = (new \yii\db\Query())
					->select(['t2.id', 
						't2.title', 
						"JSON_EXTRACT(`jsonTitle` , '$.".Yii::$app->language."') as jsonTitle", 
						't2.parent_id', 
						't2.position', 
						//'t2.visible', 
						"JSON_EXTRACT(`jsonVisible` , '$.ru') as visible_ru", 
						"JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."') as visible", 
						't2.type', 
						'childrenCount' => 'CONCAT(1)', 
						'childrenCountItem' => 'CONCAT(0)', 
						'countMultyItem' => 'CONCAT(0)', 
						])
					->from(Page::tableName().' as t2')
					->where('t2.alias=:alias', [':alias' => 'root'])
					->orderBy('t2.position');
				$parentModel = (object)$parentModel->one();

				if($parentModel)
				{
					if(isSet($parentModel->jsonTitle)) $parentModel->title = json_decode($parentModel->jsonTitle);
					$parentModel->visible_ru = json_decode($parentModel->visible_ru);
					$parentModel->visible = json_decode($parentModel->visible);
					if($parentModel->visible == null) $parentModel->visible = $parentModel->visible_ru;

					$models[] = $parentModel;
				}
				else{
					throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не верный корневой узел.'));
				}
			}
				
//die("<pre>".print_r($models, true)."</pre>");
				
			//Подготовка и отправка JSON ответа
			$items = [];
			foreach($models as $model){
				$model = (object)$model;
				$item = [
					'id' => $model->id,
					'label' => $model->title,
					'parent_id' => $model->parent_id,
					'visible' => $model->visible,
					'type' => $model->type,
					'childrenCount' => $model->childrenCount,
					'childrenCountItem' => $model->childrenCountItem,
					'countMultyItem' => $model->countMultyItem,
				];
				
				if($model->childrenCount > 0){
					$item['load_on_demand'] = true;
					//$item['autoOpen'] = true;
				}
				$items[] = $item;
			}
			
			if(count($items) == $limit){
				$items[] = Html::a(Yii::$app->t('custom', 'показать все').'...', Url::to(['index-details', 'id' => $parentModel->id]));
			}
			if($test) die("<pre>".print_r($items, true)."</pre>");
			return $items;
			
		}
		else{   
			return $this->render('index', []);
		}
		
    }
	
	/**
     * Выводит полную информацию о узле
     * @param int $node Идентификатор узла
     * @param int $page Номер страницы
	 * @return null
     */
    public function actionIndexDetails($node = null ,$id = null, $page = 1, $type = 'page', $multyItem = 0)
    {
		//node и id это id родительского узла. Пришлость подстраиваться под виджет дерева
		if(!$id) $id = $node;
		
		if($id){
			$parentModel = $this->findModel((int)$id);
		}
		else{
			$parentModel = Page::find()
				->where(['alias' => 'root'])
				->one();
		}
		
		$searchModel = new PageSearch();
		$searchModel->parent_id = $parentModel->id;
		if($type == 'item') $searchModel->showItem = true;
		if($multyItem == 1) $searchModel->multyItem = true;
		$dataProvider = $searchModel->search(Yii::$app->request->get());
	//die("dataProvider<pre>".print_r($dataProvider, true)."</pre>");
	
		return $this->render('indexDetails', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'parentModel' => $parentModel,
		]);
    }

    /**
     * Displays a single Page model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($parent_id, $type = Page::TYPE_PAGE)
    {
        $model = new Page();
		$model->parent_id = $parent_id;
		$parentPage = $model->parent;

		if(!$parentPage) throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не верный корневой узел.'));
		
		//Получаем конфигурацию шаблона
		$config = $parentPage->templateConfig();
		$keysLevel = array_keys($config['levels']);
		if($type != Page::TYPE_ITEM && isSet($keysLevel[$parentPage->level + 1])) $level = $keysLevel[$parentPage->level + 1];
		elseif($type == Page::TYPE_ITEM && isSet($keysLevel[$parentPage->level])) $level = $keysLevel[$parentPage->level];
		else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'В шаблоне {template} не указан этот уровень вложенности', ['{template}' => $parentPage->parentTemplate]));

		//die("$level<pre>".print_r($config['levels'], true)."</pre>");
		if($type == Page::TYPE_GROUP && in_array(Page::TYPE_GROUP, $config['levels'][$level])) $model->type = Page::TYPE_GROUP;
		elseif($type == Page::TYPE_PAGE && in_array(Page::TYPE_PAGE, $config['levels'][$level])) $model->type = Page::TYPE_PAGE;
		elseif($type == Page::TYPE_ITEM && in_array(Page::TYPE_ITEM, $config['levels'][$level])) $model->type = Page::TYPE_ITEM;
		else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Передан не верный тип страницы.'));
 		
		//die("model<pre>".print_r($model, true)."</pre>");
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->parent_id = $parentPage->id;
			
			if($model->editors){
				$model->editors = json_encode($model->editors);	
			}
			
			$validate = Yii::$app->treePages->addNode($model, $parentPage);
			
			//die("<pre>".print_r($model, true)."</pre>");
			//Проверка шаблона. Если шаблон изменился загружаем по новой модель
			$updateBoolean = $model->testingTemplate();
			
			Yii::$app->session->setFlash('update', Yii::$app->t('warning', 'Данные успешно добавлены.'));
			return $this->redirect(['update', 'id' => $model->id]);
			
        } else {			
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $view //Для возврата во вюху details
     * @return mixed
     */
    public function actionUpdate($id, $view='index')
    {
		$model = $this->findModel($id);

		//Проверка на доступ редакторов
		if(!Yii::$app->user->can('moderator')){

			if($model->editors) $editors = json_decode($model->editors);
			elseif($model->parentEditors) $editors = json_decode($model->parentEditors);
			else $editors = null;
			
			if($editors && in_array(Yii::$app->user->identity->role, $editors)){}
			else{
				throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'У вас нет доступа к этой странице.'));
			}
		}
				
		$post = Yii::$app->request->post();
		$boolLoad = false;

		if(isSet($post['clone'])){
			//$model2 = new Page();
			$model->isNewRecord = true;
			$model->id = null;	
			$model->alias = UrlTransliterate::convertToChpu($model->title).strtotime("now");
			$model->position++;
			unset($model->hashConfig);
			//die("<pre>".print_r($model, true)."</pre>");

			$model->save(false);

			unset($post['Page']['id']);
			unset($post['Page']['alias']);
			unset($post['Page']['position']);
			unset($post['Page']['hashConfig']);
			
			//Проверка шаблона. Если шаблон изменился загружаем по новой модель
			$updateBoolean = $model->testingTemplate();

			unset($model->fields);
			foreach($post['Page'] as $field => $value) $model->$field = $value;
			$boolLoad = true;
		} else{ 
			$boolLoad = $model->load($post);
		}
		//die("model<pre>".print_r($model, true)."</pre>");

		//Заполняем галлерейки файлов и картинок
		if(isSet($post['gallery'])){
			foreach($post['gallery'] as $field => $val){
				$data = $model->convertGallery($val);
				if(array_key_exists($field, (array)$model->fields->attributes)){ 
					$model->{$field} =  json_encode($data);
				}
			}
		}
		
		if(!$model->alias){
			$model->alias = UrlTransliterate::convertToChpu($model->title);	
		}
		
		if(!isSet($post['clone'])){
			//Проверка шаблона. Перед валидацией
			$model->testingTemplate();
		}
		
		//die("<pre>".print_r($model, true)."</pre>");
        if ($boolLoad && $model->validate()) {
			//die("validate<pre>".print_r([$post,$model->fields], true)."</pre>");
			if($model->editors){
				$model->editors = json_encode($model->editors);	
			}
			
			//$model->save();	
			$model->updated_at = strtotime('now');
			Yii::$app->treePages->updateNode($model);
			if(isSet($model->fields)){ //die("<pre>".print_r($model->fields, true)."</pre>");
				$model->fields->save();	
			}
			
			//Проверка шаблона. Если шаблон изменился загружаем по новой модель
			if($model->testingTemplate())
				$model = $this->findModel($model->id);		

			//Вывод
			if(isSet($post['clone'])){ 
				Yii::$app->session->setFlash('update', Yii::$app->t('warning', 'Создана копия страницы.'));
				return $this->redirect(['update', 'id' => $model->id]);
			}
			else Yii::$app->session->setFlash('update', Yii::$app->t('warning', 'Данные успешно обновлены.'));
			
			return $this->render('update', [
                'model' => $model,
            ]);
			//if($view == 'details') return $this->redirect(['index-details']);
			//return $this->redirect(['index']);
        } else {
			//die("getErrors<pre>".print_r($model->getErrors(), true)."</pre>");
           return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

	/**
     * Перенос узла дерева
     * @param int $id
     * @param int $new_parent_id
     * @param int $prev_node_id
     * @return null
     */
    public function actionUpdatePosition($id, $new_parent_id=0, $prev_node_id=0)
    {
		if(Yii::$app->request->isAjax){
			$model = $this->findModel($id);
			
			if((int)$prev_node_id != 0){
				$model_prev_node = Page::easyFindModel($prev_node_id, 'id');
				$prev_node_position = $model_prev_node->position;
			}
			else{
				$prev_node_position = -1; //min
			}
			
			//Изменение позиции узла
			$model->parent_id = $new_parent_id;
			$model->position = $prev_node_position + 1;
			//die("<pre>".print_r($model, true)."</pre>");
			Yii::$app->treePages->updateNode($model);
		}
    }
	
		/**
     * Перенос узла GridView
     * @param int $id
     * @param int $new_parent_id
     * @param int $prev_node_id
     * @return null
     */
    public function actionUpdatePositionDetails()
    {
		if(Yii::$app->request->isAjax){
			$post = Yii::$app->request->post();
			$continue = true;
			if(!isSet($post['items'])){$continue = false;}
			if(!isSet($post['nPage'])){$continue = false;}
			if(!isSet($post['pageSize'])){$continue = false;}

			if($continue){
				$table = Page::tableName();
				$t1 = '';
				$n = 1;
				foreach($post['items'] as $item){
					$item = (int)$item;
					$position = (int)$post['nPage']*(int)$post['pageSize']+$n;
					if($t1 == ''){
						$t1.="(SELECT ($item) as id, ($position) as position)";
					}else{
						$t1.=" UNION (SELECT ($item) as id, ($position) as position)";
					}
					$n++;
				}
				Yii::$app->db->createCommand("
					UPDATE $table,
					($t1) as t1
					SET $table.position = t1.position
					WHERE $table.id = t1.id
				")->execute();
			}
			else{
				echo Yii::$app->t('error', 'Переданы неверные параметры');
			}
		}
	}
	
    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
		$model = Page::easyFindModel($id, 'id');
		
		if(!$model) throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Страница не найдена.'));
		if($model->alias == 'root') throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Нельзя удалять корневую директорию'));
		
		//Собираем всех потомков включая элементы
		$models = Yii::$app->db->createCommand('SELECT id FROM '.Page::tableName().' WHERE path LIKE "%|'.(int)$id.'|%"')
			->queryAll();
		$IDs = [$id];	
		foreach($models as $model){
			$IDs[] = $model['id'];
		}	
		$IDs = implode(",", $IDs);
		
		//die("Page<pre>".print_r($IDs, true)."</pre>");	
		$models = Yii::$app->db->createCommand('DELETE FROM '.Page::tableName().' WHERE id IN ('.$IDs.') OR parent_id IN ('.$IDs.')')
			->execute();
		
        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
    }
	
	/**
     * Панель информации о выделенной странице
     * @param int $id
     * @return string
     */
 	public function actionInformer($id){
		$model = $this->findModel($id);
		$this->layout = '/blank';
		//Получаем конфигурацию шаблона
		$config = $model->templateConfig();
		
		echo $this->render('_informer', [
                'model' => $model,
                'config' => $config,
        ]);
	}

	public function actionTest(){
		
				$page = Page::findPage('main');

		die('<pre>'.print_r($page, true).'</pre><br/>');
 
		
	}
	
	/**
     * Валидация модального окна
     * @return mixed
     */
    public function actionModalValidation(){
		$jsonFieldsModel = new DynamicForm();
		$jsonFieldsModel->pre = $key.'_';
		$config['fields']['title'] = ['Заголовок', Fields::TYPE_TEXT];
		$config['fields']['index'] = ['id', 'hidden'];

		$jsonFieldsModel->setFields($config['fields']);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {}
	}

	/**
     * Добавление itemParentId
	 * Двойные связки с родительскими категориями
	 * Только для элементов
     * @return mixed
     */
    public function actionAddItemParentId($id, $addAlias){
	//die('actionAddItemParentId id='.$id.' addAlias='.$addAlias);
		$item = Page::find()
			->where('page.id = :id', [':id' => $id])
			->one();
			
		if($item && $item->type == Page::TYPE_ITEM){}
		else Yii::$app->commonCustom->returnApi(false, [], ['Ошибка! Не верный элемент!']);
		
		$itemParentsId = explode('|', $item->itemParentsID);
		foreach($itemParentsId as $index => $val){
			if(!$val) unset($itemParentsId[$index]);
		}		

		$page = Page::find()
			->where('page.alias = :alias', [':alias' => $addAlias])
			->one();
	
		if($page){
			if(count($itemParentsId)){
				if(in_array($page->id, $itemParentsId)) Yii::$app->commonCustom->returnApi(false, [], ['Ошибка! Элемент уже прикреплен к этому идентификатору!']);
			}	
			if($page->id == $item->parent_id){
				Yii::$app->commonCustom->returnApi(false, [], ['Ошибка! Элемент уже прикреплен к этому идентификатору!']);
			} else {
				$itemParentsId[] = $page->id;
				$itemParentsId = implode("|",$itemParentsId);
				$item->itemParentsID = '|'.$itemParentsId.'|';
				$item->save(false);
				
				$html = $this->renderPartial('/../widgets/fields/views/__parentId', ['id' => $item->id, 'page' => $page]);
				
				Yii::$app->commonCustom->returnApi(true, ['html' => $html], ['Элемент связан с категорией '.$addAlias]);
			}
		} else Yii::$app->commonCustom->returnApi(false, [], ['Ошибка! Не верный идентификатор!']);

		//die('actionAddItemParentId id='.$id.' addAlias='.$addAlias);
	}
	
	public function actionRemoveItemParentId($id, $removeId){
		$item = Page::find()
			->where('page.id = :id', [':id' => $id])
			->one();
			
		if($item && $item->type == Page::TYPE_ITEM){}
		else Yii::$app->commonCustom->returnApi(false, [], ['Ошибка! Не верный элемент!']);

		$itemParentsId = explode('|', $item->itemParentsID);
		foreach($itemParentsId as $index => $val){
			if(!$val || $val == $removeId) unset($itemParentsId[$index]);
		}	
		
		$itemParentsId = implode("|",$itemParentsId);
		if(trim($itemParentsId)) $item->itemParentsID = '|'.$itemParentsId.'|';
		else $item->itemParentsID = '|';
		$item->save(false);
		
		Yii::$app->commonCustom->returnApi(true, ['removeId' => $removeId], ['Элемент удален!']);
	}
	
    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
 			throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Страница не найдена.'));
        }
    }
	
}
