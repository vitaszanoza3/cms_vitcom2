<?php

use yii\helpers\Html;
use common\models\Page;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->title;

$parentPage = $model->parent;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->t('custom', 'Страницы'), 'url' => ['index']];
if($parentPage) $this->params['breadcrumbs'][] = ['label' => $parentPage->title, 'url' => ['index-details', 'id' => $parentPage->id]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = Yii::$app->t('custom', 'Редактирование');
?>
<div class="page-update">

	<?= Yii::$app->t('custom', $model->type) ?>
    <h1> <?= Html::encode($this->title) ?></h1>
	
	<?php if($printFields = $model->printFields()): ?>
	<p>
	  <a class="btn btn-primary" data-toggle="collapse" href="#collapseTemplate" role="button" aria-expanded="false" aria-controls="collapseTemplate">
		Поля для шаблона
	  </a>
	</p>
	<div class="collapse" id="collapseTemplate">
		<div class="card card-body">
			<pre><?= print_r($printFields, true)?></pre>
		</div>
	</div>
	<?php endif ?>
	</br>

	<?php if(Yii::$app->session->getFlash('update')): ?>
		<div class="alert alert-success" role="alert">
			<strong><?= Yii::$app->session->getFlash('update') ?></strong>
		</div>
	<?php endif ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

	



</div>
