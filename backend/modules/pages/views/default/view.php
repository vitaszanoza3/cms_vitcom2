<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Page;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->title;
	if($model->parent_id != 0){
		$this->params['breadcrumbs'][] = ['label' => Yii::$app->t('custom', 'Страницы'), 'url' => ['index']];
		if($model->parent) {
			if($model->type != Page::TYPE_ITEM) $this->params['breadcrumbs'][] = ['label' => $model->parent->title, 'url' => ['index-details', 'id' => $model->parent->id]];
			else $this->params['breadcrumbs'][] = ['label' => $model->parent->title, 'url' => ['index-details', 'id' => $model->parent->id, 'type' => Page::TYPE_ITEM]];
		}
		$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['index-details', 'id' => $model->id]];
	}else{
		$this->params['breadcrumbs'][] = 
		[
			'label' => $this->title,
			'url' => Url::to(['index']),
		];
	}
//Получаем конфигурацию шаблона
$config = $model->templateConfig();
$keysLevel = array_keys($config['levels']);
$keyLevel = (isSet($keysLevel[$model->level]))?$keysLevel[$model->level]:null;

?>
<?php if($model->level && is_null($keyLevel)): ?>
<div class="alert alert-danger" role="alert">
	<strong>Ошибка! Страница не описана в конфигурации!</strong>
</div>
<?php endif; ?>

<div class="page-view">

	<?= Yii::$app->t('custom', $model->type) ?>
     <h1><?= Html::encode($this->title) ?></h1> 

	<?php if(!is_null($keyLevel) || !$model->level): ?>

    <p>
		<?php
		if(isSet($keysLevel[$model->level + 1])):
		$level = $keysLevel[$model->level + 1];
			if(Yii::$app->params['startLanguage'] == Yii::$app->language):
			?>
				<?php if(in_array(Page::TYPE_GROUP, $config['levels'][$level])): ?>
					<?= Html::a('Создать группу', ['create', 'parent_id' => $model->id, 'type' => Page::TYPE_GROUP], ['class' => 'btn btn-success']) ?>
				<?php endif; ?>
				<?php if(in_array(Page::TYPE_PAGE, $config['levels'][$level])): ?>
					<?= Html::a('Создать страницу', ['create', 'parent_id' => $model->id, 'type' => Page::TYPE_PAGE], ['class' => 'btn btn-success']) ?>
				<?php endif; ?>
				
			<?php 
			endif;
		endif; ?>
		
		<?php if(isSet($keysLevel[$model->level])): 
			$thisLevel = $keysLevel[$model->level];
		//die("$thisLevel<pre>".print_r($keysLevel, true)."</pre>");
			?>
			<?php if(Yii::$app->params['startLanguage'] == Yii::$app->language && in_array(Page::TYPE_ITEM, $config['levels'][$thisLevel])): ?>
					<?= Html::a('Создать элемент', ['create', 'parent_id' => $model->id, 'type' => Page::TYPE_ITEM], ['class' => 'btn btn-success']) ?>
			<?php endif; ?>
		<?php endif; ?>
	
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
		<a href="<?= $model->createUrl()->path ?>" target="_blank" class="btn">посмотреть на сайте</a>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parent_id',
			'position',
            'alias',
            'template',
            'type',
            'controller',
            'in_search',
            'title',
            'created_at',
            'updated_at',
        ],
    ]) ?>
	<?php endif; ?>
</div>
