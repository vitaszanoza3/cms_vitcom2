<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parent_id') ?>

    <?= $form->field($model, 'alias') ?>

    <?= $form->field($model, 'techid') ?>

    <?= $form->field($model, 'template') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'controller') ?>

    <?php // echo $form->field($model, '_left') ?>

    <?php // echo $form->field($model, '_right') ?>

    <?php // echo $form->field($model, '_level') ?>

    <?php // echo $form->field($model, 'in_search') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
