<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\models\Page;
use backend\modules\pages\PagesAsset;

$bundle = PagesAsset::register($this);
$this->registerJsFile($bundle->baseUrl .'/index-details.js');

/* @var $this yii\web\View */
/* @var $searchModel common\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

	$this->title = Yii::$app->t('custom', 'Страницы');

	if($searchModel->parent_id != 0){
		$this->params['breadcrumbs'][] = ['label' => Yii::$app->t('custom', 'Страницы'), 'url' => ['index']];
		if($parentModel->parent) $this->params['breadcrumbs'][] = ['label' => $parentModel->parent->title, 'url' => ['index-details', 'id' => $parentModel->parent->id]];
		$this->params['breadcrumbs'][] = ['label' => $parentModel->title, 'url' => ['view', 'id' => $parentModel->id]];
	}else{
		$this->params['breadcrumbs'][] = 
		[
			'label' => $this->title,
			'url' => Url::to(['index']),
		];
	}
	
	$type = Yii::$app->request->get('type');
	$multyItem = Yii::$app->request->get('multyItem');
?>

<script>

	var nPage = <?= $dataProvider->pagination->page ?>;
	var pageSize = <?= $dataProvider->pagination->pageSize ?>;
	var blockDragAndDrop = false; //Блокировка переноса узлов для едиторов

</script>

<div class="page-index-details">

    <h1><?= Html::encode($parentModel->title) ?></h1>
	(
	<?= ($type == Page::TYPE_ITEM)? Html::a('страницы', ['index-details', 'id' => $searchModel->parent_id]):'страницы' ?>
	/ <?= ($type != Page::TYPE_ITEM || ($type == Page::TYPE_ITEM && $multyItem))? Html::a('элементы', ['index-details', 'id' => $searchModel->parent_id, 'type' => Page::TYPE_ITEM]):'элементы' ?>
	/ <?= ($type != Page::TYPE_ITEM || ($type == Page::TYPE_ITEM && !$multyItem))? Html::a('мульти связка', ['index-details', 'id' => $searchModel->parent_id, 'type' => Page::TYPE_ITEM, 'multyItem' => 1]):'мульти связка' ?>
	)

    <p>
	<?php
	$config = $parentModel->templateConfig();
	$keysLevel = array_keys($config['levels']);
	if(isSet($keysLevel[$parentModel->level + 1])):
	$level = $keysLevel[$parentModel->level + 1];
		if(Yii::$app->params['startLanguage'] == Yii::$app->language):
		?>
			<?php if(in_array(Page::TYPE_GROUP, $config['levels'][$level])): ?>
				<?= Html::a('Создать группу', ['create', 'parent_id' => $parentModel->id, 'type' => Page::TYPE_GROUP], ['class' => 'btn btn-success']) ?>
			<?php endif; ?>
			<?php if(in_array(Page::TYPE_PAGE, $config['levels'][$level])): ?>
				<?= Html::a('Создать страницу', ['create', 'parent_id' => $parentModel->id, 'type' => Page::TYPE_PAGE], ['class' => 'btn btn-success']) ?>
			<?php endif; ?>

		<?php 
		endif;
	endif; ?>
	
	<?php if(isSet($keysLevel[$parentModel->level])): 
		$thisLevel = $keysLevel[$parentModel->level];
	//die("$thisLevel<pre>".print_r($keysLevel, true)."</pre>");
		?>
		<?php if(Yii::$app->params['startLanguage'] == Yii::$app->language && in_array(Page::TYPE_ITEM, $config['levels'][$thisLevel])): ?>
				<?= Html::a('Создать элемент', ['create', 'parent_id' => $parentModel->id, 'type' => Page::TYPE_ITEM], ['class' => 'btn btn-success']) ?>
		<?php endif; ?>

	<?php endif; ?>
	<?= Html::a('Обновить', ['update', 'id' => $parentModel->id], ['class' => 'btn btn-primary']) ?>
    </p>
	
	<?php
	$columns = [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            //'parent_id',
			'alias',
			[
				'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
				'attribute' => 'title',
				'label' => 'title',
				'format' => 'text',
				'filter' => true,
				'value' => function ($data) {
					if(isSet($data['jsonTitle'])) $data['title'] = json_decode($data['jsonTitle']);
					return $data['title']; 
				},
			],
            //'type',
			[
				'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
				'attribute' => 'type',
				'label' => 'type',
				'format' => 'text',
				'filter' => [
					'group' => 'group',
					'page' => 'page',
					'item' => 'item',
				],
				'value' => function ($data) {
					return $data['type']; 
				},
			],
			//'visible',
			[
				'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
				'attribute' => 'visible',
				'label' => 'visible',
				'format' => 'text',
				'filter' => [
					0 => 'Опубликована',
					1 => 'Черновик',
					2 => 'Скрытая',
					3 => 'Данные',
				],
				'value' => function ($data) {
					$visible = json_decode($data['visible']);
					//return (int)$visible;
					return (isSet(Page::getVisibleList()[$visible]))?Page::getVisibleList()[$visible]:null;
				},
			],
            //'template',
            // 'controller',		
    ];
	
	if($type != Page::TYPE_ITEM){
		$columns[] = 'template';
		$columns[] = 'controller';
	}
	
	//position
	if(!$multyItem){
		$columns[] = [
					'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
					'attribute' => 'position',
					'label' => 'position',
					'format' => 'raw',
					'filter' => false,
					'value' => function ($data) {
						return $data['position'].' <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-down-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M11.5 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L11 2.707V14.5a.5.5 0 0 0 .5.5zm-7-14a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L4 13.293V1.5a.5.5 0 0 1 .5-.5z"/>
							</svg>'; 
					},
		];
	}
	
	//buttons
	$columns[] = [
			   'class' => \yii\grid\ActionColumn::className(),
			   'buttons' => [
					'index-details' => function ($url, $model) {
						$model = (object)$model;
						$data = '';
						if($model->childrenCount > 0)
							$data .= Html::a(' <span class="glyphicon glyphicon-plus"></span>', $url, [
										//'title' => Yii::t('app', 'View'),
										//'class'=>'btn btn-primary btn-xs',                                  
							]);
						if($model->childrenCountItem > 0){
							$data .= Html::a(' <span class="badge">'.$model->childrenCountItem.'</span>', $url + ['type' => 'item'], [                                  
							]);
						}
						if($model->countMultyItem > 0){
							$data .= Html::a(' <span class="badge color-blue">'.$model->countMultyItem.'</span>', $url + ['type' => 'item', 'multyItem' => 1], [                                  
							]);
						}
						return $data;
					},
				],

			   'urlCreator'=>function($action, $model, $key, $index){
						$model = (object)$model;
					    return [$action, 'id'=>$model->id, 'view'=>'details'];
				   },
			   'template'=>'{view} {update} {delete}{index-details}',
	];
	?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'tableOptions' => [
            'class' => 'table table-striped table-bordered sortable'
        ],
        'columns' => $columns,
    ]); ?>

</div>
