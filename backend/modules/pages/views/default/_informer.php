<?php
use yii\helpers\Html;
use common\models\Page;

$keysLevel = array_keys($config['levels']);
if(isSet($keysLevel[$model->level + 1])):
	$level = $keysLevel[$model->level + 1];
	if(Yii::$app->params['startLanguage'] == Yii::$app->language):
		?>
		<?php if(in_array(Page::TYPE_GROUP, $config['levels'][$level])): ?>
			<?= Html::a('Создать группу', ['create', 'parent_id' => $model->id, 'type' => Page::TYPE_GROUP], ['class' => 'btn btn-success']) ?>
		<?php endif; ?>
		<?php if(in_array(Page::TYPE_PAGE, $config['levels'][$level])): ?>
			<?= Html::a('Создать страницу', ['create', 'parent_id' => $model->id, 'type' => Page::TYPE_PAGE], ['class' => 'btn btn-success']) ?>
		<?php endif; ?>
<?php 
	endif;
endif; ?>

<?php if(isSet($keysLevel[$model->level])): 
$thisLevel = $keysLevel[$model->level];
?>
<?php if(Yii::$app->params['startLanguage'] == Yii::$app->language && in_array(Page::TYPE_ITEM, $config['levels'][$thisLevel])): ?>
		<?= Html::a('Создать элемент', ['create', 'parent_id' => $model->id, 'type' => Page::TYPE_ITEM], ['class' => 'btn btn-success']) ?>
	<?php endif; ?>
<?php endif; ?>

<?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<?= Html::a('Удалить', ['delete', 'id' => $model->id], [
	'class' => 'btn btn-danger',
	'data' => [
		'confirm' => Yii::$app->t('warning', 'Вы уверены, что хотите удалить этот элемент?'),
		'method' => 'post',
	],
]) ?>

<div class="panelInfo">
	<div id="title"><label><?= Yii::$app->t('custom', 'title') ?>:</label> <?= $model->title ?></div>
	<div id="alias"><label><?= Yii::$app->t('custom', 'alias') ?>:</label> <?= $model->alias ?></div>
	<div id="type"><label><?= Yii::$app->t('custom', 'type') ?>:</label> <?= $model->type ?></div>
	<div id="template"><label><?= Yii::$app->t('custom', 'template') ?>:</label> <?= $model->template ?></div>
	<div id="controller"><label><?= Yii::$app->t('custom', 'controller') ?>:</label> <?= $model->controller ?></div>
	<div id="editors"><label><?= Yii::$app->t('custom', 'editors') ?>:</label> <?= $model->editors ?></div>
	<div id="editors"><label><?= Yii::$app->t('custom', 'visible') ?>:</label> <?= (isSet($model->visibleList[$model->visible]))? $model->visibleList[$model->visible]:'' ?></div>
</div>

<p><a href="<?= $model->createUrl()->path ?>" target="_blank">посмотреть</a>