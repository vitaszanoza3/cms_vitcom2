<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\Url;
use yii\bootstrap\Modal;

use common\models\RoleEditor;
use common\models\Page;
use common\models\Fields;
use backend\modules\pages\widgets\fields\CKEditorWidget;
use backend\modules\pages\widgets\fields\GalleryWidget;
use backend\modules\pages\widgets\fields\JsonFieldsWidget;
use backend\modules\pages\widgets\fields\MailFormWidget;
use backend\modules\pages\widgets\fields\FormWidget;
use backend\modules\pages\widgets\fields\ParensIdFieldsWidget;

use backend\modules\pages\PagesAsset;
$bundle = PagesAsset::register($this);

$this->registerJsFile($bundle->baseUrl .'/jsonModal.js', ['position' => \yii\web\View::POS_END]);
$this->registerJsFile($bundle->baseUrl .'/mailFormModal.js', ['position' => \yii\web\View::POS_END]);
$this->registerJsFile($bundle->baseUrl .'/itemParentsId.js', ['position' => \yii\web\View::POS_END]);

$this->registerJsFile('/backend/web/extensions/ckeditor/ckeditor.js', ['position' => \yii\web\View::POS_END]);
$this->registerJsFile('/backend/web/extensions/ckfinder3_5/ckfinder.js', ['position' => \yii\web\View::POS_END]);
$this->registerJsFile('/backend/web/extensions/ckfinder3_5/ckfinder_popup.js', ['position' => \yii\web\View::POS_END]);
$this->registerJsFile('/backend/web/js/jquery.inputmask.js', ['position' => \yii\web\View::POS_END]);
$this->registerJsFile('/backend/web/js/custom.js', ['position' => \yii\web\View::POS_END]);

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */

//$ckeditorPath = $this->assetManager->publish( Yii::getAlias('@ext/ckeditor/ckeditor.js') );
//die("<pre>".print_r($ckeditorPath, true)."</pre>");
//$this->registerJsFile($ckeditorPath[1]);
//$this->registerJsFile('/backend/web/assets/31e811f/ckeditor.js', ['position' => \yii\web\View::POS_HEAD]);
//$ck_editor = array();

//Получаем конфигурацию шаблона
$config = $model->templateConfig();
$keysLevel = array_keys($config['levels']);
$keyLevel = (isSet($keysLevel[$model->level]))?$keysLevel[$model->level]:null;

//die("model<pre>".print_r($model, true)."</pre>");

?>

<script>
$(function ($) {
 
});
</script>

<?php if($model->level && is_null($keyLevel)): ?>
<div class="alert alert-danger" role="alert">
	<strong>Ошибка! Страница не описана в конфигурации!</strong>
</div>
<?php endif; ?>

<?php if(!is_null($keyLevel) || !$model->level): ?>
<div class="page-form">

    <?php $form = ActiveForm::begin([
	]); ?>
	<?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
   
    <?php 
		if($model->type == Page::TYPE_ITEM && !$model->isNewRecord)
			echo $form->field($model, 'itemParentsID')->widget(ParensIdFieldsWidget::className(),['form'=> $form]);
	?>

	 <?php 
		if($model->type != Page::TYPE_ITEM && !$model->isNewRecord)
			echo $form->field($model, 'on_itemParentsID')->checkbox();
	?>

    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'date_display')->widget(DatePicker::classname(), [
		 'language' => 'ru',
		 'dateFormat' => 'php:d/m/Y',
		 'options' => [
			'class' => 'form-control',
			'enctype'=>'multipart/form-data',
			'type_field' => 'TYPE_DATE'
		 ],
	 ]) ?>

	 <?= $form->field($model, 'visible')->dropDownList($model->visibleList, ['prompt' => '']) ?>

	 <?php 
	//Закрыто для модераторов
	if(Yii::$app->user->can('admin') && $model->type != Page::TYPE_ITEM):
	?>
		<div class="panel panel-primary">
			<div class="panel-heading"> 
				<h3 class="panel-title">Панель администратора</h3> 
			</div> 
			<div class="panel-body">
				<?= $form->field($model, 'type')->dropDownList($model->typeList) ?>
				
				<?= $form->field($model, 'template')->dropDownList($model->templateList , ['prompt' => '']) ?>
				
				<?= $form->field($model, 'controller')->dropDownList($model->controllerList , ['prompt' => '']) ?>

				<?= $form->field($model, 'in_search')->radioList(array( 1 => 'Да', '' => 'Нет')); ?>
			</div>
		</div>
	<?php endif ?>
	
	<?php 
	//Закрыто для редакторов
	if(Yii::$app->user->can('moderator')):
	?>
	<div class="panel panel-info"> 
		<div class="panel-heading"> 
			<h3 class="panel-title">Панель модератора</h3>
		</div> 
		<div class="panel-body">
			<?php
			$model->editors = json_decode($model->editors, true);
			echo $form->field($model, 'editors')->checkboxList(RoleEditor::editors());
			?>
		</div> 
	</div>
	<?php endif ?>


	<?php 
		//die("printFields<pre>".print_r([$model], true)."</pre>");

	if(Yii::$app->controller->action->id == 'update' && isSet($model->fields)): 
		echo '<hr/>';
		
		if($model->type != Page::TYPE_GROUP){
			$fields = $model->printFields();
			if(isSet($fields['fields'])){
				foreach($fields['fields'] as $fieldName => $value){

					if($fieldName == 'id' || $fieldName == 'page_id' || $fieldName == 'lang') continue;
					
					$field = $fieldName;
					//$field = str_replace("f_", '', $fieldName);
					$fieldName = 'f_'.$fieldName;

					//if($field == 'city') die('city '.$model->parentFieldsGroup);
					if(!is_null($keyLevel) && isSet($config['fields'][$field]) && isSet($config['fields'][$field]['levels'][$keyLevel]) && in_array($model->type, $config['fields'][$field]['levels'][$keyLevel])){
					
						$type = $config['fields'][$field]['type'];
						//if($field == 'city') die('city='.$type);

						switch(Fields::preset($type)['field']){
							case 'textInput': echo $form->field($model, $fieldName)->textInput(['maxlength' => true]); break;
							case 'urlInput': echo $form->field($model, $fieldName.'[0]')->textInput(['maxlength' => true, 'placeholder' => 'название ссылки']); 
								echo $form->field($model, $fieldName.'[1]')->textInput(['maxlength' => true, 'placeholder' => 'ссылка'])->label(false);
								break;
							case 'phoneInput': echo $form->field($model, $fieldName)->textInput(['maxlength' => true, 'type_field' => $type]); break;
							case 'passwordInput': echo $form->field($model, $fieldName)->passwordInput(); break;
							case 'textarea': echo $form->field($model, $fieldName)->textarea(['rows'=>3,'cols'=>5]); break;
							case 'ck_editor': echo $form->field($model, $fieldName)->widget(CKEditorWidget::className(),[]); break;
							case 'ckfinder': echo $form->field($model, $fieldName)->widget(GalleryWidget::className(),['type' => $type, 'config' => $config['fields'][$field], 'form' => $form]); break;
							case 'json': echo $form->field($model, $fieldName)->widget(JsonFieldsWidget::className(),['form'=> $form, 'config' => $config['fields'][$field]]); break;
							case 'form': echo $form->field($model, $fieldName)->widget(FormWidget::className(),['form'=> $form, 'config' => $config['fields'][$field]]); break;
							case 'mailForm': echo $form->field($model, $fieldName)->widget(MailFormWidget::className(),['form'=> $form, 'config' => $config['fields'][$field]]); break;
							case 'dropDownList': echo $form->field($model, $fieldName)->dropDownList(Yii::$app->dataListComponent->{$config['fields'][$field]['function']}($config['fields'][$field]['params'])); break;
							case 'checkbox': echo $form->field($model, $fieldName)->checkbox(); break;
							case 'checkboxList': echo $form->field($model, $fieldName)->checkboxList(Yii::$app->dataListComponent->{$config['fields'][$field]['function']}($config['fields'][$field]['params'])); break;
							case 'radioList': echo $form->field($model, $fieldName)->radioList(Yii::$app->dataListComponent->{$config['fields'][$field]['function']}($config['fields'][$field]['params'])); break;
							case 'fileInput': echo $form->field($model, $fieldName)->fileInput(); break;
							case 'datePicker': echo $form->field($model, $fieldName)->widget(DatePicker::classname(), [
									 'language' => 'ru',
									 'dateFormat' => 'php:d/m/Y',
									 'options' => [
										'class' => 'form-control',
										'enctype'=>'multipart/form-data',
										'type_field' => $type
									 ],
								 ]); break;
						}
							

					}
					
				}
			}
		}
	endif;
	?>
	
	<?= $form->field($model, 'parent_id')->hiddenInput()->label(false); ?>
	
    <div class="form-group">
		<?php if(Yii::$app->controller->action->id == 'update'){
				echo Html::submitButton('Обновить', ['class' => 'btn btn-primary', 'name' => 'update']);
				if(Yii::$app->params['startLanguage'] == Yii::$app->language){
					echo " ".Html::submitButton('Клонировать', [
						'class' => 'btn btn-success', 
						'name' => 'clone',
						//'formaction' => Url::to(['create', 'parent_id' => $model->parent_id, 'type' => $model->type]),
						]);
				}	
		}else{
			if(Yii::$app->params['startLanguage'] == Yii::$app->language){
				echo Html::submitButton('Создать', ['class' => 'btn btn-success', 'name' => 'create']);
			}
		}
		?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php endif; ?>
