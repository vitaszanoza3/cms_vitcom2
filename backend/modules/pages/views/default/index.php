<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\pages\PagesAsset;
$bundle = PagesAsset::register($this);

$this->registerJsFile(Yii::$app->request->baseUrl.'/js/tree.jquery.js');
$this->registerJsFile($bundle->baseUrl .'/tree.js');
//$this->registerJsFile($bundle->baseUrl .'/jsonModal.js');
$this->registerCssFile(Yii::$app->request->baseUrl.'/css/jqtree.css');

$this->title = Yii::$app->t('custom', 'Страницы');
$this->params['breadcrumbs'][] = $this->title;

$auth = Yii::$app->authManager;
?>

<script>
<?php if(Yii::$app->user->can('moderator')): ?>
	var blockDragAndDrop = false; //Блокировка переноса узлов для едиторов
<?php else: ?>
	var blockDragAndDrop = true; //Блокировка переноса узлов для едиторов
<?php endif ?>
</script>

<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>
 
 	<div>
		<div class="body-column1">
			<div class="pageInformer">
				
			</div>
		</div>
		<div class="body-column2">
			<div id="tree" data-url="/backend/web/pages/default/index">tree</div>   
		</div>
</div>
