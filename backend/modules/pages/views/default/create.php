<?php

use yii\helpers\Html;
use common\models\Page;
use backend\modules\pages\PagesAsset;


/* @var $this yii\web\View */
/* @var $model common\models\Page */

$type = Yii::$app->request->get('type');
if($type == Page::TYPE_GROUP) $this->title = 'Создание группы';
if($type == Page::TYPE_PAGE) $this->title = 'Создание страницы';
if($type == Page::TYPE_ITEM) $this->title = 'Создание элемента';

$parentPage = $model->parent;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->t('custom', 'Страницы'), 'url' => ['index']];
if($parentPage) $this->params['breadcrumbs'][] = ['label' => $parentPage->title, 'url' => ['view', 'id' => $parentPage->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
