<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use backend\modules\pages\models\DynamicForm;
use common\models\Fields;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

use backend\modules\pages\widgets\fields\CKEditorWidget;
use backend\modules\pages\widgets\fields\GalleryWidget;

$jsonFieldsModel = new DynamicForm($key);
$config['fields'] = [
	'on' => ['', Fields::TYPE_CHECKBOX_LIST, 'function' => 'defaultData', 
		'params' => [
			'values' => [
				'on' => 'Включить',
			],
		],
	],
	'title' => ['Заголовок', Fields::TYPE_TEXT, 'required' => true],
	'full_text' => ['Контент', Fields::TYPE_CK_EDITOR],
	'from_email' => ['from email', Fields::TYPE_EMAIL, 'required' => true],
	'to_email' => ['to email', Fields::TYPE_TEXT, 'required' => true],
	'success' => ['Текст успеха', Fields::TYPE_TEXT, 'required' => true],
];

$jsonFieldsModel->setFields($config['fields']);
//die('config<pre>'.print_r($config, true).'</pre>');

$this->beginBlock('modal_'.$key);

	Modal::begin([
		'header' => '<h2>'.$config['label'].'</h2>',
		//'toggleButton' => [
			//'label' => 'click Modal',
			//'class' => 'btn btn-success',
		//],
		'options' => [
			'id' => 'modal_'.$key,
		],
		'footer' => '<a onclick="serializeMailForm(\'modal_'.$key.'\')" class="btn btn-success">Сохранить</a>',
	]);
	 
		$form = ActiveForm::begin([
			'id' => 'form_'.$key,
			'action' => '#',
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			'options' => [
				'data-type' => 'mail',
			],
		]); 
		
			foreach($jsonFieldsModel->attributes as $fieldName => $value){
				$shortFieldName = str_replace($key.'_', '', $fieldName);
				if($shortFieldName == 'id') continue;
				//echo $form->field($jsonFieldsModel, $field)->textInput(['maxlength' => true]);
				$type = $config['fields'][$shortFieldName][1];
				switch(Fields::preset($type)['field']){
					case 'textInput': echo $form->field($jsonFieldsModel, $fieldName)->textInput(['maxlength' => true]); break;
					case 'urlInput': echo $form->field($jsonFieldsModel, $fieldName.'[0]')->textInput(['maxlength' => true, 'placeholder' => 'название ссылки']); 
						echo $form->field($jsonFieldsModel, $fieldName.'[1]')->textInput(['maxlength' => true, 'placeholder' => 'ссылка'])->label(false);
						break;
					case 'passwordInput': echo $form->field($jsonFieldsModel, $fieldName)->passwordInput(); break;
					case 'textarea': echo $form->field($jsonFieldsModel, $fieldName)->textarea(['rows'=>3,'cols'=>5]); break;
					case 'ck_editor': echo $form->field($jsonFieldsModel, $fieldName)->widget(CKEditorWidget::className(),[
						//'id' => $key.'_'.$fieldName
					]); break;
					case 'dropDownList': echo $form->field($jsonFieldsModel, $fieldName)->dropDownList(Yii::$app->dataListComponent->{$config['fields'][$shortFieldName]['function']}($config['fields'][$shortFieldName]['params'])); break;
					case 'checkboxList': echo $form->field($jsonFieldsModel, $fieldName.'[]')->checkboxList(Yii::$app->dataListComponent->{$config['fields'][$shortFieldName]['function']}($config['fields'][$shortFieldName]['params'])); break;
					case 'radioList': echo $form->field($jsonFieldsModel, $fieldName)->radioList(Yii::$app->dataListComponent->{$config['fields'][$shortFieldName]['function']}($config['fields'][$shortFieldName]['params'])); break;
					//case 'fileInput': echo $form->field($jsonFieldsModel, $fieldName)->fileInput(); break;
					case 'fileInput': echo $form->field($jsonFieldsModel, $fieldName)->render('
						<label class="control-label">'.$jsonFieldsModel->attributeLabels()[$fieldName].'</label></br>
						<input type="text" id="'.'dynamicform-'.$fieldName.'" class="form-control" name="DynamicForm['.$fieldName.']" value="" aria-invalid="false" style="display: inline-block;width: 87%;"> 
						<a onclick="selectFileWithCKFinder(this.parentNode.querySelector(\'#dynamicform-'.$fieldName.'\'))" class="btn btn-success">Обзор</a>
						<div class="help-block"></div>
					');  break;
					case 'datePicker': echo $form->field($jsonFieldsModel, $fieldName)->widget(DatePicker::classname(), [
							'language' => 'ru',
							'dateFormat' => 'php:d/m/Y',
							'options' => [
								'class' => 'form-control',
								'enctype'=>'multipart/form-data',
								
							],
						 ]); break;
					case 'hiddenInput': echo $form->field($jsonFieldsModel, $fieldName)->hiddenInput()->label(false); break;
				}
			}
			//echo $form->field($jsonFieldsModel, 'index')->hiddenInput()->label(false);
			echo $form->errorSummary($jsonFieldsModel);

		ActiveForm::end();
	Modal::end();
	
$this->endBlock();
?>
<div class="container mailFormField" data-field="<?= $key ?>"></div>

