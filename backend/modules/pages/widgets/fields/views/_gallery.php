<?php $n=0; foreach($data as $item): ?>
<div id="row_<?= $field ?>_<?= $n ?>" class="row">
	<div class="col-xs-2">
		<a href="<?= $item->url ?>" target="_blank">
		<img src="<?= $item->thumbnail ?>" alt="<?= $item->url ?>" title="<?= $item->url ?>">
		</a>
		<span> &nbsp;<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-down-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.5 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L11 2.707V14.5a.5.5 0 0 0 .5.5zm-7-14a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L4 13.293V1.5a.5.5 0 0 1 .5-.5z"></path></svg></span>
	</div>
	<div class="col-lg-4">
		<input type="hidden" name="gallery[<?= $field ?>][url][]" id="url_<?= $field ?>_<?= $n ?>" value="<?= $item->url ?>">
		<input type="hidden" name="gallery[<?= $field ?>][thumbnail][]" id="thumbnail_<?= $field ?>_<?= $n ?>" value="<?= $item->thumbnail ?>">
		<input type="text" name="gallery[<?= $field ?>][title][]" id="title_<?= $field ?>_<?= $n ?>" placeholder="Заголовок" value="<?= $item->title ?>">
		<textarea rows="4" name="gallery[<?= $field ?>][text][]" id="text_<?= $field ?>_<?= $n ?>" placeholder="Описание"><?= $item->text ?></textarea>
	</div>
	<div class="col-md-6">
		<svg style="cursor: pointer;" onclick="galleryItemRemove('row_<?= $field ?>_<?= $n ?>')" width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x" fill="red" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path></svg>
	</div>
</div>
<?php $n++; endforeach ?>