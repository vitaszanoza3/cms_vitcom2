<?php

namespace backend\modules\pages\widgets\fields;

use yii\base\Widget;
use yii\helpers\Html;

class GalleryWidget extends Widget
{
    public $model, $attribute, $view, $type, $config, $form;

    public function init()
    {
        parent::init();
    }

    public function run()
    { 
	//die($this->attribute." model<pre>".print_r($this->model, true)."</pre>");

		//die('<pre>'.print_r($this->config, true).'</pre>');
		//echo 'model=<pre>'.print_r($this->model, true).'</pre><br/>';
		//echo 'attribute=<pre>'.print_r($this->attribute, true).'</pre><br/>';
		//echo 'view=<pre>'.print_r($this->view, true).'</pre><br/>';
		//echo 'attribute=<pre>'.print_r($this->model->{$this->attribute}, true).'</pre><br/>';
		//die();
		if($this->model->{$this->attribute}) $count = count($this->model->{$this->attribute});
		else $count = 0;
		$result = '<input type="hidden" id="page-'.$this->attribute.'" class="form-control" name="Page['.$this->attribute.']" value="'.(($count > 0)?$count:null).'" maxlength="255" aria-required="true" aria-invalid="false">';
		$container = '';
		$result .= '<div class="fieldGallery" id="'.$this->attribute.'">';
			$result .= '<script>
				var n_'.$this->attribute.' = 0;
				var limit_'.$this->attribute.' = '.(isSet($this->config['limit'])?$this->config['limit']:0).';
			</script>';
			$data = $this->model->{$this->attribute};
			//die('data<pre>'.print_r($this->model, true).'</pre>');
			if(is_array($data)){
			//die('val<pre>'.print_r($val, true).'</pre>');
				$container = $this->render('_gallery', ['field' => $this->attribute, 'data' => $data]);
			}
			$result .= '<div class="container" style="width: 100%;">'.$container.'</div>';
			$result .= '<img id="but_'.$this->attribute.'" src="/backend/web/img/downloadicon.png" onclick="galleryOpenPopup(\''.$this->attribute.'\', n_'.$this->attribute.'++, \''.$this->type.'\')" style="width: 50px; height: 50px; cursor: pointer;"/>';
		$result .= '</div>';
		
        return $result;
    }
}