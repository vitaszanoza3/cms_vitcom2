<?php

namespace backend\modules\pages\widgets\fields;

use yii\base\Widget;
use yii\helpers\Html;

class JsonFieldsWidget extends Widget
{
    public $model, $attribute, $view, $config, $form;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
		//die('<pre>'.print_r($this->config, true).'</pre>');
		//echo 'model=<pre>'.print_r($this->model, true).'</pre><br/>';
		//echo 'attribute=<pre>'.print_r($this->attribute, true).'</pre><br/>';
		//echo 'view=<pre>'.print_r($this->view, true).'</pre><br/>';
		$json = $this->model->{$this->attribute};
		$result = '<script>
			$(function() {
				n_'.mb_strtolower($this->attribute).' = 0;
				limit_'.mb_strtolower($this->attribute).' = '.(isSet($this->config['limit'])?$this->config['limit']:0).';
				collection_'.mb_strtolower($this->attribute).' = initJsonField(\''.$this->attribute.'\', \''.Html::decode($json).'\');
			});
			</script>';
		$result .= $this->render('_jsonModal', ['key' => mb_strtolower($this->attribute), 'config' => $this->config]);
		$result .= $this->form->field($this->model, $this->attribute)->hiddenInput()->label(false);
		//die($result);
        return $result;
    }
}