<?php

namespace backend\modules\pages\widgets\fields;

use yii\base\Widget;
use yii\helpers\Html;

class FormWidget extends Widget
{
    public $model, $attribute, $view, $config, $form;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
		//die('<pre>'.print_r($this->config, true).'</pre>');
		//echo 'model=<pre>'.print_r($this->model, true).'</pre><br/>';
		//echo 'attribute=<pre>'.print_r($this->attribute, true).'</pre><br/>';
		//echo 'view=<pre>'.print_r($this->view, true).'</pre><br/>';
		$json = $this->model->{$this->attribute};
		$key = mb_strtolower($this->attribute);
		$result = '<script>
			$(function() {
				data_'.$key.' = initFormField(\''.$this->attribute.'\', \''.Html::decode($json).'\', \'form\');
			});
			</script>';
		$result .= '<a data-toggle="modal" data-target="#modal_'.$key.'" onClick="loadMailFormModal(\''.$key.'\', data_'.$key.')" style="margin: 3px;vertical-align: top;" class="glyphicon glyphicon-pencil"></a>';
		$result .= $this->render('_formModal', ['key' => $key, 'config' => $this->config]);
		$result .= $this->form->field($this->model, $this->attribute)->hiddenInput()->label(false);
		//die($result);
        return $result;
    }
}