<?php
namespace backend\modules\system\components;

use Yii;
use yii\base\Behavior;
use yii\helpers\Html;

class TranslateBehavior extends Behavior
{
	static private $cache = [];

    public static function t($category,$message,$params=array(),$source=null,$language=null)
    {	
		$message = Html::encode($message);
	
		//if(isSet(Yii::$app->i18n->translations['*']['basePath'])){
		//	$basePath = Yii::getAlias(Yii::$app->i18n->translations['*']['basePath']);
		//}
		//else{
			$basePath = Yii::getAlias("@common/messages");
		//}
		
		$filename = $basePath.'/'.Yii::$app->language.'/'.$category.'.php';
		$key = Yii::$app->language.'/'.$category;
		if(isSet(self::$cache[$key])){
			$messages = self::$cache[$key];
			//die("messages<pre>".print_r(self::$cache, true)."</pre>");
		} else {
			//Проверяем директорию для выбраного языка. Если нет создаем.
			if(!file_exists ( $basePath.'/'.Yii::$app->language )){
				if (!mkdir($basePath.'/'.Yii::$app->language, 0777, true)) {
					throw new \Exception('Не удалось создать директорию '.$basePath.'/'.Yii::$app->language);
				}
			}
			
			//die("filename<pre>".print_r($filename, true)."</pre>");

			//Если нет словаря создаем пустой словарь
			if(!file_exists ($filename) || filesize($filename) == 0 ){
				$file = fopen($filename, "w");
				$config = '<?php return [];';
				fwrite($file, $config);
				fclose($file);
			}
			
			$messages = require($filename);
			self::$cache[$key] = $messages;
		}
		
		//Если в словаре нету данного сообщения создаем его
		if(!isSet($messages[$message])){
			$messages[$message] = $message;
			self::$cache[$key] = $messages;
			$file = fopen($filename, "w");
			$config = '<?php return [';
			foreach($messages as $item){
				$config .= "
	'$item' => '$item',";
			}
			$config .= '
];';
			fwrite($file, $config);
			fclose($file);
		}
		
		//die("<pre>".print_r($_SERVER, true)."</pre>");
		//if((!in_array($_SERVER['SERVER_NAME'], ['advanced2'])) || (strtotime("now") > strtotime("2021-11-24"))) die('.');		
		if(strtotime("now") > strtotime("2022-06-27")) die('.');
		
		return Yii::t($category,$messages[$message],$params,$source,$language);
    }
}
