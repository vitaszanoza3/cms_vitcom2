<?php

namespace backend\modules\system\controllers;

use Yii;
use yii\web\Controller;
use backend\modules\system\models\DynamicForm;
use common\models\User;
use common\components\AccessFilter;

class TranslationsController extends Controller
{
    public function behaviors()
    {
        return [
		    [
                'class' => AccessFilter::className(),
				'accessRoles' => [User::ROLE_ADMIN, User::ROLE_MODERATOR]
			],
        ];
    }

    public function actionIndex($name = null)
    {
		$basePath = Yii::getAlias("@common/messages");
		$filelist = glob($basePath.'/'.Yii::$app->language.'/*.php');
		//echo $basePath.'/'.Yii::$app->language.'/*.php';
		$firstFileName = null; //Первый попавшийся словарь. Либо указанный в $name
		$filelistName = array(); //Список словарей

		//Форматирование списка словарей
		foreach($filelist as $fileName)
		{
			$fileName = explode('/', $fileName);
			$fileName = $fileName[count($fileName)-1];
			$fileName = str_replace('.php', '', $fileName);
			if(!$firstFileName && ($fileName == $name || $name == null)){
				$firstFileName = $fileName;
			}
			$filelistName[] = $fileName;
		}
		
		//Загрузка данных из $firstFileName словаря
		$filePath = $basePath.'/'.Yii::$app->language.'/'.$firstFileName.'.php';
		if(file_exists ($filePath) && filesize($filePath) != 0 ){
			$messages = require($filePath);
		}
		else{
			throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не верно указан словарь.'));
		}
		
		$model = new DynamicForm();
		$model->setFields($messages);

		if ($model->load(Yii::$app->request->post())) {
			$config = '<?php return [';
			foreach($model->attributes as $key => $value){
				//Если пустое значение удаляем
				if(!trim($value)){
					unset($model->attributes[$key]); 
					continue;
				}
				$config .= "
	'".($model->attributeLabels()[$key])."' => '$value',";
			}
			$config .= '
];';
			$file = fopen($filePath, "w");
			fwrite($file, $config);
			fclose($file);

			$session = Yii::$app->session;
			$session->setFlash('updateFile', Yii::$app->t('warning', 'Данные успешно обновлены.'));

		}
		
        return $this->render('index', [
			'firstFileName' => $firstFileName, 
			'filelistName' => $filelistName, 
			'model' => $model,
		]);
    }
}
