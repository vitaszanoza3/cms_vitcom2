<?php
namespace backend\modules\system\models;

use Yii;
use yii\base\Model;

/**
 * Dynamic Form
 * Заточена под систему переводов
 */
class DynamicForm extends Model
{
    public $attributes = array();
    public $pre = '';
	private $fields = array();
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             //[$this->attributes, 'required'],
			 [array_keys($this->attributes), 'string','min' => 0, 'max' => 1024],
        ];
    }

	
	public function setFields($fields){
		$this->fields = $fields;
		$i = 0;
		foreach($this->fields as $key => $value){
			$this->attributes[$this->pre.'field_'.$i] = $value;
			$i++;
		}		
	}

	public function __get($name) {
		$local_attributes = [
			'isNewRecord',
		];
		if(!in_array($name,$local_attributes) && isSet($this->attributes[$name])){
			return $this->attributes[$name];
		}
		else{
			return parent::__get($name);
		}
    }

    public function __set($name, $value) {
		$local_attributes = [
			'isNewRecord',
		];
		if(!in_array($name,$local_attributes) && isSet($this->attributes[$name])){
			$this->attributes[$name] = $value;
		}
		else{
			parent::__set($name, $value);
		}
        
    }

 	public function attributeLabels(){
		$i = 0;
		$labels = array();
		foreach($this->fields as $key => $value){
			$labels[$this->pre.'field_'.$i] = $key;
			$i++;
		}
		return $labels;
	}
}
