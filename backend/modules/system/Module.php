<?php

namespace backend\modules\system;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\system\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
	}
}
