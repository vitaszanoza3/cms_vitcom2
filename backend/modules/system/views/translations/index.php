<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

//die("<pre>".print_r($filelistName, true)."</pre>");
?>

<div class="users-default-index">
    <h1>Система переводов. Файл:<?= $firstFileName ?></h1>
	
	<?php if(Yii::$app->session->getFlash('updateFile')): ?>
		<div class="alert alert-success" role="alert">
			<strong><?= Yii::$app->session->getFlash('updateFile') ?></strong>
		</div>
	<?php endif ?>
	
	<div>
		<div class="body-column1">
			<?php 
			foreach($filelistName as $fileName):
				echo Html::a(Html::encode($fileName), Url::to(['/system/translations', 'name' => $fileName])).'<br/>';
			endforeach;
			?>
			
		</div>
		<div class="body-column2">
		
		    <?php $form = ActiveForm::begin(); ?>

			<?php $i=0; foreach($model->attributes as $key => $value): ?>
				<?= $form->field($model, $key)->textInput() ?>
			<?php $i++; endforeach; ?>

			<div class="form-group">
				<?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>
			</div>

			<?php ActiveForm::end(); ?>
		
		</div>
	</div>
</div>
