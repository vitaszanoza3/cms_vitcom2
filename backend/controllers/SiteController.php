<?php
namespace backend\controllers;

$path = str_replace('\backend\controllers', '\backend\web\extensions\ckfinder3_5\core\connector\php', __DIR__);
require_once $path . '/vendor/autoload.php';
use CKSource\CKFinder\CKFinder;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

use common\components\AccessFilter;

use common\models\LoginForm;
use yii\filters\RateLimiter;

use common\models\User;
use common\models\IpLimiter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			//[
            //    'class' => AccessFilter::className(),
			//	'accessRoles' => ['admin', 'moderator', 'editor', 'user']
			//],
			/*
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'test'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
			'rateLimiter' => [
				'class' => RateLimiter::className(),
				'user' => function() { return new IpLimiter(); },
				'except' => ['error']
           ],
        ];
    }

   /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
				'view' => '@backend/views/site/error.php'
            ],
        ];
    }

	
    public function actionIndex()
    {
		//return $this->render('index');
		return $this->redirect(['pages/default']);
    }

	public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
		return $this->goHome();
    }
	
	public function actionLanguage($language)
    {
		if(isSet(Yii::$app->params['language'][$language])){ 
			Yii::$app->language = $language;
		} else {$language = 'ru';}
	
		//Подмена ?language= на нужный язык
		$referrer = Yii::$app->request->referrer;
		if($referrer){
			$url = str_replace('?', '&', Yii::$app->request->referrer);
			$url = explode('&', $url);
			foreach($url as $item){
				$item = explode('=', $item);
				if(mb_strtolower($item[0]) == 'language'){
					$referrer = str_replace('language='.$item[1], 'language='.$language, Yii::$app->request->referrer);
					break;
				}
			}
		}
		//die("<pre>".print_r($referrer, true)."</pre>");
		return $this->redirect($referrer ?: Yii::$app->homeUrl);
        //return $this->goBack();
    }
	
    public function actionDeleteCache()
    {
	//die('actionDeleteCache');
		$errors = [];
		if(Yii::$app->user->can('moderator')){
			if (file_exists(Yii::getAlias('@cacheImages/'))) {
				foreach (glob(Yii::getAlias('@cacheImages/*')) as $file) {
					if(!unlink($file)){
						$errors[] = 'Ошибка! Файл не удален. '.$file;
					}
				}
			}
		} else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Страница не найдена.'));
		
		return $this->redirect(Yii::$app->homeUrl);
		//if(count($errors)) return Yii::$app->commonCustom->returnApi(false, [], $errors);
		//else return Yii::$app->commonCustom->returnApi(true, [], ['Кеш успешно удален.']);
   }

	/**
     * Входной скрипт для CKFinder
     */
	public function actionCkfinder(){
		$request = Yii::$app->request;
		if($request->get('command') == 'Thumbnail' && $request->get('type') == 'Images' && $request->get('size') == '150x150'){
			//Ждя создания миниатюры открытый доступ
			$authentication = true;
		}
		else{
			if(Yii::$app->getUser()->identity) {
				$roles = [User::ROLE_ADMIN,User::ROLE_MODERATOR];
				$roleEditorsModel = \common\models\RoleEditor::find()->all();
				foreach($roleEditorsModel as $model){
					$roles[] = $model->role;
				}
				if(in_array(Yii::$app->getUser()->identity->role, $roles)) $authentication = true;
				else $authentication = false;
			} else $authentication = false;
		} 
		if($authentication){
			$path = str_replace('\backend\controllers', '\backend\web\extensions\ckfinder3_5\core\connector\php', __DIR__);
			$ckfinder = new CKFinder($path . '/../../../config.php');
			$ckfinder->run();
		}
		exit;
	}
	
}
