<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@cacheImages' => '@app/../uploads/cacheImages',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/../vendor',
	'bootstrap' => ['start'],
    'components' => [
		'assetManager' => [
			'bundles' => [
				'yii\web\JqueryAsset' => [
					'js'=>[]
				],
			],
		],
		'authManager' => [
            //'class' => 'yii\rbac\PhpManager',
			'class' => 'yii\rbac\DbManager',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
		'i18n' => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@common/messages',
					'sourceLanguage' => 'ru',
				],
			],
		],
		'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'start' => [
            'class' => 'common\components\Start',
        ],
		'dataListComponent' => [
            'class' => 'backend\modules\pages\components\DataListComponent',
        ],
		'commonCustom' => [
            'class' => 'common\components\CommonCustomComponent',
        ],
		'actionForm' => [
            'class' => 'frontend\components\ActionFormComponent',
        ],
		'db' => require($_SERVER['DOCUMENT_ROOT'] . '/config/db.php'),
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
	'language' => 'ru-RU',
	'as translateBehavior' => backend\modules\system\components\TranslateBehavior::className(),
	'params' => [
		'language' => [
			'ru' => 'ru-RU',
			'en' => 'en-US',
		],
		'startLanguage' => 'ru', //Язык по умолчанию. Если не будет даных то они удут браться с этой языковой версии. И фильтры будут работать только с этой версиеей.
	],
];
