<?php
namespace common\components;

use Yii;
use yii\base\Component;

class Start extends Component
{
    public function init()
    {
        parent::init();
		
		//Установка языка
		if(Yii::$app->request->get('language') && isSet(Yii::$app->params['language'][Yii::$app->request->get('language')])){
			Yii::$app->session->set('language', Yii::$app->request->get('language'));
		}
		if (!Yii::$app->session->get('language')) {
			Yii::$app->session->set('language', 'ru');
		}
		Yii::$app->language = Yii::$app->session->get('language');

    }
	
}
