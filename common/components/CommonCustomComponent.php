<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\imagine\Image;

class CommonCustomComponent extends Component
{
    /**
     * Send a GET request using cURL
     * @param string $url to request
     * @param array $get values to send
     * @param array $options for cURL
     * @return string
     */
	 
	public $cache = [];

    public function curl_get($url, array $get = array(), array $options = array()) {
        $defaults = array(
            CURLOPT_URL => $url . (strpos($url, "?") === FALSE ? "?" : "") . http_build_query($get) ,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_DNS_USE_GLOBAL_CACHE => false,
            CURLOPT_SSL_VERIFYHOST => 0, //unsafe, but the fastest solution for the error " SSL certificate problem, verify that the CA cert is OK"
            CURLOPT_SSL_VERIFYPEER => 0, //unsafe, but the fastest solution for the error " SSL certificate problem, verify that the CA cert is OK"
         );
        $ch = curl_init();
        curl_setopt_array($ch, ($options + $defaults));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        if (!$result = curl_exec($ch)) {
            trigger_error(curl_error($ch));
        }

        curl_close($ch);
        return $result;
    }
	
	 public function curl_post($url, array $post = NULL, array $options = array())
    {
        $defaults = array(
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_URL => $url,
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_TIMEOUT => 15,
            CURLOPT_POSTFIELDS => http_build_query($post),
        );


        $ch = curl_init();
        curl_setopt_array($ch, ($options + $defaults));
        if( ! $result = curl_exec($ch))
        {
            trigger_error(curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

	public function imgSize($url, $size){
		if(!is_array($size)){
			throw new \yii\base\Exception('Переменная size должна быть массивом с 1 или 2 параметрами');
		}
		if(!in_array(count($size), [1,2])){
			throw new \yii\base\Exception('Переменная size должна быть массивом с 1 или 2 параметрами');
		}
		
		$pos = strripos($url, 'http');
		if ($pos === false) { 
			$fullUrl = Yii::getAlias('@app/..').$url; 
			//$exists = file_exists($url);
		} else {
			//$exists = self::remoteFileExists($url);
			$fullUrl = $url;
		}
		$extension = pathinfo($fullUrl)['extension'];
		$fileName = hash('md5', $url.print_r($size, true));

		if(!file_exists(Yii::getAlias('@cacheImages/').$fileName.'.'.$extension)){
		
			$origSize = @getimagesize($fullUrl);
			if($origSize){
				$origWidth = $origSize[0];
				$origHeight = $origSize[1];
				$mime = $origSize['mime'];
				
				$types = [
					'image/gif' => 'gif',
					'image/jpeg' => 'jpg',
					'image/pjpeg' => 'jpg',
					'image/png' => 'png',
				];

				//Проверка mimeType
				if(!isSet($types[$mime])) return (object)array(
					'type' => null,
					'url' => null,
				);
				
				if(count($size) == 1){
					$cof = $origWidth/$size[0];
					$height = $origHeight/$cof;
				} else {
					$height = $size[1];
				}
				
				//if(!file_exists(Yii::getAlias('@cacheImages/').$fileName.'.'.$types[$mime])){
					Image::thumbnail($fullUrl, $size[0], $height)
						->save(Yii::getAlias('@cacheImages/'.$fileName.'.'.$extension), ['quality' => 100]);
				//}
				//return '/uploads/cacheImages/'.$fileName.'.'.$types[$mime];
				return (object)array(
						'type' => $types[$mime],
						'url' => '/uploads/cacheImages/'.$fileName.'.'.$extension,
					);
			} else {
				if(mb_strtolower($extension) == 'svg'){
					return (object)array(
						'type' => 'svg',
						'url' => $url,
					);
				}
			}
		} else {
			return (object)array(
						'type' => $extension,
						'url' => '/uploads/cacheImages/'.$fileName.'.'.$extension,
					);
		}
		
		return (object)array(
			'type' => null,
			'url' => null,
		);
	}
	
	//Проверка URL. Есть ли файл по ссылке
	function remoteFileExists($url) {
		$curl = curl_init($url);

		//don't fetch the actual page, you only want to check the connection is ok
		curl_setopt($curl, CURLOPT_NOBODY, true);

		//do request
		$result = curl_exec($curl);

		$ret = false;

		//if request did not fail
		if ($result !== false) {
			//if request was ok, check response code
			$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  

			if ($statusCode == 200) {
				$ret = true;   
			}
		}

		curl_close($curl);

		return $ret;
	}

   /**
	 * Формат вывода API
     * @return json
     */
    public function returnApi($success, $data = [], $messages = []) {
	    header("Content-type: application/json; charset=utf-8");
        header('Access-Control-Allow-Origin: *');

        echo json_encode([
			'success' => ($success)?true:false,
			'messages' => $messages,
			'data' => $data,
		]);
		exit;
    }


}
