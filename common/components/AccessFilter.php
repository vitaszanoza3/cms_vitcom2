<?php

namespace common\components;

use Yii;
use yii\base\ActionFilter;

class AccessFilter extends ActionFilter
{
	public $accessRoles = [];

    public function beforeAction($action)
    {
		//die("<pre>".print_r($this->accessRoles, true)."</pre>");
		if(!Yii::$app->user->isGuest){
			foreach($this->accessRoles as $key => $value)
			{
				$role = '';
				$actions = null;
				
				if(is_array($value)){
					$role = $key;
					$actions = $value;
				}
				else{
					$role = $value;
				}
				
				if(Yii::$app->user->can($role)){
					//Если не заданны ограничения
					if(!$actions){
						return parent::beforeAction($action);
					}
					//Если действие доступно
					elseif(in_array($action->id, $actions)){
						return parent::beforeAction($action);
					}
				}
				
			}
		}
		else{
			//redirect не срабатывает в поведениях
			//return Yii::$app->response->redirect('/site/login');
		}
		
		//Если выход не произведен ранее выдаем ошибку
		throw new \yii\web\ForbiddenHttpException('У вас нет доступа к этой странице.');
		//throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'У вас нет доступа к этой странице.'));
		exit;
		//throw new \Exception('У вас нет доступа к этой странице');
		
    }

    public function afterAction($action, $result)
    {
        return parent::afterAction($action, $result);
    }
}
