<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

class Fields extends \yii\db\ActiveRecord
{
	public static $curentTemplate = 'default';
	public static $language = 'ru';
	public $myAttributes = null;

    const TYPE_INT = 'TYPE_INT';
    const TYPE_DOUBLE = 'TYPE_DOUBLE';
    const TYPE_SMALL_TEXT = 'TYPE_SMALL_TEXT';
    const TYPE_TEXT = 'TYPE_TEXT';
    const TYPE_PASSWORD = 'TYPE_PASSWORD';
    const TYPE_FULL_TEXT = 'TYPE_FULL_TEXT';
    const TYPE_CK_EDITOR = 'TYPE_CK_EDITOR';
    const TYPE_DROPDOWN_LIST = 'TYPE_DROPDOWN_LIST';
    const TYPE_CHECKBOX_LIST = 'TYPE_CHECKBOX_LIST';
    const TYPE_CHECKBOX = 'TYPE_CHECKBOX';
    const TYPE_RADIO = 'TYPE_RADIO';
    const TYPE_FILES = 'TYPE_FILES';
    const TYPE_IMAGES = 'TYPE_IMAGES';
    const TYPE_FILE = 'TYPE_FILE';
	const TYPE_DATE = 'TYPE_DATE';
	const TYPE_EMAIL = 'TYPE_EMAIL';
	const TYPE_URL = 'TYPE_URL';
	const TYPE_PHONE = 'TYPE_PHONE';
	const TYPE_JSON = 'TYPE_JSON';
	const TYPE_HIDDEN = 'TYPE_HIDDEN';
	const TYPE_FORM = 'TYPE_FORM';
	const TYPE_MAIL_FORM = 'TYPE_MAIL_FORM';


    /**
     * @inheritdoc
     */
    public static function tableName()
    { 
		$table = '{{%fields_'.self::$curentTemplate.'_'.self::$language.'}}';
		return $table;
    }
	
    /**
     * Пресеты для дополнительных полей
     */
    public static function preset($type)
    {
		$presets = [
			self::TYPE_INT => [
				'field' => 'textInput',
				'type' => 'int(10)',
			],
			self::TYPE_DOUBLE => [
				'field' => 'textInput',
				'type' => 'DOUBLE',
			],
			self::TYPE_SMALL_TEXT => [
				'field' => 'textInput',
				'type' => 'varchar(50)',
			],
			self::TYPE_TEXT => [
				'field' => 'textInput',
				'type' => 'varchar(255)',
			],
			self::TYPE_PASSWORD => [
				'field' => 'passwordInput',
				'type' => 'varchar(50)',
			],
			self::TYPE_FULL_TEXT => [
				'field' => 'textarea',
				'type' => 'text',
			],
			self::TYPE_CK_EDITOR => [
				'field' => 'ck_editor',
				'type' => 'text',
			],
			self::TYPE_DROPDOWN_LIST => [
				'field' => 'dropDownList',
				'type' => 'varchar(100)',
			],
			self::TYPE_CHECKBOX_LIST => [
				'field' => 'checkboxList',
				'type' => 'varchar(100)',
			],
			self::TYPE_CHECKBOX => [
				'field' => 'checkbox',
				'type' => 'TINYINT(1)',
			],
			self::TYPE_RADIO => [
				'field' => 'radioList',
				'type' => 'varchar(100)',
			],
			self::TYPE_FILES => [
				'field' => 'ckfinder',
				'type' => 'text',
				'types' => ['jpg','jpeg','gif','png','svf','tiff','tif','bmp','avi','csv','doc','docx','gz','gzip','mov','mp3','mp4','mpeg','pdf','rar','rtf','tgz','xls','xlsx','zip'],
				'max_size' => 20000000, //20mb
			],
			self::TYPE_IMAGES => [
				'field' => 'ckfinder',
				'type' => 'text',
				'types' => ['jpg','jpeg','gif','png','svf','tiff','tif','bmp'],
				'max_size' => 20, //20mb
			],
			self::TYPE_FILE => [
				'field' => 'fileInput',
				'type' => 'varchar(255)',
				'types' => ['jpg','jpeg','gif','png','svf','tiff','tif','bmp','avi','csv','doc','docx','gz','gzip','mov','mp3','mp4','mpeg','pdf','rar','rtf','tgz','xls','xlsx','zip'],
				'max_size' => 20, //20mb
			],
			self::TYPE_DATE => [
				'field' => 'datePicker',
				'type' => 'int(10)',
			],
			self::TYPE_EMAIL => [
				'field' => 'textInput',
				'type' => 'varchar(50)',
			],
			self::TYPE_URL => [
				'field' => 'urlInput',
				'type' => 'varchar(255)',
			],
			self::TYPE_PHONE => [
				'field' => 'phoneInput',
				'type' => 'varchar(255)',
			],
			self::TYPE_JSON => [
				'field' => 'json',
				'type' => 'text',
			],
			self::TYPE_HIDDEN => [
				'field' => 'hiddenInput',
				'type' => 'varchar(255)',
			],
			self::TYPE_FORM => [
				'field' => 'form',
				'type' => 'text',
			],
			self::TYPE_MAIL_FORM => [
				'field' => 'mailForm',
				'type' => 'text',
			],

		];
        return (isSet($presets[$type]))?$presets[$type]:['field' => null];
    }
	
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['par1', 'par2', 'par3'], 'integer'],
			//[['f_inner_title', 'f_short_text'], 'string', 'max' => 255],
		];
    }

	/**
     * Проверяем шаблон
     * @param string $template
     * @return boolean
     */
	public static function testingTemplate($template, $page_id, $tables, $config){
		//die("<pre>".print_r([$template, $page_id, $tables, $config], true)."</pre>");
		$table = 'fields_'.Html::encode($template).'_'.Yii::$app->language;
		$updateBoolean = false;
		
		//Проверка таблицы
		if(!in_array($table, $tables)){
			
			//Копируем таблицу руской версии, если она есть
			if(in_array('fields_'.Html::encode($template).'_ru', $tables)){
				self::copyTable($table, 'fields_'.Html::encode($template).'_ru', $config);
			} else {
			
				//Создание таблицы
				Yii::$app->db->createCommand("
				CREATE TABLE $table(
					 f_id INT AUTO_INCREMENT PRIMARY KEY,
					 page_id INT,
					 INDEX i_page_id(page_id),
					FOREIGN KEY (page_id) REFERENCES page (id) ON DELETE CASCADE
				 )
				ENGINE=InnoDB
				DEFAULT CHARACTER SET = utf8
				COLLATE='utf8_general_ci'
				")
					->execute();
			}
			$updateBoolean = true;
		}

		if(count($config['fields']) > 0){

			//die("fields<pre>".print_r($fields, true)."</pre>");
			$updateBoolean = self::updateFields($config, $table, $updateBoolean);
			
			//Проверка записи
			$res = Yii::$app->db->createCommand("SELECT f_id FROM $table WHERE page_id = :page_id")
				->bindValue(':page_id', $page_id)
				->queryColumn();
			if(!count($res)){
				if(Yii::$app->language != Yii::$app->params['startLanguage']){
					//Проверка записи
					$res = Yii::$app->db->createCommand("SELECT f_id FROM ".'fields_'.Html::encode($template).'_ru'." WHERE page_id = :page_id")
						->bindValue(':page_id', $page_id)
						->queryColumn();
						if(count($res)){
							//Получаем поля русской ворсии
							$items = Yii::$app->db->createCommand("show columns FROM fields_".Html::encode($template)."_ru")->queryAll();
							$columns = [];
							foreach($items as $item){
								$columns[] = $item['Field'];
							}
							$columns = implode(',',$columns);
		
							//Копируем с русской
							Yii::$app->db->createCommand("
							INSERT INTO $table ($columns)
							(SELECT *
							FROM ".'fields_'.Html::encode($template).'_ru'." as t2
							WHERE t2.page_id = :page_id
							)")->bindValue(':page_id', $page_id)
							->execute();
						} else {
							//Создаем пустую запись
							Yii::$app->db->createCommand("INSERT INTO $table (page_id) VALUES(:page_id)")
								->bindValue(':page_id', $page_id)
								->execute();
						}
				} else {
					//Создаем запись
					Yii::$app->db->createCommand("INSERT INTO $table (page_id) VALUES(:page_id)")
						->bindValue(':page_id', $page_id)
						->execute();
						//echo 'INSERT '.$page_id;
				}
				$updateBoolean = true;
			}
		}
		return $updateBoolean;
	}
	
	private function copyTable($table, $tableIn, $config){
		//Копирование таблицы. (при копировании таблицы не создаются ключи. поэтому создаем новую)
		Yii::$app->db->createCommand("
		CREATE TABLE $table(
			 f_id INT AUTO_INCREMENT PRIMARY KEY,
			 page_id INT,
			 INDEX i_page_id(page_id),
			 FOREIGN KEY (page_id) REFERENCES page(id) ON DELETE CASCADE
		 )
		ENGINE=InnoDB
		DEFAULT CHARACTER SET = utf8
		COLLATE='utf8_general_ci'
		")->execute();
		
		//Создание динамических полей
		self::updateFields($config, $table, true);
		
		Yii::$app->db->createCommand("
		INSERT INTO $table
		SELECT *
		FROM $tableIn
		")->execute();
	}
	
	private function updateFields($config, $table, $updateBoolean){
		//Проверка полей
		$fields = [];
		$items = Yii::$app->db->createCommand("show columns FROM $table")
			->queryAll();
		foreach($items as $item){
			$fields[$item['Field']] = $item['Type'];
		}
		foreach($config['fields'] as $key => $value){
			$key = Html::encode($key);
			$type = Html::encode(self::preset($value['type'])['type']);
			if(!isSet($fields['f_'.$key])){
				//Создаем поле
				Yii::$app->db->createCommand("ALTER TABLE $table ADD f_$key $type")->execute();
				$updateBoolean = true;
			}elseif($fields['f_'.$key] != $type){
				//Меняем тип
				Yii::$app->db->createCommand("ALTER TABLE $table CHANGE f_$key f_$key $type")->execute();
				$updateBoolean = true;
			}
		}
		return $updateBoolean;
	}
	
		/**
     * Проверяем шаблон
     * @param string $template
     * @return boolean
     */
	public static function mimeTyps($type){
		$mimeTyps = [
			'json' => ['application/json'],
			'pdf' => ['application/pdf'],
			'zip' => ['application/zip'],
			'gzip' => ['application/gzip'],
			'rar' => ['application/x-rar-compressed'],
			'xml' => ['application/xml'],
			'mp4' => ['audio/mp4','video/mp4'],
			'mp3' => ['audio/mpeg'],
			'mpeg' => ['audio/mpeg','video/mpeg'],
			'gif' => ['image/gif'],
			'jpeg' => ['image/jpeg', 'image/pjpeg'],
			'jpg' => ['image/jpeg', 'image/pjpeg'],
			'png' => ['image/png'],
			'svg' => ['image/svg+xml', 'text/xml'],
			'tiff' => ['image/tiff'],
			'css' => ['text/css'],
			'txt' => ['text/plain'],
			'csv' => ['text/csv'],
			'doc' => ['application/msword'],
			'docx' => ['application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
			'xls' => ['application/vnd.ms-excel'],
			'xlsx' => ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
			'xlsm' => ['application/vnd.ms-excel.sheet.macroEnabled.12'],
			'xml' => ['text/xml'],
			'odf' => ['application/vnd.oasis.opendocument.text','application/vnd.oasis.opendocument.spreadsheet','application/vnd.oasis.opendocument.presentation','application/vnd.oasis.opendocument.graphics'],
		];

		if(isSet($mimeTyps[$type])){
			return $mimeTyps[$type];
		} else return null;

	}
}
