<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * This is the model class for table "role_editor".
 *
 * @property string $id
 * @property string $role
 * @property integer $created_at
 * @property integer $updated_at
 */
class RoleEditor extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role_editor';
    }

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           ['role', 'required'],
           ['role', 'string', 'max' => 255],
           ['role', 'unique', 'targetClass' => RoleEditor::ClassName(), 'message' => 'Эта роль уже существует.'],
		   /*
		   ['role', 'notIn', 'range' =>[
                User::ROLE_ADMIN,
                User::ROLE_MODERATOR,
				User::ROLE_EDITOR,
                User::ROLE_USER,
                User::ROLE_BANED,
            ]],
			*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role' => 'Role',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
	
	public function search($params)
    {
        $query = RoleEditor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
	
	/**
     * Вывод всех редакторов в массиве
     */
    static public function editors(){
		$editors = [];
		foreach(self::find()->all() as $model){
			$editors[$model->role] = $model->role;
		}
		return $editors;
	}
}
