<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\Html;
use common\models\RoleEditor;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property string $role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_NOT_ACTIVE = 1;
    const STATUS_ACTIVE = 10;
	
	const ROLE_ADMIN = 'admin';
	const ROLE_MODERATOR = 'moderator';
	const ROLE_EDITOR = 'editor';
	const ROLE_USER = 'user';
	const ROLE_BANED = 'baned';

	public $password, $passwordConfirm;
	
	public function __get($name) {
	
		return parent::__get($name);
	}
	
	public function __set($name, $value) {
		
		return parent::__set($name, strip_tags($value));
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
			
        ];
    }

    /**
     * @inheritdoc
     */
	public function scenarios()
    {
        $scenarios = parent::scenarios();
        return $scenarios;
    }
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'],'required', 'message' => Yii::$app->t('error','Обязательное поле')],
            [['password'],'required', 'message' => Yii::$app->t('error','Обязательное поле'), 'except' => ['system_registration']],
 			
            [['username','password'],'filter', 'filter' => 'trim'],
            [['username', 'name', 'phone'],'string','min' => 2, 'max' => 255, 'message' => Yii::$app->t('error','Минимум 2 символа. Максимум 255.')],
			['username', 'match', 'pattern' => '#^[\w_-]+$#i', 'on' => ['registration', 'system_registration']],
            ['username', 'unique', 'targetClass' => User::ClassName(), 'on' => ['registration', 'system_registration'], 'message' => Yii::$app->t('error','Это имя уже занято.')],
			
            ['password', 'string', 'min' => 6, 'max' => 255, 'message' => Yii::$app->t('error','Минимум 6 символов. Максимум 255.')],
			[['passwordConfirm'], 'compare', 'compareAttribute' => 'password', 'on' => ['registration', 'system_registration'], 'message' => Yii::$app->t('error','Не совпадает пароль')], 
			
            ['email','email', 'on' => ['registration', 'system_registration'], 'message' => Yii::$app->t('error','Не правельный электронный адрес.')],
            ['email', 'unique', 'targetClass' => User::ClassName(), 'on' => ['registration', 'system_registration'], 'message' => Yii::$app->t('error','Эта почта уже занята.')],
			
            ['status', 'default', 'value' => User::STATUS_ACTIVE, 'on' => ['registration', 'system_registration']],
            ['status', 'in', 'range' =>[
                User::STATUS_DELETED,
                User::STATUS_NOT_ACTIVE,
                User::STATUS_ACTIVE,
            ], 'on' => ['registration', 'system_registration']
			, 'message' => Yii::$app->t('error','Не верный статус.')],
			
            [['role','email'],'required', 'on' => ['registration', 'system_registration'], 'message' => Yii::$app->t('error','Обязательное поле')],
			['role', 'default', 'value' => User::ROLE_USER, 'on' => ['registration', 'system_registration']],
            ['role', 'in', 'range' => self::getAllRoles()
				 , 'on' => ['registration', 'system_registration']
				 , 'message' => Yii::$app->t('error','Не верно указана роль.')
			 ],

			[['allowance','allowance_updated_at'], 'integer'],
       ];
    }
	
	 public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);
		if($insert){
			$auth = Yii::$app->authManager;
			if($role = $auth->getRole($this->role)){
				if(!$auth->getAssignment($this->role, $this->getId())){
					$auth->revokeAll($this->getId());
					$auth->assign($role, $this->getId());
				}
			}
		}
	}

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
			'id' => $id, 
			'status' => [self::STATUS_NOT_ACTIVE,self::STATUS_ACTIVE]]
		);
    }


    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        //return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
 		return static::findOne([
			'username' => $username,
			'status' => [self::STATUS_NOT_ACTIVE,self::STATUS_ACTIVE],
		]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }


	
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
	
	/**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

	
    /**
     * Возвращает список ролей
     */
    public function getAllRoles()
    {
		$auth = Yii::$app->authManager;

		if(Yii::$app->user->can(User::ROLE_ADMIN)){
			$roles = [
				User::ROLE_ADMIN => User::ROLE_ADMIN,
				User::ROLE_MODERATOR => User::ROLE_MODERATOR,
				//User::ROLE_EDITOR => User::ROLE_EDITOR,
				User::ROLE_USER => User::ROLE_USER,
				User::ROLE_BANED => User::ROLE_BANED,
			];
			//Добавление редакторов
			$models = RoleEditor::find()->all();
			foreach($models as $model)
				$roles[$model->role] = $model->role;
		}
		elseif(Yii::$app->user->can(User::ROLE_MODERATOR)){
			$roles = [
				User::ROLE_MODERATOR => User::ROLE_MODERATOR,
				//User::ROLE_EDITOR => User::ROLE_EDITOR,
				User::ROLE_USER => User::ROLE_USER,
				User::ROLE_BANED => User::ROLE_BANED,
			];
			//Добавление редакторов
			$models = RoleEditor::find()->all();
			foreach($models as $model)
				$roles[$model->role] = $model->role;
		}
		else{
			$roles = [
				User::ROLE_USER => User::ROLE_USER,
				User::ROLE_BANED => User::ROLE_BANED,
			];
			$roles[$this->role] = $this->role;
		}
		
        return $roles;
    }
				

	
	
}
