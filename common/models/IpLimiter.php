<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * IpLimiter model
 * Ограничение коли-ва запросов
 * не более 20 запросов за 20 секунд
 */
class IpLimiter extends ActiveRecord implements \yii\filters\RateLimitInterface
{
	public $separateRates = false;
	public $rateLimit = 60;			
	public $model;

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%iplimiter}}';
    }
	
	 /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip'],'required', 'message' => Yii::$app->t('error','Обязательное поле')],
            [['ip'],'string'],
			[['allowance','allowance_updated_at'], 'integer'],
       ];
    }
	
    public function getRateLimit($request, $action) {
		$ip = \Yii::$app->getRequest()->getUserIP();
		$this->model = IpLimiter::find()
			->where('ip = :ip', [':ip' => $ip])
			->one();
		if(!$this->model){
			$this->model = $this;
			$this->model->ip = $ip;
		}

		return [$this->rateLimit,120];
    }

    public function loadAllowance($request, $action)
    {
		$allowance = $this->model->allowance;
		$allowance_updated_at = $this->model->allowance_updated_at;
		return [($allowance)?$allowance:0, ($allowance_updated_at)?$allowance_updated_at:0];
    }

    public function saveAllowance($request, $action, $allowance, $timestamp)
    {
		$this->model->allowance =  $allowance;
 		$this->model->allowance_updated_at = $timestamp;
		$this->model->save();
 		//die($timestamp."<pre>".print_r($this, true)."</pre>");
 
    }
	
}
