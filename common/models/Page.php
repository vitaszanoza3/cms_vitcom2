<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\RoleEditor;
use backend\modules\pages\components\UrlTransliterate;
use yii\db\Expression;
use yii\helpers\Html;
use common\components\AnObj;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;

use frontend\models\DynamicForm;
use common\models\Fields;

/**
 * This is the model class for table "page".
 *
 * @property string $id
 * @property string $parent_id
 * @property string $title
 * @property string $alias
 * @property string $position
 * @property string $path
 * @property string $visible
 * @property string $type
 * @property string $template
 * @property string $fieldsGroup
 * @property string $controller
 * @property string $editors
 * @property string $in_search
 * @property integer $created_at
 * @property integer $updated_at
 */
class Page extends \yii\db\ActiveRecord
{
    const TYPE_GROUP = 'group';
    const TYPE_PAGE = 'page';
    const TYPE_ITEM = 'item';
 	
	const VISIBLE_PUBLISHED = 0;
	const VISIBLE_DRAFT = 1;
	const VISIBLE_HIDDEN = 2;
	const VISIBLE_DATA = 3;
	const VISIBLE_ALL = 10;

	public $childrenCount = 0; //Дополнительный параметр. Указывает есть ли у родителя потомки. Используется в админке
	public $dynamicData = []; //Для добавления динамических данных 
	
	private $parentTemplate;
	private $parentController;
	private $parentEditors;
	private $parentVisible;
	
	private $old_parent_id;
	private $old_position;

	private $level;
	private $typeList = [];
	
	//гетеры
	public function getParentTemplate(){ return $this->parentTemplate;}
	public function getParentController(){ return $this->parentController;}
	public function getParentEditors(){ return $this->parentEditors;}
	public function getParentVisible(){ return $this->parentVisible;}
	public function getOld_parent_id(){ return $this->old_parent_id;}
	public function getOld_position(){ return $this->old_position;}
	public function getLevel(){ return $this->level;}
	
	private	$local_attributes = [
			'id',
			'page_id',
			'itemParentsID',
			'on_itemParentsID',
			'lang',
			'parent_id',
			'path',
			'title',
			'alias',
			'position',
			'visible',
			'type',
			'editors',
			'in_search',
			'created_at',
			'updated_at',
			'date_display',
			'date_display_temp',
			//'childrenCount',
			'parentTemplate',
			'parentController',
			'parentEditors',
			'parentVisible',
			'fields',
			'fields_ru',
			'parent',
			'children',
			'isNewRecord',
			'visibleList',
			'typeList',
			'templateList',
			'controllerList',
			'jsonTitle',
			'jsonVisible',
			'jsonTemplate',
			'jsonController',
			'level',
			'hashConfig',
			'hashUpdate',
			'attributes',
			'oldAttributes',
			'multyLink',
		];
		
		private $templateConfig = [];

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

	public function getJsonVal($name){
		if($name == 'title') $json = json_decode($this->jsonTitle);
		elseif($name == 'visible') $json = json_decode($this->jsonVisible);
		elseif($name == 'template') $json = json_decode($this->jsonTemplate);
		elseif($name == 'controller') $json = json_decode($this->jsonController);
		elseif($name == 'hashUpdate'){ $json = json_decode($this->jsonHashUpdate); }
		
		if($json && isSet($json->{Yii::$app->language})){ return $json->{Yii::$app->language};}
		elseif($json && isSet($json->ru)) { return $json->ru;}
		else return null;
	}
	
	public function setJsonVal($name,$value){
		if($name == 'title') $json = json_decode($this->jsonTitle, true);
		elseif($name == 'visible') $json = json_decode($this->jsonVisible, true);
		elseif($name == 'template') $json = json_decode($this->jsonTemplate, true);
		elseif($name == 'controller') $json = json_decode($this->jsonController, true);
		elseif($name == 'hashUpdate'){ $json = json_decode($this->jsonHashUpdate, true); }

		$json[Yii::$app->language] = strip_tags($value);
		$this->{'json'.ucwords($name)} = json_encode($json);
		if (Yii::$app->language == 'ru' && $name == 'title') parent::__set('title', $value);
		//return null;
	}
	
	public function __get($name) {
		if(in_array($name, ['title','visible','template','controller', 'hashUpdate'])){return $this->getJsonVal($name);}
		elseif(!in_array($name,$this->local_attributes) && isSet($this->fields) && $this->fields && (
			//array_key_exists('f_'.$name, (array)$this->fields->attributes)
			//|| array_key_exists($name, (array)$this->fields->attributes)
			isSet($this->fields->{'f_'.$name})
			|| isSet($this->fields->{$name})
		)){
			return self::_fAttrGet($name, 'fields');
		}
		elseif(Yii::$app->language != Yii::$app->params['startLanguage'] && !in_array($name,$this->local_attributes) && $this->fields_ru && (
			array_key_exists('f_'.$name, (array)$this->fields_ru->myAttributes) //Почему то при махинациях с языком не верно срабатывает $this->fields_ru->attributes
			|| array_key_exists($name, (array)$this->fields_ru->myAttributes)
		)){
			return self::_fAttrGet($name, 'fields_ru');
		}
		elseif(strpos($name, 'f_') !== false){ return null; }
		elseif(!in_array($name,$this->local_attributes) && $this->fields && array_key_exists('f_'.$name, (array)$this->fields->attributes)) {return null;} //Если динамическое поле со значением null
		elseif(isSet($this->dynamicData[$name])) {return $this->dynamicData[$name];} //Динамические атрибуты
		else {
			//if($name == 'fields_ru') die("<pre>".print_r($this, true)."</pre>");

			return parent::__get($name);
			}
    }
	
	function _fAttrGet($name, $fields){
		//die("<pre>".print_r($this->{$fields}, true)."</pre>");
		$name = str_replace("f_", '', $name);
		$type = $this->templateConfig[$this->parentTemplate]['fields'][$name]['type'];
		if(in_array($type, [Fields::TYPE_IMAGES, Fields::TYPE_FILES])){
			$items = [];
			if(isSet($this->{$fields}->{'f_'.$name})){
				$items = json_decode(\yii\helpers\Html::decode($this->{$fields}->{'f_'.$name}));
				if(!$items) $items = [];
				if($items){
					foreach($items as $index => $values){
						$obj=new AnObj();
						foreach($values as $key => $value){
							$obj->{$key} = $value;
						}
						$obj->imgSize = function($size){return Yii::$app->commonCustom->imgSize($this->url, $size);};
						$items[$index] = $obj;
					}
				}
			}
			return $items;
		} elseif($type == Fields::TYPE_CHECKBOX_LIST){
			if($this->{$fields}->{'f_'.$name}){
				return explode('|', Html::decode($this->{$fields}->{'f_'.$name}));
			} else return null;
		} elseif($type == Fields::TYPE_URL){
			if($this->{$fields}->{'f_'.$name}){
				return explode('|', Html::decode($this->{$fields}->{'f_'.$name}));
			} else return null;
		}else {
			if(isSet($this->{$fields}->{'f_'.$name}) && is_array($this->{$fields}->{'f_'.$name})) return $this->{$fields}->{'f_'.$name};
			else {
				return isSet($this->{$fields}->{'f_'.$name})?Html::decode($this->{$fields}->{'f_'.$name}):'';
			}
		}
	}
	
	public function __set($name, $value) {
		if(in_array($name, ['title','visible','template','controller', 'hashUpdate'])){ $this->setJsonVal($name, $value);}
		elseif(!in_array($name,$this->local_attributes)&& $this->fields && (array_key_exists($name, (array)$this->fields->attributes))){
			$type = $this->templateConfig[$this->parentTemplate]['fields'][str_replace("f_", '', $name)]['type'];
			if(is_array($value)){ 
				$items = $value;
				foreach($items as $key => $val){
					$items[$key] = strip_tags($val);
				}
				$this->fields->$name =  implode("|",$items); 
			}
			else {
				if(in_array($type, [Fields::TYPE_CK_EDITOR, Fields::TYPE_JSON])){ $this->fields->$name = \yii\helpers\HtmlPurifier::process($value); }
				else $this->fields->$name = strip_tags($value);
			}
		}
		elseif(strpos($name, 'f_') !== false){ return null; }
		else{
			if(is_array($value)){ 
				$items = $value;
				foreach($items as $key => $val){
					$items[$key] = strip_tags($val);
				}
				return parent::__set($name, implode("|",$items));
			} else {
				return parent::__set($name, strip_tags($value));
			}
		}
    }
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

	public function init()
    {
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['parent_id', 'created_at', 'updated_at'], 'integer'],
			['date_display', 'default', 'value' => strtotime("now")],
			['position', 'default', 'value' => 0],
			 ['position', 'double'],
			['visible', 'default', 'value' => Page::VISIBLE_DRAFT],
			['visible','in','range'=>[Page::VISIBLE_PUBLISHED, Page::VISIBLE_DRAFT, Page::VISIBLE_HIDDEN, Page::VISIBLE_DATA]],
			//['alias', 'unique'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'required'],
            [['path'], 'string', 'max' => 30],
			['date_display', 'validationDate_display'],
        ];
		
		//Запрет на изменение корневого алиаса
		if($this->alias != 'root'){
			$rules[] = ['alias', 'unique'];
			$rules[] = [['alias'], 'string', 'max' => 255];
		}
		
		//Разрешение на правку только модераторам и администраторам
		if(Yii::$app->user->can('moderator')){
			$new_rules = [
				['template', 'default', 'value' => NULL],
				['controller', 'default', 'value' => NULL],
				['editors', 'default', 'value' => NULL],
				['type', 'default', 'value' => 'page'],
				['in_search', 'default', 'value' => ''],
				['type','in','range'=>['page','group','item']],
				['in_search','in','range'=>['1','']],
				[['template', 'controller'], 'string', 'max' => 50],
				['editors', 'validationEditors'],
				['on_itemParentsID','in','range'=>['1','','0']],
				
			];
			$rules = array_merge ($rules, $new_rules);
		}
		
		//Добавляем правила для дополнительных полей
		if($this->parentTemplate && $this->isNewRecord == false){
			$config = $this->templateConfig();
			$new_rules = [];
			$keysLevel = array_keys($config['levels']);
			$level = (isSet($keysLevel[$this->level]))?$keysLevel[$this->level]:null;
			foreach($config['fields'] as $fieldName => $value){
				if(!is_null($level) && isSet($config['fields'][$fieldName]['levels'][$level]) && in_array($this->type, $config['fields'][$fieldName]['levels'][$level])){
					switch($value['type']){
						case 'TYPE_INT': $new_rules['integer'][] = 'f_'.$fieldName; break;
						//case 'TYPE_FILE': $new_rules['file'][] = 'f_'.$fieldName; break; //Нужно доделать!
						case 'TYPE_FILES': $new_rules['gallery'][] = 'f_'.$fieldName; break; 
						case 'TYPE_IMAGES': $new_rules['gallery'][] = 'f_'.$fieldName; break; 
						case 'TYPE_DATE': $new_rules['validationDate_display'][] = 'f_'.$fieldName; break;
						case 'TYPE_DROPDOWN_LIST': $new_rules['in'][] = 'f_'.$fieldName; break;
						case 'TYPE_RADIO': $new_rules['in'][] = 'f_'.$fieldName; break;
						case 'TYPE_CHECKBOX_LIST': $new_rules['safe'][] = 'f_'.$fieldName; break;
						case 'TYPE_EMAIL': $new_rules['email'][] = 'f_'.$fieldName; break;
						case 'TYPE_URL': $new_rules['safe'][] = 'f_'.$fieldName; break;
						case 'TYPE_CAPTCHA': $new_rules['captcha'][] = 'f_'.$fieldName; break;
						default: $new_rules['string'][] = 'f_'.$fieldName; break;
					}
					if(isSet($config['fields'][$fieldName]['required']) && $config['fields'][$fieldName]['required']){
						$new_rules['required'][] = 'f_'.$fieldName;
					}
				}
			}
			//die("new_rules<pre>".print_r($new_rules, true)."</pre>");
			foreach($new_rules as $preset => $fields){
				if($preset == 'in'){
					foreach($fields as $fieldName){
						$field = str_replace("f_", '', $fieldName);
						$rules[] = [$fieldName, $preset, 'range'=> array_keys(Yii::$app->dataListComponent->{$config['fields'][$field]['function']}($config['fields'][$field]['params']))];
					}
				}elseif($preset == 'url'){
					$rules[] = [$fields, $preset, 'defaultScheme' => 'http'];
				}
				elseif($preset == 'file'){
					foreach($fields as $fieldName){
						$field = str_replace("f_", '', $fieldName);
						$rules[] = [$fieldName, $preset, 'types'=>$config['fields'][$field]['types']];
					}
				}
				elseif($preset == 'gallery'){
					//Поля галлереи заполняются в контроллере update
				}				
				elseif($preset == 'required'){
					$rules[] = [$fields, 'required'];
				}else{
					$rules[] = [$fields, $preset];
				}
			}
		}

		//die("rules<pre>".print_r($rules, true)."</pre>");
		return $rules;
    }

	/**
     * Валидатор для сохранения дат
     */
	public function validationDate_display($attribute, $params) 
	{
		//Если не число
		if(!preg_match("|^[\d]+$|", $this->$attribute)){
	
			$date =  explode("/", $this->$attribute);
			//Если содержит 3 элемента то текст. Иначе число
			if(count($date)==3){
				$this->$attribute = strtotime($date[2].'-'.$date[1].'-'.$date[0]);
			}
			else{
				$this->$attribute = strtotime("now");
			}
		}
		else{
			$this->$attribute = strtotime("now");
		}
	}
	
	/**
     * Валидатор для редакторов. Сверяет со списком RoleEditor::editors()
     */
	public function validationEditors($attribute, $params) 
	{
		if(is_array($this->$attribute)){
			$editors = RoleEditor::editors();
			//die("<pre>".print_r($this->$attribute, true)."</pre>");
			
			foreach ($this->$attribute as $attr){
				if(!isSet($editors[$attr])){
					$this->addError($attribute, Yii::$app->t('error', 'Не верно переданы параметры!')); 
					return;
				}
			}
		}
	}

	/**
     * Валидатор массивов
     */
	public function arrayVal($attribute, $params) {
		if (!is_array($this->$attribute)) {
			$this->addError($attribute, 'Значение должно быть массивом!');
		}
		return;
	}

	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'title' => 'Заголовок',
            'alias' => 'Идентификатор',
            'itemParentsID' => 'Мульти связка',
			'on_itemParentsID' => 'Выводить мульти связку',
            'position' => 'Позиция',
            'visible' => 'Видимость',
            'type' => 'Тип страницы',
            'template' => 'Основной шаблон',
            'fieldsGroup' => 'Дочерние шаблоны',
            'controller' => 'Контроллер',
            'editors' => 'Доступ редакторам',
            'in_search' => 'Участвует в поиске',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'date_display' => 'Дата',
        ];
		
		//Конфигурация шаблона
		if($this->parentTemplate){
			$config = $this->templateConfig();
			foreach($config['fields'] as $key => $value){
				$attributeLabels['f_'.$key] = $value['label'];
			}
		}

		return $attributeLabels;
    }
	
	public function getLabel($name){
		return Yii::$app->t('fields_'.$this->parentTemplate, $this->attributeLabels()['f_'.$name]);
	}

	/**
     * Scopes visible 
	 * Добавляем условие поиска опубликованные и данные
	 */
 	public static function find()
	{
		return new \backend\modules\pages\models\PageQuery(get_called_class());
	}

	/**
     * Поиск страницы по ID
	 */
	public static function findOne($id)
    {
		//Указываем настройки страницы
		//Yii::$app->treePages->settingsPage(null, $id);
		return parent::findOne($id);
    }
	 
	/**
     * Поиск страницы по алиасу
    */
	public static function findPage($alias)
    {
		$model = parent::find()
			->where('page.alias = :alias', [':alias' => $alias])
			->one();
		//die("findPage=<pre>".print_r($model, true)."</pre>");
		
		if ($model !== null) {
            return $model;
        } else {
			throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не верно указана страница.').' alias='.$alias);
            //throw new NotFoundHttpException(Yii::$app->t('error', 'Не верно указана страница.').' alias='.$alias);
        }
    }

	/**
     * Поиск родителя
     */
	public function getParent()
    {
		return $this->hasOne(Page::className(), ['id' => 'parent_id']);
    }

	
	/**
     * Поиск первого опубликованного потомка
     */
	public function getChildrenFirst()
    {
		return $this->hasMany(Page::className(), ['parent_id' => 'id'])->orderBy('position ASC')->visiblePublished()->limit(1)->one();
    }
	
	/**
     * Поиск первого опубликованного потомка
     */
	public function getChildrenFirstPage()
    {
		return $this->hasMany(Page::className(), ['parent_id' => 'id'])->orderBy('position ASC')->typePage()->limit(1)->one();
    }

	/**
     * Поиск первого опубликованного потомка
     */
	public function getChildrenFirstPagePublished()
    {
		return $this->hasMany(Page::className(), ['parent_id' => 'id'])->orderBy('position ASC')->typePage()->visiblePublished()->limit(1)->one();
    }
	
	/**
     * Поиск всех потомков. Включая скрытые
     */
	public function getChildrenAllPage()
    {
		return $this->hasMany(Page::className(), ['parent_id' => 'id'])->orderBy('position ASC')->typePage();
    }
	
	/**
     * Поиск опубликованных потомков
     */
	public function getChildrenPublishedPage()
    {
		return $this->hasMany(Page::className(), ['parent_id' => 'id'])->orderBy('position ASC')->typePage()->visiblePublished();
    }
	
	/**
     * Поиск первого опубликованного потомка
     */
	public function getChildrenFirstItem()
    {
		return $this->hasMany(Page::className(), ['parent_id' => 'id'])->orderBy('position ASC')->typeItem()->limit(1)->one();
    }
	
	/**
     * Поиск всех потомков. Включая скрытые
     */
	public function getChildrenAllItem()
    {
		return $this->hasMany(Page::className(), ['parent_id' => 'id'])->orderBy('position ASC')->typeItem();
    }
	
	/**
     * Поиск опубликованных потомков
     */
	public function getChildrenPublishedItem()
    {
		return $this->hasMany(Page::className(), ['parent_id' => 'id'])->orderBy('position ASC')->typeItem()->visiblePublished();
    }

	/**
     * Поиск потомков с данными
     */
	public function getChildrenData()
    {
		return $this->hasMany(Page::className(), ['parent_id' => 'id'])->orderBy('position ASC')->visibleData();
    }

    /**
     * Связка с дополнительными полями. Перед использованием необходимо указать родительякий шаблон
     */
	public function getFields()
    {
		if($this->parentTemplate){
			//$this->testingTemplate(); //нельзя ставить. так как testingTemplate вызывает save а save вызывает fields
			Fields::$curentTemplate = $this->parentTemplate; //Передача шаблона. Для указания таблицы
			Fields::$language = Yii::$app->language; //Передача языка. Для указания таблицы
			$fields = $this->hasOne(Fields::className(), ['page_id' => 'id']);
			if($this->id) return $fields;
			else return null;
		} 
		return null;
    }
	
	/**
     * Связка с дополнительными полями. Перед использованием необходимо указать родительякий шаблон
	 * частенько в других языках нет нужных полей. поэтому нужно брать из русской. 
	 * используется в get
     */
	public function getFields_ru()
    {
		if($this->parentTemplate){
			$language = Yii::$app->params['startLanguage'];
			//$this->testingTemplate();
			Fields::$curentTemplate = $this->parentTemplate; //Передача шаблона. Для указания таблицы
			Fields::$language = $language; //Передача языка. Для указания таблицы
			$fields = $this->hasOne(Fields::className(), ['page_id' => 'id']);
			$fields = $fields->one();
			$fields->myAttributes = $fields->attributes; //Из за махинаций с языком не работают attributes
			Fields::$language = Yii::$app->language; //Обязательно вернуть верный язык

			if($this->id) return $fields;
			else return null;
		}
		return null;
    }

	/**
     * Герерируем алиас до первой проверки
     */
	public function beforeValidate()
	{
		if(!$this->alias){
			$this->alias = UrlTransliterate::convertToChpu($this->title);	
		}
		 return parent::beforeValidate();
	}
 
	/**
     * Дополняем модель настройками
     */
	public function afterFind(){
		parent::afterFind();
		
		/*
		* собираться все настройки страницы. и сохраняться в Page
		*/
		$this->updateSettings();
	}
	
	public function beforeSave($insert){
		if(!$this->isNewRecord && isSet($this->fields) && $this->fields && isSet($this->fields->attributes)){
			foreach($this->fields->attributes as $key => $val)
			{
				$this->fields->{$key} = \yii\helpers\Html::encode($val);
			}
		}
		//die("Page<pre>".print_r($this->fields->attributes, true)."</pre>");
	
		return  parent::beforeSave($insert);
	}
	
	public function afterSave($insert, $changedAttributes){
		parent::afterSave($insert, $changedAttributes);
 		/*
		* собираться все настройки страницы. и сохраняться в Page
		*/
		if(
			$this->attributes['jsonTemplate'] != (isSet($this->oldAttributes['jsonTemplate'])?$this->oldAttributes['jsonTemplate']:null)
			|| $this->attributes['jsonController'] != (isSet($this->oldAttributes['jsonController'])?$this->oldAttributes['jsonController']:null)
			|| $this->attributes['editors'] != (isSet($this->oldAttributes['editors'])?$this->oldAttributes['editors']:null)
			|| $this->attributes['parent_id'] != (isSet($this->oldAttributes['parent_id'])?$this->oldAttributes['parent_id']:null)
			|| $this->attributes['path'] != (isSet($this->oldAttributes['path'])?$this->oldAttributes['path']:null)
			|| !$this->parentTemplate
		) {
			$this->updateSettings();
		}
	}
	
	public function updateSettings(){
		/*
		* собираться все настройки страницы. и сохраняться в Page
		*/

		$templateLevel = null;
		
		if(!$this->parentTemplate && $this->template){ 
			$this->parentTemplate = $this->template;
			$this->level = 0;
		}
		if(!$this->parentController && $this->controller) $this->parentController = $this->controller;
		if(!$this->parentEditors && $this->editors) $this->parentEditors = $this->editors;
		if(!$this->parentVisible && in_array($this->visible, [Page::VISIBLE_HIDDEN, Page::VISIBLE_DATA])) $this->parentVisible = $this->visible;
				
		if($this->parentTemplate && $this->parentController && $this->parentEditors){}
		else{
			if($this->type != Page::TYPE_ITEM){
				if(!$this->path || ($this->path == '|' && $this->alias != 'root')){
					$model = self::easyFindModel($this->parent_id);
					$this->path = $model->path.$model->id.'|';
					$this->save(false);
					$IDs = explode('|', $this->path);
				} else $IDs = explode('|', $this->path);
			} else {
				$model = self::easyFindModel($this->parent_id);
				$IDs = explode('|', $model->path);
				$IDs[] = $model->id;
			}
			foreach($IDs as $index => $id) {
					if(!$id) unset($IDs[$index]);
				}
			//die($this->path." implode<pre>".print_r(implode('|', $IDs), true)."</pre>");
			
			$keyCache = md5(implode('|', $IDs));
			if(isSet(Yii::$app->commonCustom->cache['cachePathPages'][$keyCache])){
				$parent_pages = Yii::$app->commonCustom->cache['cachePathPages'][$keyCache];
			}
			else {
				$parent_pages = (new \yii\db\Query())
					->select(['id', 
						'parent_id', 
						'title', 
						'path', 
						//'visible',
						"JSON_EXTRACT(`jsonVisible` , '$.ru') as visible_ru", 
						"JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."') as visible", 					
						//'template', 
						"JSON_EXTRACT(`jsonTemplate` , '$.ru') as template_ru", 
						"JSON_EXTRACT(`jsonTemplate` , '$.".Yii::$app->language."') as template", 
						//'controller', 
						"JSON_EXTRACT(`jsonController` , '$.ru') as controller_ru", 
						"JSON_EXTRACT(`jsonController` , '$.".Yii::$app->language."') as controller", 
						'editors'])
					->from('page')
					->where([
						'id' => $IDs,
					])
				->orderBy(array( "((LENGTH(path) - LENGTH(REPLACE(path, '|', ''))) / LENGTH('|'))" => SORT_DESC))
				->all();
				Yii::$app->commonCustom->cache['cachePathPages'][$keyCache] = $parent_pages;
				
				//Кешируем родительский вариант
				$IDs2 = [];
				$parent_pages2 = $parent_pages;

				if(count($parent_pages2) > 1){
					$parent_pages3 = [];
					foreach($parent_pages2 as $index => $val){
						if($index == 0){
							$IDs2 = explode('|', $val['path']);
							foreach($IDs2 as $i => $id) {
								if(!$id) unset($IDs2[$i]);
							}
						}
						if($index > 0){ 
							$parent_pages3[] = $val; 
						}
					}
					$keyCache2 = md5(implode('|', $IDs2));
					Yii::$app->commonCustom->cache['cachePathPages'][$keyCache2] = $parent_pages3;
					//echo "<p>".$this->title." path2<pre>".print_r([implode('|', $IDs2), $keyCache2, $parent_pages3], true)."</pre>";	

				}
			}

			$nLevel = 1;
			foreach($parent_pages as $parent_page){
				self::getJsonValModel($parent_page, 'template');
				self::getJsonValModel($parent_page, 'controller');
				self::getJsonValModel($parent_page, 'visible');
				
				$parent_page = (object)$parent_page;
				if($this->parentTemplate && $this->parentController && $this->parentEditors) break;
				if(!$this->parentTemplate && $parent_page->template){ 
					$this->parentTemplate = $parent_page->template;
					if($this->type == Page::TYPE_ITEM) $this->level = $nLevel - 1;
					else $this->level = $nLevel;
				}
				//if(!$this->parentFieldsGroup && $parent_page->fieldsGroup) $this->parentFieldsGroup = $parent_page->fieldsGroup;
				if(!$this->parentController && $parent_page->controller) $this->parentController = $parent_page->controller;
				if(!$this->parentEditors && $parent_page->editors) $this->parentEditors = $parent_page->editors;
				if(!$this->parentVisible && in_array($parent_page->visible, [Page::VISIBLE_HIDDEN, Page::VISIBLE_DATA])) $this->parentVisible = $parent_page->visible;
				$nLevel++;
			}
			
		}
		
		$this->old_parent_id = $this->parent_id;
		$this->old_position = $this->position;
		
		//Указываем тип страницы согластно конфигу шаблона. 
		//if(!$this->parentTemplate) die("path<pre>".print_r($this, true)."</pre>");	
		$config = $this->templateConfig();
		//die("config<pre>".print_r($config, true)."</pre>");
		try {
			$keysLevel = array_keys($config['levels']);
			$keyLevel = (isSet($keysLevel[$this->level]))?$keysLevel[$this->level]:null;
			$this->typeList = (!is_null($keyLevel))?$config['levels'][$keyLevel]:[];
		} catch (Exception $e) {
			throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не верные настройки конфига шаблона. '.$e->getMessage()));
		}
		
	}
	
	/**
     * Элементы поля type
     */
	public function getTypeList(){
		$data = [];
		foreach($this->typeList as $value){
			if($value != Page::TYPE_ITEM)
				$data[$value] = Yii::$app->t('custom', $value);
		}
		return $data;
		/*
		return [
			'group' => 'Группа',
			'page' => 'Страница',
			'item' => 'Элемент',
		];
		*/
	}	

	/**
     * Элементы поля template
     */
	public function getTemplateList(){
		$basePath = Yii::getAlias("@frontend/views/templates");
		$filelist = glob($basePath.'/*');
		$fileNameList = [];

		//Форматирование списка шаблонов
		foreach($filelist as $fileName)
		{
			$fileName = explode('/', $fileName);
			$template = $fileName[count($fileName)-1];
			
			$pathTemplate = Yii:: getAlias('@frontend').'/views/templates/'.$template.'/config.php';
			if(file_exists($pathTemplate))
			{
				$config = $this->templateConfig($template);
				if(isSet($config['title'])){
					$fileNameList[$template] = $config['title'];
				}
			}
		}
		return $fileNameList;
	}	
	
	/**
     * Элементы поля controller
     */
	public function getControllerList(){
		$basePath = Yii::getAlias("@frontend/controllers");
		$filelist = glob($basePath.'/*');
		$fileNameList = [];
		
		//Форматирование списка
		foreach($filelist as $fileName)
		{
			$fileName = explode('/', $fileName);
			$controller = $fileName[count($fileName)-1];
			$controller = str_replace('.php', '', $controller);
			$controller = Yii::$app->custom->converterItemUrl($controller);
			$fileNameList[$controller] = $controller;
		}
		return $fileNameList;
	}	
	
	
	
	/**
     * Выдает и буферезирует конфиги шаблонов
     * @return array
     */
	public function templateConfig($template = null){
		if(!$template) $template = $this->parentTemplate;
		if(!$template) $template = 'default';
		if(isSet($this->templateConfig[$template])) return $this->templateConfig[$template];
		else {
			$pathTemplate = Yii:: getAlias('@frontend').'/views/templates/'.$template.'/config.php';
			$config = require $pathTemplate;
			$this->templateConfig[$template] = $config;
			return $this->templateConfig[$template];
		}
	}
	
	/**
     * Выдает все таблицы
     * @return array
     */
	private function getTabels(){
		if(isSet(Yii::$app->commonCustom->cache['cacheTabels'])) return Yii::$app->commonCustom->cache['cacheTabels'];
		else{
			//Получаем список всех таблиц. Для дополнительных полей
			$items = Yii::$app->db->createCommand("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = :dbName")
				->bindValue(':dbName', $this->getDbName())
				->queryAll();
			$tabels = [];
			foreach($items as $item){
				$tabels[] = $item['TABLE_NAME'];
			}
			Yii::$app->commonCustom->cache['cacheTabels'] = $tabels;
			return $tabels;
		}
		
	}
	
	/**
     * Выдает имя текущей базы
     * @return string
     */
	private function getDbName(){
		$dbname = null;
		$dsn = Yii::$app->db->dsn;
		$dsn =  explode(";", $dsn);
		foreach($dsn as $item){
			$item =  explode("=", $item);
			if($item[0] == 'dbname'){
				$dbname = $item[1];
			}
		}
		return $dbname;
	}
	
	/**
     * Проверяем шаблон
     * @param string $template
     * @param string $page_id
     * @return boolean
     */
	public function testingTemplate(){
	//Конфигурация шаблона
	//die('testingTemplate');
		if(!trim($this->parentTemplate)) return false;
		$config = $this->templateConfig();

		$json = (array)json_decode(\yii\helpers\Html::decode($this->hashConfig));
		$hashConfig = null;
		if($json && isSet($json[Yii::$app->language])) $hashConfig = $json[Yii::$app->language];
		
		if($hashConfig != md5(print_r($config, true))){
			$result = Fields::testingTemplate($this->parentTemplate, $this->id, $this->getTabels(), $config);
			if($result){
				//Обновляем список всех таблиц. Для дополнительных полей
				$tabels = [];
				$items = Yii::$app->db->createCommand("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = :dbName")
					->bindValue(':dbName', $this->getDbName())
					->queryAll();
				foreach($items as $item){
					$tabels[] = $item['TABLE_NAME'];
				}
				Yii::$app->commonCustom->cache['cacheTabels'] = $tabels;
			}
			
			$json[Yii::$app->language] = md5(print_r($config, true));
			$this->hashConfig = json_encode($json);
			$this->save(false);
		
			return $result;
		} else return false;
	}
	
	/**
     * Для вывода в шаблоне заголовка из алиаса выподающего списка
	 * @param string $field
	 * @return string
     */
	public function getTitleList($field){
		$config = $this->templateConfig();
		if(isSet($config['fields'][$field])){
			$fields = Yii::$app->dataListComponent->{$config['fields'][$field]['function']}($config['fields'][$field]['params']);
			//die("fields<pre>".print_r($fields, true)."</pre>");
			return $fields[$this->$field];
		}
		else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не верное поле {field}', ['field' => $field]));
	}
	
	/**
     * Информер полей шаблона
	 * @return string
     */
	public function printFields(){
		if($this->type != Page::TYPE_GROUP){
			$data = [
				'template' => $this->parentTemplate.'/'.$this->type.'L'.$this->level,
			];
			$config = $this->templateConfig();
			foreach($config['fields'] as $key => $field){
				if(isSet($field['levels'][$this->level]) && in_array($this->type, $field['levels'][$this->level])){
					$data['fields'][$key]['label'] = $field['label'];
					$data['fields'][$key]['type'] = $field['type'];
					if(in_array($field['type'], [Fields::TYPE_JSON, Fields::TYPE_MAIL_FORM])){
						foreach($field['fields'] as $keyJson => $fieldJson){
							$data['fields'][$key]['fields'][$keyJson]['label'] = $fieldJson[0];
							$data['fields'][$key]['fields'][$keyJson]['type'] = $fieldJson[1];
						}
					}
				}
			}
			//return '<pre>'.print_r($data, true).'</pre>';
			return $data;
		} else null;
	}

	/**
     * Создание url для страниц
	 * Можно получить путь из модели
	 * Либо передать объект страницы явно.
	 * Обязательные поля переданного объекта страницы id, parent_id, type, path, controller
     */
	public function createUrl($page = null){

		if($page && isSet($page->id)) $id = $page->id;
		else $id = $this->id;
	
		if($page && isSet($page->alias)) $alias = $page->alias;
		else $alias = $this->alias;
	
		if($page && isSet($page->title)) $title = $page->title;
		else $title = $this->title;
	
		if($page && isSet($page->parent_id)) $parent_id = $page->parent_id;
		else $parent_id = $this->parent_id;
		
		if($page && isSet($page->type)) $type = $page->type;
		else $type = $this->type;
		
		if($page && isSet($page->path)) $path = $page->path;
		else $path = $this->path;
		
		if($page && isSet($page->controller) && $page->controller) $controller = $page->controller;
		else $controller = $this->parentController;
		
		//cacheUrl
		if(isSet(Yii::$app->commonCustom->cache['cacheUrl'][$id])) return Yii::$app->commonCustom->cache['cacheUrl'][$id];
		
		if($type == Page::TYPE_ITEM){
			$parent_page = self::easyFindModel($parent_id);
			$path = $parent_page->path.$parent_page->id;
		}
		
		$IDs = explode('|', $path);
		$IDs = array_diff($IDs, array('')); //Удаляет пустые значения
		//die("<pre>".print_r($IDs, true)."</pre>");
		
		$keyCache = md5(print_r($IDs, true));
		if(isSet(Yii::$app->commonCustom->cache['cachePathPagesUrl'][$keyCache])) $parent_pages = Yii::$app->commonCustom->cache['cachePathPagesUrl'][$keyCache];
		else {
			$parent_pages = (new \yii\db\Query())
				->select(['id', 
					'parent_id', 
					'title', 
					'path', 
					'alias', 
					'type', 
					])
				->from('page')
				->where(['AND',
					['id' => $IDs],
					['!=', 'alias', 'root'],
					['!=', 'alias', 'main'],
				])
			->orderBy(array( "((LENGTH(path) - LENGTH(REPLACE(path, '|', ''))) / LENGTH('|') + (CASE WHEN type = 'item' THEN 1000 ELSE 0 END))" => SORT_ASC))
			->all();
			Yii::$app->commonCustom->cache['cachePathPagesUrl'][$keyCache] = $parent_pages;
		}
		$IDs[] = $id;
		$parent_pages[] = [
			'id' => $id,
			'parent_id' => $parent_id,
			'title' => $title,
			'path' => $path,
			'alias' => $alias,
			'type' => $type,
		];
		$keyCache = md5(print_r($IDs, true));
		Yii::$app->commonCustom->cache['cachePathPagesUrl'][$keyCache] = $parent_pages;
		//die("$alias<pre>".print_r($parent_pages, true)."</pre>");
		
		$urlPath = '';
		$parent_page = [];
		$parent_page['alias'] = 'main';
		foreach($parent_pages as $parent_page){
			if($urlPath == '') $urlPath .= $parent_page['alias'];
			else $urlPath .= '/'.$parent_page['alias'];
		}
		//if($parent_page['alias'] != 'main'){ 
			if(Yii::$app->controller->action->id == 'api') $urlPath .= '.api'; 
			else $urlPath .= '.html'; 
		//}
		if($controller != 'site') $urlPath = $controller.'/index/'.$urlPath;
		
		$urlPathIndex = $urlPath;
		$urlPathIndex = str_replace('/api/', '/index/', $urlPathIndex); 
		$urlPathIndex = str_replace('.api', '.html', $urlPathIndex); 
		
		$urlPathApi = $urlPath;
		$urlPathApi = str_replace('/index/', '/api/', $urlPathApi); 
		$urlPathApi = str_replace('.html', '.api', $urlPathApi); 

		$result = (object)[
			'thisAlias' => $parent_page['alias'],
			'controller' => $controller,
			'data' => $parent_pages,
			'path' => '/'.$urlPath,
			'pathIndex' => '/'.$urlPathIndex,
			'pathApi' => '/'.$urlPathApi,
		];
		Yii::$app->commonCustom->cache['cacheUrl'][$id] = $result;
		//echo "<pre>".print_r($result, true)."</pre>";
		return $result;
	}	
		
	/**
     * Преобразует данные в удобный вид
 	 * @param array $data
	 * @return array
    */
	public function convertGallery($data){
		$data2 = [];
		foreach($data['url'] as $index => $url){
			$data2[] = [
				'url' => $url,
				'thumbnail' => $data['thumbnail'][$index],
				'title' => $data['title'][$index],
				'text' => $data['text'][$index],
			];
		}
		return $data2;
	}
	
	/*
	* Конвертр для вывода json коллекций во вьюху
	*/
	public function getJsonField($attribute){
		$config = $this->templateConfig();
		$config = $config['fields'][$attribute];
		if($config['type'] != Fields::TYPE_JSON) throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Тип атрибута должен быть Fields::TYPE_JSON'));
		
		$jsonFields = json_decode($this->{$attribute});
		if($jsonFields){

			foreach($jsonFields as &$obj){
				$obj = (array)$obj;
				$key = 'title';
				$obj[$key] = (isSet($obj[$key]))?urldecode($obj[$key]):null;
				$obj[$key] = (isSet($obj[$key]))?strip_tags($obj[$key]):null;
				foreach($config['fields'] as $key => $value){
					$type = $value[1];
					
						if($type == Fields::TYPE_CHECKBOX_LIST){
							if(isSet($obj[$key])){
								$obj[$key] = explode('|', Html::decode(str_replace('undefined|', '', urldecode($obj[$key]))));
							} else $obj[$key] = [];
						} elseif($type == Fields::TYPE_URL){
							if(isSet($obj[$key.'-0'])){
								$obj[$key] = [
									urldecode($obj[$key.'-0']),
									urldecode($obj[$key.'-1']),
								];
							} else $obj[$key] = null;
						} else $obj[$key] = (isSet($obj[$key]))?urldecode($obj[$key]):null;
					
						if($type == Fields::TYPE_CK_EDITOR){
							$obj[$key] = \yii\helpers\HtmlPurifier::process($obj[$key]);
						} 
						elseif(in_array($type, [Fields::TYPE_CHECKBOX_LIST, Fields::TYPE_URL])){}
						else $obj[$key] = (isSet($obj[$key]))?strip_tags($obj[$key]):null;
				}
				$obj = (object)$obj;
			}
			//die("jsonFields<pre>".print_r($jsonFields, true)."</pre>");
			return $jsonFields;
		} else return [];
	}	
	
	public function getForm($fieldName, &$magicData){
		if(isSet($magicData[$fieldName])) $dynamicForm = $magicData[$fieldName];
		else {
			$config = $this->templateConfig();
			if(isSet($config['fields'][$fieldName])) $config = $config['fields'][$fieldName];
			else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не верное поле '.$fieldName));
		
			$dynamicForm = new DynamicForm();
			$dynamicForm->setFields($config['fields']);
		}
		$magicData[$fieldName] = $dynamicForm;
		return $dynamicForm;
	}
	
	public function formBegin($fieldName, $magicData, $template = "{label}\n{input}\n{hint}\n{error}"){
		Yii::$app->view->registerJsFile('/js/jquery.inputmask.js', ['position' => \yii\web\View::POS_END], 'jquery.inputmask');
		Yii::$app->view->registerJsFile('/js/form.js', ['position' => \yii\web\View::POS_END], 'form');

		$config = $this->templateConfig();
		if(isSet($config['fields'][$fieldName])) $config = $config['fields'][$fieldName];
		else throw new \yii\web\ForbiddenHttpException(Yii::$app->t('error', 'Не верное поле '.$fieldName));
		
		$dynamicForm = new DynamicForm();
		if(isSet($magicData[$fieldName])) $dynamicForm = $magicData[$fieldName];
		else {
			$dynamicForm->setFields($config['fields']);
		}

		$action = $this->createUrl()->path;
		if($action == '/') $action = '/main.html';
		
		$type = 'mail';
		if($config['type'] == Fields::TYPE_FORM) $type = 'form';
		$form = ActiveForm::begin([
			'id' => $fieldName,
			'options' => ['enctype' => 'multipart/form-data', 'data-type' => $type],
			'action' => $action,
			'fieldConfig' => [
				'template' => $template,
			],
		]);
		$dynamicForm->form = $form;
		$dynamicForm->config = $config;
		$dynamicForm->fieldName = $fieldName;
		$dynamicForm->options = json_decode(\yii\helpers\Html::decode($this->{$fieldName}));
		
		return $dynamicForm;
	}
	
	public function formEnd(){
		//die("<pre>".print_r($this, true)."</pre>");
		return ActiveForm::end();
	}
	
	public function getMultyLink(){
		if($this->itemParentsID == '|') return [];
		else {
			$IDs = explode('|', $this->itemParentsID);
			$IDs = array_diff($IDs, array('')); //Удаляет пустые значения
			return $IDs;
		}
	}
	
	//Добавление мульти связки. Без проверок!
	public function setMultyLink($itemParentsId){
		$this->itemParentsID = '|'.$itemParentsId.'|';
	}
	
	/**
     * Элементы поля visible
     */
	static public function getVisibleList(){
		return [
			0 => 'Опубликована',
			1 => 'Черновик',
			2 => 'Скрытая',
			3 => 'Данные',
		];
	}	
	
	/**
     * Выдает следующий уровень потомков
     * @param int $parent_id
     * @param int $limit
     * @return array Page $models
     */
	static public function getChildrenNode($parent_id, $limit = 10, $visible = null)
	{
		//$subQueryChildrenCount = (new \yii\db\Query())->select('COUNT(*)')->from(Page::tableName().' as t1')->where('t1.parent_id = t2.id AND t1.type != :type', [':type' =>Page::TYPE_ITEM]);
		$subQueryChildrenCount = (new \yii\db\Query())->select('(1)')->from(Page::tableName().' as t1')->where('t1.parent_id = t2.id AND t1.type != :type', [':type' =>Page::TYPE_ITEM])->limit(1);
		$subQueryChildrenCountItem = (new \yii\db\Query())->select('COUNT(*)')->from(Page::tableName().' as t1')->where('t1.parent_id = t2.id AND t1.type = :type', [':type' =>Page::TYPE_ITEM]);
		$subQueryCountMultyItem = (new \yii\db\Query())->select("(CASE WHEN t2.on_itemParentsID = 1 THEN (SELECT COUNT(*) FROM `page` `t1` WHERE t1.itemParentsID LIKE CONCAT('%|', `t2`.`id`, '|%') AND `t1`.`type` = 'item') ELSE 0 END)");
		
		$page = (new \yii\db\Query())
			->select(['t2.id', 
				't2.title', 
				't2.alias', 
				"JSON_EXTRACT(`jsonTitle` , '$.".Yii::$app->language."') as jsonTitle", 
				't2.parent_id', 
				't2.position', 
				//'t2.visible', 
				"JSON_EXTRACT(`jsonVisible` , '$.ru') as visible_ru", 
				"JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."') as visible", 
				't2.type', 
				'childrenCount' => $subQueryChildrenCount, 
				'childrenCountItem' => $subQueryChildrenCountItem, 
				'countMultyItem' => $subQueryCountMultyItem, 
				//'t2.template', 
				"JSON_EXTRACT(`jsonTemplate` , '$.ru') as template_ru", 
				"JSON_EXTRACT(`jsonTemplate` , '$.".Yii::$app->language."') as template", 
				//'t2.controller', 
				"JSON_EXTRACT(`jsonController` , '$.ru') as controller_ru", 
				"JSON_EXTRACT(`jsonController` , '$.".Yii::$app->language."') as controller", 
				't2.editors', 
				't2.path'])
			->from(Page::tableName().' as t2')
			->where('parent_id=:parent_id AND type != :type', [':parent_id' => $parent_id, ':type' => Page::TYPE_ITEM]);
			
		if(!is_null($visible) && in_array($visible, [Page::VISIBLE_PUBLISHED,Page::VISIBLE_DRAFT,Page::VISIBLE_HIDDEN,Page::VISIBLE_DATA])){
			$page->andWhere(['OR',
				['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), $visible],
				['AND',
					['IS', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), null],
					['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.ru')"), $visible]
				],
			]);
		}	
		$page->orderBy('position');
		if($limit>0) $page->limit($limit);
		$models = $page->all();
		
		foreach($models as &$model){
			if(isSet($model['jsonTitle'])) $model['title'] = json_decode($model['jsonTitle']);
			self::getJsonValModel($model, 'visible');
			self::getJsonValModel($model, 'template');
			self::getJsonValModel($model, 'controller');
		}
		return $models;
	}
	
	/**
     * Облегченный поиск по id или alias
     * @param int $id OR string $alias
     * @return Page object
     */
    public function easyFindModel($var, $type_var = 'id')
    {
		//cacheEasyFindModel
		if(isSet(Yii::$app->commonCustom->cache['cacheEasyFindModel'][$var])){ return Yii::$app->commonCustom->cache['cacheEasyFindModel'][$var]; }
		
		$model = null;
		if($type_var == 'id'){
			$model = (new \yii\db\Query())
				->select(['id', 
					'parent_id', 
					'title', 
					"JSON_EXTRACT(`jsonTitle` , '$.".Yii::$app->language."') as jsonTitle", 
					'alias', 
					'position', 
					//'visible',
					"JSON_EXTRACT(`jsonVisible` , '$.ru') as visible_ru", 
					"JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."') as visible", 					
					'type', 
					//'template', 
					"JSON_EXTRACT(`jsonTemplate` , '$.ru') as template_ru", 
					"JSON_EXTRACT(`jsonTemplate` , '$.".Yii::$app->language."') as template", 
					//'controller', 
					"JSON_EXTRACT(`jsonController` , '$.ru') as controller_ru", 
					"JSON_EXTRACT(`jsonController` , '$.".Yii::$app->language."') as controller", 
					'editors', 
					'path'])
				->from(Page::tableName())
				->where('id=:id', [':id' => (int)$var])
				->one();
		}
		elseif($type_var == 'alias'){
			$model = (new \yii\db\Query())
				->select(['id', 
					'parent_id', 
					'title', 
					"JSON_EXTRACT(`jsonTitle` , '$.".Yii::$app->language."') as jsonTitle", 
					'alias', 
					'position', 
					//'visible',
					"JSON_EXTRACT(`jsonVisible` , '$.ru') as visible_ru", 
					"JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."') as visible", 					
					'type', 
					//'template', 
					"JSON_EXTRACT(`jsonTemplate` , '$.ru') as template_ru", 
					"JSON_EXTRACT(`jsonTemplate` , '$.".Yii::$app->language."') as template", 					
					//'controller', 
					"JSON_EXTRACT(`jsonController` , '$.ru') as controller_ru", 
					"JSON_EXTRACT(`jsonController` , '$.".Yii::$app->language."') as controller", 					
					'editors', 
					'path'])
				->from(Page::tableName())
				->where('alias=:alias', [':alias' => $var])
				->one();
		}
		
		if($model){
			if(isSet($model['jsonTitle'])) $model['title'] = json_decode($model['jsonTitle']);
			self::getJsonValModel($model, 'visible');
			self::getJsonValModel($model, 'template');
			self::getJsonValModel($model, 'controller');
			
			$model = ($model)?(object)$model:null;
			if($model) Yii::$app->commonCustom->cache['cacheEasyFindModel'][$var] = $model;
		}

		return $model;
    }
	
	/**
     * Облегченный поиск потомков
     * @param int $id OR string $alias
     * @return array Page object
     */
    static public function easyChildren($parent_id, $visible = null)
    {
		//easyChildren
		if(isSet(Yii::$app->commonCustom->cache['easyChildren'][$parent_id.$visible])){ return Yii::$app->commonCustom->cache['easyChildren'][$parent_id.$visible]; }
		
		$models = (new \yii\db\Query())
			->select(['id', 
				'parent_id', 
				'title', 
				"JSON_EXTRACT(`jsonTitle` , '$.".Yii::$app->language."') as jsonTitle", 
				'alias', 
				'position', 
				//'visible',
				"JSON_EXTRACT(`jsonVisible` , '$.ru') as visible_ru", 
				"JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."') as visible", 					
				'type', 
				//'template', 
				"JSON_EXTRACT(`jsonTemplate` , '$.ru') as template_ru", 
				"JSON_EXTRACT(`jsonTemplate` , '$.".Yii::$app->language."') as template", 
				//'controller', 
				"JSON_EXTRACT(`jsonController` , '$.ru') as controller_ru", 
				"JSON_EXTRACT(`jsonController` , '$.".Yii::$app->language."') as controller", 
				'editors', 
				'path',
				'updated_at',
				])
			->from(Page::tableName())
			->where('parent_id=:parent_id AND type != :type', [':parent_id' => $parent_id, ':type' => Page::TYPE_ITEM]);
			
		if(!is_null($visible) && in_array($visible, [Page::VISIBLE_PUBLISHED,Page::VISIBLE_DRAFT,Page::VISIBLE_HIDDEN,Page::VISIBLE_DATA])){
			$models->andWhere(['OR',
				['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), $visible],
				['AND',
					['IS', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), null],
					['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.ru')"), $visible]
				],
			]);
		}	
		$models->orderBy('position');
		$models = $models->all();
		
		foreach($models as $index => $model){
			if(isSet($model['jsonTitle'])) $model['title'] = json_decode($model['jsonTitle']);
			self::getJsonValModel($model, 'visible');
			self::getJsonValModel($model, 'template');
			self::getJsonValModel($model, 'controller');
			
			$model = ($model)?(object)$model:null;
			$models[$index] = $model;
			if($model) Yii::$app->commonCustom->cache['cacheEasyFindModel'][$model->id] = $model;
		}
		Yii::$app->commonCustom->cache['easyChildren'][$parent_id.$visible] = $models;
		return $models;
    }
	
	/**
     * Облегченный поиск элементов
     * @param array $where ('parent_id' => $IDs | 'id' => $IDs)
     * @param array $parentTemplate
     * @param array $options (visible | fields | addWhere | sort | pageSize)
     * @return array Page object
     */
    static public function easyItems($where, $parentTemplate, $options = [])
    {
		if(isSet($options['visible'])) $visible = $options['visible'];
		else $visible = Page::VISIBLE_PUBLISHED;

		if(isSet($options['fields'])) $fields = $options['fields'];
		else $fields = [];
		
		if(isSet($options['addWhere'])) $addWhere = $options['addWhere'];
		else $addWhere = [];
		
		if(isSet($options['sort'])) $sort = $options['sort'];
		else $sort = 'position ASC';
		
		if(isSet($options['pageSize'])) $pageSize = $options['pageSize'];
		else $pageSize = 10;

		$request = Yii::$app->request;
		$language = Yii::$app->language;
		$query = (new \yii\db\Query())
			->select(array_merge (['page.id', 
				'page.parent_id', 
				'page.title', 
				"JSON_EXTRACT(`page`.`jsonTitle` , '$.".Yii::$app->language."') as jsonTitle", 
				'page.alias', 
				'page.position', 
				//'visible',
				"JSON_EXTRACT(`page`.`jsonVisible` , '$.ru') as visible_ru", 
				"JSON_EXTRACT(`page`.`jsonVisible` , '$.".Yii::$app->language."') as visible", 					
				'page.type', 
				//'template', 
				"JSON_EXTRACT(`page`.`jsonTemplate` , '$.ru') as template_ru", 
				"JSON_EXTRACT(`page`.`jsonTemplate` , '$.".Yii::$app->language."') as template", 
				//'controller', 
				"JSON_EXTRACT(`page`.`jsonController` , '$.ru') as controller_ru", 
				"JSON_EXTRACT(`page`.`jsonController` , '$.".Yii::$app->language."') as controller", 
				'page.editors', 
				'page.path'],$fields))
			->from(Page::tableName())
			->leftJoin('fields_'.$parentTemplate.'_'.$language.' as '.$language, $language.'.page_id = page.id')
			->where(['AND',
					$where,
					['=', 'type', Page::TYPE_ITEM],
			]);
			
		if(!is_null($visible) && in_array($visible, [Page::VISIBLE_PUBLISHED,Page::VISIBLE_DRAFT,Page::VISIBLE_HIDDEN,Page::VISIBLE_DATA])){
			$query->andWhere(['OR',
				['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), $visible],
				['AND',
					['IS', new Expression("JSON_EXTRACT(`jsonVisible` , '$.".Yii::$app->language."')"), null],
					['=', new Expression("JSON_EXTRACT(`jsonVisible` , '$.ru')"), $visible]
				],
			]);
		}	
		
		if(count($addWhere)){
			$query->andWhere($addWhere);
		}
		
		$query->orderBy($sort);
		//die("<pre>".print_r($query, true)."</pre>");
		
		$provider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => $pageSize,
			],
		]);
		
		$provider->pagination->route = $request->pathInfo;
		$provider->pagination->params = [
			'page' => $request->get('page'),
		];
		
		$items = $provider->getModels();
				
		foreach($items as $index => $model){
			if(isSet($model['jsonTitle'])) $model['title'] = json_decode($model['jsonTitle']);
			self::getJsonValModel($model, 'visible');
			self::getJsonValModel($model, 'template');
			self::getJsonValModel($model, 'controller');
			
			$model = ($model)?(object)$model:null;
			$items[$index] = $model;
		}
		//Yii::$app->commonCustom->cache['easyItems'][$parent_id.$visible] = $models;
		return (object)[
			'items' => $items,
			'provider' => $provider,
		];
    }
	
	static public function getJsonValModel(&$model, $attribute){
		$model[$attribute.'_ru'] = json_decode($model[$attribute.'_ru']);
		$model[$attribute] = json_decode($model[$attribute]);
		if($model[$attribute] == null) $model[$attribute] = $model[$attribute.'_ru'];
	}	
	
	

}
