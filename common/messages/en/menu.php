<?php return [
	'Пользователи' => 'Users',
	'Редакторы' => 'Editods',
	'Страницы' => 'Pages',
	'Переводы' => ' Translations',
	'Выход ({name})' => 'Enter ({name})',
	'Вы действительно хотите удалить кэш с картинками?   ' => 'Are you sure you want to delete your picture cache? ',
	'Кэш' => 'Cache',
];