<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=myengine',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
	'enableSchemaCache' => false,
	// Продолжительность кеширования схемы.
	'schemaCacheDuration' => 3600,
	// Название компонента кеша, используемого для хранения информации о схеме
	'schemaCache' => 'cache',
];
